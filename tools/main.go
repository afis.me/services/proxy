package main

// Build tools, make build compatible for any platform

import (
	cliInterfaces "code.afis.me/modules/go/cli/interfaces"
	cliLib "code.afis.me/modules/go/cli/lib"
	logInterface "code.afis.me/modules/go/logger/interfaces"
	logLib "code.afis.me/modules/go/logger/lib"
	"fmt"
	"github.com/shirou/gopsutil/v3/host"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type Config struct{}
type BuildConfig struct {
	Dir           string `yaml:"dir" default:"."`
	OutputDir     string `yaml:"outputDir" default:"./bin"`
	OutputName    string `yaml:"outputName" default:"app"`
	DisableOSName bool   `yaml:"disableOSName" default:"false"`
	PkgName       string `yaml:"pkgName"`
	Run           bool   `yaml:"run" default:"false"`
}

var config = &Config{}
var buildConfig = &BuildConfig{}
var logger logInterface.Logger

func init() {
	logger = logLib.NewLib()
	logger.Init("Build Tools", "1.0")
}

func getGitVersion() (version string) {
	version = "---"
	if dat, err := exec.Command("sh", "-c", "git rev-list -1 HEAD 2>/dev/null").CombinedOutput(); err == nil {
		ver := strings.TrimSpace(string(dat))
		if len(ver) >= 8 {
			version = ver[:8]
		}
	}
	return version
}

func getOsBuildName() (osBuildName string) {
	osBuildName = "---"
	if strings.Contains(strings.ToLower(runtime.GOOS), "linux") {
		_, err := os.Stat("/etc/os-release")
		if err == nil {
			if dat, err := exec.Command("sh", "-c", `awk -F'=' '/PRETTY_NAME/ {print $2}' /etc/os-release | sed "s/\"//g"`).CombinedOutput(); err == nil {
				return strings.TrimSpace(string(dat))
			}
		}
	}

	if info, err := host.Info(); err == nil {
		return info.Platform
	}
	return osBuildName
}

func main() {
	cli := cliLib.NewLib()
	cli.Init(cliInterfaces.Options{
		Name: logger.ServiceName(),
	})
	cli.SetConfig(config)
	cli.SetDefaultAction(func(ctx interface{}) (err error) {
		logger.Quit()
		return nil
	})

	cli.SetCommand([]*cliInterfaces.Command{{
		Name:   "build",
		Config: buildConfig,
		Action: func(ctx interface{}) (err error) {
			if len(buildConfig.PkgName) == 0 {
				return fmt.Errorf("pkgName is required")
			}

			buildDir, err := filepath.Abs(buildConfig.Dir)
			if err != nil {
				return err
			}

			outPutDir, err := filepath.Abs(buildConfig.OutputDir)
			if err != nil {
				return err
			}

			if !buildConfig.DisableOSName {
				buildConfig.OutputName += "-" + runtime.GOOS + "-" + runtime.GOARCH
			}

			if strings.Contains(strings.ToLower(runtime.GOOS), "windows") {
				buildConfig.OutputName += ".exe"
			}

			binaryOut := filepath.Join(outPutDir, buildConfig.OutputName)
			_ = os.Remove(binaryOut)
			logger.Debug("Build application: %s", buildConfig.PkgName)
			logger.Debug("Directory: %s", buildDir)
			logger.Debug("Output: %s", binaryOut)
			ldFlags := fmt.Sprintf(`-s -w 
					-X '%s/app/interfaces.Version=%s'
					-X '%s/app/interfaces.OSBuildName=%s'
					-X '%s/app/interfaces.BuildDate=%d'
				`,
				buildConfig.PkgName, getGitVersion(),
				buildConfig.PkgName, getOsBuildName(),
				buildConfig.PkgName, time.Now().Unix())
			if err := runCmd(buildDir, "go", []string{"build",
				"-tags", "netgo nokafka",
				"-ldflags", ldFlags,
				"-o", binaryOut,
			}); err != nil {
				return err
			}

			if buildConfig.Run {
				logger.Info("Run application: %s", buildConfig.OutputName)
				if err := runCmd(buildDir, binaryOut, nil); err != nil {
					return err
				}
			}

			logger.Quit()
			return nil
		},
	}})

	if err := cli.Run(); err != nil {
		logger.Error(err).Quit()
	}
}

func runCmd(dir string, p string, args []string) (err error) {
	cmd := exec.Command(p, args...)
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Dir = dir
	err = cmd.Start()
	if err != nil {
		logger.Error("[%s] receiving msg: %s", p, err.Error())
		return err
	}

	err = cmd.Wait()
	if err != nil {
		logger.Error("[%s] receiving msg: %s", p, err.Error())
		return err
	}

	return nil

}
