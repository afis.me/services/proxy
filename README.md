### Go Services Proxy (Nano-Proxy)
Simple http proxy, including load balancer, tracing and auth

##### Compile from source
Required Golang 1.19+

```sh
make build
```

##### Run with docker image
```sh
docker run -d --name nano-proxy --net host --restart unless-stopped \
--memory="1g" --cpus="1" \
afisme/nano-proxy:latest
```

### Configuration Parameter
#### listener (repeated)
- name: ```string, required, must be unique```
- port: ```int, required```
- listen: ```string, required```
- mode: ```string, valid value 'http || tcp || socks || http-proxy || forwarding || forwarding-reverse'```
- secure: ```bool, (http || tcp || http-proxy)```
- pem: ```string, directory of certs or path certs file, bundle in pem format (http || tcp || http-proxy)```
- autoReloadPemDir: ```bool, auto reloading if certs change without downtime, (http || tcp || http-proxy)```
- routing: ```repeated string, (http || tcp)```
- transport: ```string, transport name, (socks || forwarding || forwarding-reverse)```
- allow: ```repeated string, regex, allow address (socks)```
- vault: ```object (auto get certs from vault)```
  - name: ```string, required```
  - address: ```string, vault server address```
  - token: ```string, vault auth token```
  - keys: ```repeated string (vault secret keys)```

#### routing (repeated)
- name: ```string, required, must be unique```
- domain: ```repeated string```
- router: ```repeated object```
  - prefix: ```repeated string, required```
  - backend: ```string, required, backend name```
  - protected: ```bool, if true, this router need auth```
  - enableTracing: ```bool, if true, add jaeger tracing```
  - authConfig: ```object (if protected = true)```
    - mode: ```string, required, basic || oauth2```
    - config:  ```string, required, auth config name```
    - ignoreStatic: ```bool, bypass auth if true, static ext like css, js```
  - function: ```repeated string, function name```

#### backend (repeated)
- name: ```string, required, must be unique```
- mode: ```string, required, manual || discovery || modules```
- driver : ```string, docker || modules_name, (discovery || modules)```
- dockerDiscovery: ```object (discovery)```
  - network:  ```string, required, docker network name```
  - port : ```int, required, docker container port```
  - driver : ```string, required, http || tcp```
  - secure: ```bool```
  - proto: ```http version, h1 || h2```
  - insecureSkipVerify:  ```bool, allow bad server certificate```
  - maxRequest: ```int, max request can be handle this server, default 100```
  - healthcheck: ```string, healthcheck name```
- server: ```repeated object, (manual)```
  - name: ```string, required, must be unique```
  - url: ```string, required, hostname:port format```
  - secure: ```bool```
  - proto: ```http version, h1 || h2, if empty auto proto will used```
  - insecureSkipVerify:  ```bool, allow bad server certificate```
  - maxRequest: ```int, max request can be handle this server, default 100```
  - healthcheck: ```string, healthcheck name```
  - options: ```repeated string, options name```
  - trustedCA: ```string, path of trusted CA certs```

#### transport (repeated)
- name: ```string, required, must be unique```
- driver: ```string, required, ssh || direct```
- sshHost: ```string, (ssh)```
- sshUser:  ```string, (ssh)```
- sshPort:  ```int, (ssh)```
- sshPassword: ```string, (ssh)```
- sshPrivateKey: ```string, (ssh)```
- sshPassphrase: ```string, (ssh)```
- sshTimeout: ```int, (ssh)```


#### auth (repeated)
- name: ```string, required, must be unique```
- users: ```object (basic)```
  - username1: password
  - username2: password
- oauth2Config: ```object (oauth2)```
  - clientId: ```string, required```
  - callback: ```string, required, path after auth success```
  - clientSecret: ```string, required, secret if signingMethod HS256 or public key if signingMethod RS256```
  - signingMethod: ```string, required, HS256 || RS256```
  - authorizationEndpoint: ```string, required```
  - tokenEndpoint: ```object, required```
    - url: ```string, required```
    - proto: ```http version, h1 || h2, if empty auto proto will used```
    - secure: ```bool```
    - insecureSkipVerify:  ```bool, allow bad server certificate```
    - isGrpc: ```bool```
  - externalValidation: ```object```
    - enable: ```bool, if true, every request will be call this validation```
    - revalidationOnRefreshToken: ```bool, if true, revalidated after get refresh token```
    - validation: ```object```
      - url: ```string, required```
      - proto: ```http version, h1 || h2```
      - secure: ```bool```
      - insecureSkipVerify:  ```bool, allow bad server certificate```
      - isGrpc: ```bool```

#### vanityUrls (repeated)
- name: ```string, required, must be unique```
- config:  ```string, required, vanityurls config path or inline base64 yaml config```
- maxRequest: ```int, max request can be handle, default 100```

#### fileBrowser (repeated)
- name: ```string, required, must be unique```
- directory: ```string, required, directory will be serve```
- returnToIndex: ```bool, if true, will be always sending index file if not found. single web apps```
- maxRequest: ```int, max request can be handle, default 100```

### list function names
- ALWAYS_HTTPS ```always redirect to https```
- HTTP.RESPONSE.HEADER.SET::`key`::`value` `replace response header value`
- HTTP.RESPONSE.HEADER.ADD::`key`::`value` `add header value if not available`
- HTTP.RESPONSE.HEADER.REMOVE::`key` `remove response header key`
- HTTP.RESPONSE.HEADER.DEL::`key`::`value` `delete header value if available`
- HTTP.REQUEST.HEADER.SET::`key`::`value` `replace request header value`
- HTTP.REQUEST.HEADER.ADD::`key`::`value` `add header value if not available`
- HTTP.REQUEST.HEADER.REMOVE::`key` `remove request header key`
- HTTP.REQUEST.HEADER.DEL::`key`::`value` `delete header value if available`

### list server options names
- REWRITE_HOST ```replace request HOST with server address, only for http mode```

### modules list
- modules.proxy.vanityUrls 
  <br> *is a simple Go server that allows you to set custom import paths for your Go packages*
  <br> example vanityUrls config
  ```yaml
  host: code.afis.me
  paths:
    /modules/listener:
      repo: https://gitlab.com/afis.me/modules/go/listener
      vcs: gitlab
  ```
- modules.proxy.fileBrowser
  <br> *is a simple Go server to serving static files*


### Examples
Change all example config with your config
#### Simple socks proxy
```shell
 go run . --config examples/config/socks-proxy.yaml
```