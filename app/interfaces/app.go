package interfaces

import (
	listenerInterfaces "code.afis.me/modules/go/listener/interfaces"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"context"
)

var Name string = "nano-router"
var Version string
var OSBuildName string
var BuildDate string

type Controllers interface {
	Init(listenerInterfaces.Listener)
}

type RequestContextType string

const RequestContextProxy RequestContextType = "RequestContextProxy"

type RequestContext struct {
	requestID        string
	routingName      string
	backendName      string
	backendMode      string
	pathMatch        string
	protected        bool
	authConfig       *proxy.AuthConfig
	serverData       interface{}
	enableTracing    bool
	function         []string
	isInternalStatic bool
	outGoingContext  context.Context
	authProvider     Auth
}

func (c *RequestContext) SetAuthProvider(authProvider Auth) {
	c.authProvider = authProvider
}

func (c *RequestContext) GetAuthProvider() Auth {
	return c.authProvider
}

func (c *RequestContext) SetOutGoingContext(ctx context.Context) {
	c.outGoingContext = ctx
}

func (c *RequestContext) GetOutGoingContext() context.Context {
	return c.outGoingContext
}

func (c *RequestContext) GetRequestID() (val string) {
	return c.requestID
}

func (c *RequestContext) SetRequestID(val string) {
	c.requestID = val
}

func (c *RequestContext) GetRoutingName() (val string) {
	return c.routingName
}

func (c *RequestContext) SetRoutingName(val string) {
	c.routingName = val
}

func (c *RequestContext) GetBackendName() (val string) {
	return c.backendName
}

func (c *RequestContext) SetBackendMode(val string) {
	c.backendMode = val
}

func (c *RequestContext) GetBackendMode() (val string) {
	return c.backendMode
}

func (c *RequestContext) SetBackendName(val string) {
	c.backendName = val
}

func (c *RequestContext) GetIsProtected() (val bool) {
	return c.protected
}

func (c *RequestContext) SetIsProtected(val bool) {
	c.protected = val
}

func (c *RequestContext) GetAuthConfig() (val *proxy.AuthConfig) {
	return c.authConfig
}

func (c *RequestContext) SetAuthConfig(val *proxy.AuthConfig) {
	c.authConfig = val
}

func (c *RequestContext) GetEnableTracing() (val bool) {
	return c.enableTracing
}

func (c *RequestContext) SetEnableTracing(val bool) {
	c.enableTracing = val
}

func (c *RequestContext) GetServerData() (val interface{}) {
	return c.serverData
}

func (c *RequestContext) SetServerData(val interface{}) {
	c.serverData = val
}

func (c *RequestContext) GetPathMatch() (val string) {
	return c.pathMatch
}

func (c *RequestContext) SetPathMatch(val string) {
	c.pathMatch = val
}

func (c *RequestContext) GetFunction() (val []string) {
	return c.function
}

func (c *RequestContext) SetFunction(val []string) {
	c.function = val
}

func (c *RequestContext) GetIsInternalStatic() (val bool) {
	return c.isInternalStatic
}

func (c *RequestContext) SetIsInternalStatic(val bool) {
	c.isInternalStatic = val
}

type Router interface {
	AddListener(val *proxy.Listener) error
	RemoveListener(name string) error
	AddRouting(s *proxy.Routing) error
	RemoveRouting(name string) error
	AddBackend(s *proxy.Backend) error
	RemoveBackend(name string) error
	AddServer(s *proxy.Server) error
	RemoveServer(s *proxy.RemoveServerRequest) error
	GetLimiter() interface{}
	SuspendServer(s *proxy.SuspendServerRequest) error
}
