package interfaces

import (
	"github.com/docker/docker/api/types"
)

type Node struct {
	Name    string
	Address string
	Token   string
	Secure  string
}

type Docker interface {
	AddClient(node *Node) (err error)
	RemoveNodeContainer(nodeName string, containerName string, ignoreNotfound bool, opt types.ContainerRemoveOptions) (err error)
	StopNodeContainer(nodeName string, containerID string) (err error)
	FindContainerByNetwork(nodeName string, networkID string) (res []types.Container, err error)
	InspectNodeContainer(nodeName string, containerID string) (res types.ContainerJSON, err error)
}
