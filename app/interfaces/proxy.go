package interfaces

import (
	proxy "code.afis.me/proto/services/proxy/interfaces"
)

type Proxy interface {
	AddClient(node *Node) (err error)
	CheckClient(nodeName string) bool
	RemoveClient(nodeName string) error
	AddBackend(nodeName, developmentName string, backend map[string]*proxy.Backend, removeIfConflict bool) (err error)
	AddRouting(nodeName, developmentName string, routing map[string]*proxy.Routing, removeIfConflict bool) (err error)
	AddListener(nodeName, developmentName string, listener map[string]*proxy.Listener, removeIfConflict bool) (err error)
	AddServer(nodeName string, server *proxy.Server) (err error)
	RemoveServer(nodeName string, re *proxy.RemoveServerRequest) (err error)
	GetBackend(nodeName string) (res []*proxy.Backend, err error)
	RemoveAllServer(nodeName string) (err error)
	SuspendServer(nodeName string, server *proxy.SuspendServerRequest) (err error)
}
