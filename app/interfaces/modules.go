package interfaces

import (
	"net/http"

	proxy "code.afis.me/proto/services/proxy/interfaces"
	"github.com/docker/docker/api/types"
	"golang.org/x/net/context"
)

type Modules interface {
	ID() string
	Name() string
	UseInternalService() bool
	Protected(p string) (protected bool, config *proxy.AuthConfig)
	ServeHTTP(http.ResponseWriter, *http.Request)
}

type ApiModels interface {
	Init() error
	AddData(enableTracing bool, ctx context.Context, req *proxy.Configstore) (err error)
	FindDataByTypeAndName(enableTracing bool, ctx context.Context, typ string, name string) (res *proxy.Configstore, err error)
	GetAllData(enableTracing bool, ctx context.Context) (res []*proxy.Configstore, err error)
	RemoveData(enableTracing bool, ctx context.Context, name string, typ string) (err error)
}

type OnConnected func()

type DockerInternalClient interface {
	Init()
	InspectNodeContainer(containerID string) (res types.ContainerJSON, err error)
	FindContainerByNetwork(networkID string) (res []*proxy.ContainerNetworkResults, err error)
}
