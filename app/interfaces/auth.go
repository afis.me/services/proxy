package interfaces

type Auth interface {
	Valid() (valid bool, isRedirect bool)
	IsExpired() bool
}

type WebsocketProxyError struct {
	Error   bool   `json:"error"`
	Code    int64  `json:"code"`
	Message string `json:"message"`
}
