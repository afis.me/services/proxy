package utils

import "path/filepath"

var StaticAssets = map[string]bool{
	".ico":   true,
	".jpg":   true,
	".jpeg":  true,
	".png":   true,
	".svg":   true,
	".js":    true,
	".css":   true,
	".woff":  true,
	".woff2": true,
	".map":   true,
}

func IsStatic(p string) (v bool) {
	return StaticAssets[filepath.Ext(p)]
}
