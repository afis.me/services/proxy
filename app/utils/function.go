package utils

import (
	"fmt"
	"net/http"
	"strings"
)

func WriteResponseFunction(function []string, res interface{}) {
	for _, s := range function {

		// RESPONSE HEADER MANIPULATION
		if strings.Contains(strings.ToUpper(s), "HTTP.RESPONSE.HEADER.SET") {
			st := strings.Split(s, "::")
			if len(st) == 3 {
				if w, ok := res.(*http.Response); ok {
					w.Header.Set(st[1], st[2])
				} else if w, ok := res.(http.ResponseWriter); ok {
					w.Header().Set(st[1], st[2])
				}
			}
		}

		if strings.Contains(strings.ToUpper(s), "HTTP.RESPONSE.HEADER.REMOVE") {
			st := strings.Split(s, "::")
			if len(st) == 2 {
				if w, ok := res.(*http.Response); ok {
					w.Header.Del(st[1])
				} else if w, ok := res.(http.ResponseWriter); ok {
					w.Header().Del(st[1])
				}
			}
		}

		if strings.Contains(strings.ToUpper(s), "HTTP.RESPONSE.HEADER.ADD") {
			st := strings.Split(s, "::")
			if len(st) == 3 {
				ssb := strings.Split(st[2], ",")
				if len(ssb) != 0 {
					var headerVal = make(map[string]bool)
					var headerRaw string
					if w, ok := res.(*http.Response); ok {
						headerRaw = w.Header.Get(st[1])
					} else if w, ok := res.(http.ResponseWriter); ok {
						headerRaw = w.Header().Get(st[1])
					}

					for _, sv := range strings.Split(headerRaw, ",") {
						headerVal[strings.ToLower(strings.TrimSpace(sv))] = true
					}

					for _, ss := range ssb {
						vt := strings.ToLower(strings.TrimSpace(ss))
						if headerVal[vt] == false {
							headerVal[vt] = true
						}
					}

					if len(headerVal) != 0 {
						var newHeader []string
						for sm := range headerVal {
							newHeader = append(newHeader, sm)
						}

						if w, ok := res.(*http.Response); ok {
							w.Header.Set(st[1], strings.Join(newHeader, ","))
						} else if w, ok := res.(http.ResponseWriter); ok {
							w.Header().Set(st[1], strings.Join(newHeader, ","))
						}

					}
				}

			}
		}

		if strings.Contains(strings.ToUpper(s), "HTTP.RESPONSE.HEADER.DEL") {
			st := strings.Split(s, "::")
			if len(st) == 3 {
				ssb := strings.Split(st[2], ",")
				if len(ssb) != 0 {
					var headerVal = make(map[string]bool)
					var headerRaw string
					if w, ok := res.(*http.Response); ok {
						headerRaw = w.Header.Get(st[1])
					} else if w, ok := res.(http.ResponseWriter); ok {
						headerRaw = w.Header().Get(st[1])
					}

					for _, sv := range strings.Split(headerRaw, ",") {
						headerVal[strings.ToLower(strings.TrimSpace(sv))] = true
					}

					for _, ss := range ssb {
						vt := strings.ToLower(strings.TrimSpace(ss))
						if headerVal[vt] == true {
							delete(headerVal, vt)
						}
					}

					if len(headerVal) != 0 {
						var newHeader []string
						for sm := range headerVal {
							newHeader = append(newHeader, sm)
						}

						if w, ok := res.(*http.Response); ok {
							w.Header.Set(st[1], strings.Join(newHeader, ","))
						} else if w, ok := res.(http.ResponseWriter); ok {
							w.Header().Set(st[1], strings.Join(newHeader, ","))
						}

					}
				}

			}
		}
	}
}

func CheckFunction(function []string, w http.ResponseWriter, r *http.Request) {
	for _, s := range function {
		switch strings.ToUpper(s) {
		case "ALWAYSHTTPS":

			var notSecure = true
			if strings.Contains(strings.ToLower(r.Proto), "https") {
				notSecure = false
			}

			proto := r.Header.Get("x-forwarded-proto")
			if len(proto) != 0 {
				if proto == "https" || proto == "HTTPS" {
					notSecure = false
				}
			}

			if notSecure {
				http.Redirect(w, r, fmt.Sprintf("https://%s%s", r.Host, r.URL), http.StatusPermanentRedirect)
				return
			}
		default:
			if strings.Contains(strings.ToUpper(s), "PATH.STRIP.PREFIX") {
				st := strings.Split(s, "::")
				if len(st) == 2 {
					r.URL.Path = strings.TrimPrefix(r.URL.Path, st[1])
					r.RequestURI = strings.TrimPrefix(r.RequestURI, st[1])
				}
			}

			// REQUEST HEADER MANIPULATION
			if strings.Contains(strings.ToUpper(s), "HTTP.REQUEST.HEADER.SET") {
				st := strings.Split(s, "::")
				if len(st) == 3 {
					r.Header.Set(st[1], st[2])
				}
			}

			if strings.Contains(strings.ToUpper(s), "HTTP.REQUEST.HEADER.REMOVE") {
				st := strings.Split(s, "::")
				if len(st) == 2 {
					r.Header.Del(st[1])
				}
			}

			if strings.Contains(strings.ToUpper(s), "HTTP.REQUEST.HEADER.ADD") {
				st := strings.Split(s, "::")
				if len(st) == 3 {
					ssb := strings.Split(st[2], ",")
					if len(ssb) != 0 {
						var headerVal = make(map[string]bool)
						for _, sv := range strings.Split(r.Header.Get(st[1]), ",") {
							headerVal[strings.ToLower(strings.TrimSpace(sv))] = true
						}

						for _, ss := range ssb {
							vt := strings.ToLower(strings.TrimSpace(ss))
							if headerVal[vt] == false {
								headerVal[vt] = true
							}
						}

						if len(headerVal) != 0 {
							var newHeader []string
							for sm := range headerVal {
								newHeader = append(newHeader, sm)
							}
							r.Header.Set(st[1], strings.Join(newHeader, ","))
						}
					}

				}
			}

			if strings.Contains(strings.ToUpper(s), "HTTP.REQUEST.HEADER.DEL") {
				st := strings.Split(s, "::")
				if len(st) == 3 {
					ssb := strings.Split(st[2], ",")
					if len(ssb) != 0 {
						var headerVal = make(map[string]bool)
						for _, sv := range strings.Split(r.Header.Get(st[1]), ",") {
							headerVal[strings.ToLower(strings.TrimSpace(sv))] = true
						}

						for _, ss := range ssb {
							vt := strings.ToLower(strings.TrimSpace(ss))
							if headerVal[vt] == true {
								delete(headerVal, vt)
							}
						}

						if len(headerVal) != 0 {
							var newHeader []string
							for sm := range headerVal {
								newHeader = append(newHeader, sm)
							}
							r.Header.Set(st[1], strings.Join(newHeader, ","))
						}
					}

				}
			}
		}
	}
}

func IsWebsocket(req *http.Request) bool {
	connHdr := ""
	connHeader := req.Header["Connection"]
	if len(connHeader) > 0 {
		connHdr = connHeader[0]
	}

	upgradeWebsocket := false
	if strings.ToLower(connHdr) == "upgrade" {
		upgradeHeader := req.Header["Upgrade"]
		if len(upgradeHeader) > 0 {
			upgradeWebsocket = strings.ToLower(upgradeHeader[0]) == "websocket"
		}
	}

	if !upgradeWebsocket {
		if req.Header.Get("Upgrade") == "websocket" {
			return true
		}
	}

	return upgradeWebsocket
}
