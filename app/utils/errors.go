package utils

import (
	"code.afis.me/services/proxy/app/interfaces"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"strings"

	grpcutil "code.afis.me/modules/go/utils/grpc"
	"code.afis.me/services/proxy/app/assets"
	"google.golang.org/grpc/codes"
)

var errorMsg = map[int]assets.ErrorMsg{
	http.StatusServiceUnavailable: {
		Status:     http.StatusText(http.StatusServiceUnavailable),
		GRPCStatus: int(codes.ResourceExhausted),
		Message:    "No server available to handle this request",
	},
	http.StatusInternalServerError: {
		Status:     http.StatusText(http.StatusInternalServerError),
		GRPCStatus: int(codes.Internal),
		Message:    "Something when wrong",
	},
	http.StatusUnauthorized: {
		Status:     http.StatusText(http.StatusUnauthorized),
		GRPCStatus: int(codes.Unauthenticated),
		Message:    "Sorry, You don't have permission to access this",
	},
	http.StatusNotFound: {
		Status:     http.StatusText(http.StatusNotFound),
		GRPCStatus: int(codes.NotFound),
		Message:    "Sorry, Your Requested Resource Not Found",
	},
}

func ErrorResponse(w http.ResponseWriter, r *http.Request, eMsg int, customMessage interface{}) {
	msg := errorMsg[eMsg]

	if vals, ok := customMessage.(string); ok {
		if len(vals) != 0 {
			msg.Message = vals
		}
	}

	// response interceptor
	if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
		if value, ok := valCtx.(*interfaces.RequestContext); ok {
			WriteResponseFunction(value.GetFunction(), w)
		}
	}

	meta := grpcutil.ExtractIncoming(grpcutil.NewContextFromRequest(r))
	if !grpcutil.IsGrpcRequest(r) {

		var msw = []byte(msg.Message)
		if strings.Contains(meta.Get("accept"), "application/json") {
			msw, _ = json.Marshal(msg)
			w.Header().Set("content-type", meta.Get("content-type"))
		} else {
			if strings.Contains(meta.Get("accept"), "text/html") {
				if dt := assets.GenerateError(os.Getenv("ServiceID"),
					r.Header.Get("RequestID"), eMsg, msg); len(dt) != 0 {
					msw = dt
				}
			}
		}

		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusOK)
			w.Write(nil)
		} else {
			w.WriteHeader(eMsg)
			_, _ = w.Write(msw)
		}

	} else {

		w.Header().Set("content-type", meta.Get("content-type"))
		w.Header().Set("grpc-status", strconv.Itoa(msg.GRPCStatus))
		w.Header().Set("grpc-message", msg.Message)
		if grpcutil.IsGrpcWebRequest(r) {
			w.WriteHeader(eMsg)
		} else {
			w.WriteHeader(http.StatusOK)
		}

	}
}
