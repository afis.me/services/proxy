package forwardingreserve

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"strings"
	"sync"
	"time"

	proxyProto "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/transport/ssh"
	"github.com/go-playground/validator/v10"
)

type Transport struct {
	validate *validator.Validate
	options  *proxyProto.Listener
	ssh      *ssh.Transport
	conn     net.Listener
	healthy  bool
	network  string
	sync.RWMutex
}

func NewTransport(opt *proxyProto.Listener) *Transport {
	return &Transport{
		validate: validator.New(),
		options:  opt,
	}
}

func (c *Transport) Start() {

	go func() {
		_ = c.ListenAndServe()
	}()
}

func (c *Transport) ListenAndServe() error {
	if len(c.options.Listen) == 0 {
		c.options.Listen = "0.0.0.0/tcp"
	}

	lss := strings.Split(c.options.Listen, "/")
	if len(lss) >= 2 {
		c.options.Listen = lss[0]
		switch lss[1] {
		case "tcp":
			c.network = lss[1]
		default:
			return fmt.Errorf("only support tcp for now")
		}

	}

	if len(c.network) == 0 {
		c.network = "tcp"
	}

	if err := c.validate.Struct(c.options); err != nil {
		config.GetLogger().Error(err).Quit()
	}

	if err := c.validate.Var(c.options.Destination, "required,hostname_port"); err != nil {
		config.GetLogger().Error("Destination '%s' address not valid",
			c.options.Destination).Quit()
	}

	if c.options.TransportOption == nil {
		config.GetLogger().Error("transport options not valid, nil variable").Quit()
	}

	switch strings.ToLower(c.options.TransportOption.Driver) {
	case "ssh":
		c.ssh = ssh.NewTransport(c.options.TransportOption)
	default:
		config.GetLogger().Error("transport %s not valid", c.options.TransportOption.Name).Quit()
	}

	address := fmt.Sprintf("%s:%d", c.options.Listen, c.options.Port)

	go func() {
		_ = c.ssh.Listen(address, c.isHealthy, c.onConnect, c.forward)
	}()
	go c.checkLocal()

	return nil
}

func (c *Transport) onConnect(conn net.Listener) {
	c.conn = conn
}

func (c *Transport) isHealthy() bool {
	c.RLock()
	healthy := c.healthy
	c.RUnlock()
	return healthy
}

func (c *Transport) checkLocal() {
	for range time.NewTicker(1 * time.Second).C {
		_, err := net.Dial(c.network, c.options.Destination)
		healthy := false
		if err == nil {
			healthy = true
		}

		c.Lock()
		c.healthy = healthy
		c.Unlock()

		if !healthy {
			if c.conn != nil {
				_ = c.conn.Close()
			}
		}

	}
}

func (c *Transport) forward(remoteLoc net.Conn) (err error) {

	netR, err := net.Dial(c.network, c.options.Destination)
	if err != nil {
		if !strings.Contains(err.Error(), "connect: connection refused") {
			config.GetLogger().Error(err)
		}

		if err := remoteLoc.Close(); err != nil {
			config.GetLogger().Error(err)
		}

		return err
	}

	defer func(netR net.Conn) {
		_ = netR.Close()
	}(netR)

	if netR != nil {
		if val, ok := netR.(*net.TCPConn); ok {
			_ = val.SetKeepAlive(true)
			_ = val.SetKeepAlivePeriod(30 * time.Second)
		}

		var wg sync.WaitGroup
		wg.Add(2)

		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			defer func(netR net.Conn) {
				_ = netR.Close()
			}(netR)

			buf := make([]byte, 1024)
			for {
				n, err := remoteLoc.Read(buf)
				if err != nil {
					return
				}
				if n > 0 {
					_, err := io.Copy(netR, bytes.NewBuffer(buf[:n]))
					if err != nil {
						config.GetLogger().Error("io.Copy error: %s", err)
						return
					}
				}
			}
		}(&wg)

		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			defer func(remoteLoc net.Conn) {
				_ = remoteLoc.Close()
			}(remoteLoc)

			buf := make([]byte, 1024)
			for {
				n, err := netR.Read(buf)
				if err != nil {
					return
				}
				if n > 0 {
					_, err := io.Copy(remoteLoc, bytes.NewBuffer(buf[:n]))
					if err != nil {
						config.GetLogger().Error("io.Copy error: %s", err)
						return
					}

				}
			}
		}(&wg)

		wg.Wait()
	}

	return err

}

func (c *Transport) Copy(wg *sync.WaitGroup, writer, reader net.Conn) {
	defer wg.Done()
	_, err := io.Copy(writer, reader)
	if err != nil {
		config.GetLogger().Error("io.Copy error: %s", err)
	}
}
