package http

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"

	"code.afis.me/modules/go/listener/interfaces"
	proxyProto "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/modules/go/utils/crypto/hash"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/transport/ssh"
)

var hasPort = regexp.MustCompile(`:\d+$`)

type Transport struct {
	options *proxyProto.Listener
	ssh     *ssh.Transport
	regexp  []*regexp.Regexp
	sync.RWMutex
}

func NewTransport(opt *proxyProto.Listener) *Transport {
	return &Transport{
		options: opt,
	}
}

func (c *Transport) Start() {
	if c.options.TransportOption == nil {
		config.GetLogger().Error("transport options not valid, nil variable").Quit()
	}

	if len(c.options.TransportOption.Driver) == 0 {
		c.options.TransportOption.Driver = "direct"
	}

	switch strings.ToLower(c.options.TransportOption.Driver) {
	case "direct":
	case "ssh":
		c.ssh = ssh.NewTransport(c.options.TransportOption)
	default:
		config.GetLogger().Error("transport %s not valid", c.options.TransportOption.Name).Quit()
	}

	for _, s := range c.options.Allow {
		c.regexp = append(c.regexp, regexp.MustCompile(s))
	}

}

func (c *Transport) Handle(listener interfaces.Listener) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		id := hash.CreateRandomId(10)
		driver := "direct"
		start := time.Now()

		host := r.URL.Host
		if !hasPort.MatchString(host) {
			host += ":80"
		}

		var useSsh bool
		defer func() {
			if err != nil {
				config.GetLogger().Trace("Connection (%s) closed (%s) : %s", id, time.Since(start), err.Error())
			} else {
				config.GetLogger().Trace("Connection (%s) closed (%s) from %s", id, time.Since(start), host)
			}
		}()

		if r.Method != "CONNECT" {
			w.WriteHeader(http.StatusForbidden)
			_, _ = w.Write([]byte("forbidden"))
			return
		}

		hij, ok := w.(http.Hijacker)
		if !ok {
			if r.Body != nil {
				defer func(Body io.ReadCloser) {
					_ = Body.Close()
				}(r.Body)
			}
			http.Error(w, "webserver doesn't support hijacking", http.StatusInternalServerError)
			return
		}

		hijConn, _, err := hij.Hijack()
		if err != nil {
			if r.Body != nil {
				defer func(Body io.ReadCloser) {
					_ = Body.Close()
				}(r.Body)
			}

			return
		}

		defer func(hijConn net.Conn) {
			_ = hijConn.Close()
		}(hijConn)
		switch strings.ToLower(c.options.TransportOption.Driver) {
		case "ssh":
			if c.IsValid(host) {
				driver = "ssh"
				useSsh = true
			}
		}

		config.GetLogger().Trace("Connection (%s) (%s) opened to %s", driver, id, host)

		var conn net.Conn

		if useSsh {
			if !c.ssh.IsConnected {
				err = fmt.Errorf("ssh not connected")
				return
			}

			conn, err = c.ssh.ClientDial("tcp", host)
			if err != nil {
				return
			}

		} else {

			conn, err = net.DialTimeout("tcp", host, 5*time.Second)
			if err != nil {
				return
			}

		}

		if _, err = hijConn.Write([]byte("HTTP/1.1 200 OK\r\n\r\n")); err != nil {
			if err := conn.Close(); err != nil {
				config.GetLogger().Error(err)
			}
			return
		}

		var wg sync.WaitGroup
		wg.Add(2)
		go func() {
			defer wg.Done()
			defer func(conn net.Conn) {
				_ = conn.Close()
			}(conn)
			_, e := io.Copy(conn, hijConn)
			if e != nil {

				c.Lock()
				err = e
				c.Unlock()

				return
			}

			if c, ok := hijConn.(*net.TCPConn); ok {
				if err := c.CloseRead(); err != nil {
					config.GetLogger().Error(err)
				}
			}
		}()

		go func() {
			defer wg.Done()
			defer func(conn net.Conn) {
				_ = conn.Close()
			}(conn)
			_, e := io.Copy(hijConn, conn)
			if e != nil {
				c.Lock()
				err = e
				c.Unlock()
				return
			}

			if c, ok := hijConn.(*net.TCPConn); ok {
				if err := c.CloseWrite(); err != nil {
					config.GetLogger().Error(err)
				}
			}
		}()
		wg.Wait()

		if err := conn.Close(); err != nil {
			config.GetLogger().Error(err)
		}
	}
}

func (c *Transport) IsValid(domain string) bool {
	if len(c.regexp) == 0 {
		return true
	}

	for _, s := range c.regexp {
		if s.MatchString(domain) {
			return true
		}
	}

	return false
}
