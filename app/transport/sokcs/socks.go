package sokcs

import (
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"fmt"
	"github.com/go-playground/validator/v10"
	"regexp"
)

type Transport struct {
	validate *validator.Validate
	options  *proxy.Listener
	regexp   []*regexp.Regexp
}

func NewTransport(opt *proxy.Listener) *Transport {
	return &Transport{
		validate: validator.New(),
		options:  opt,
	}
}

func (c *Transport) Start(authName interface{}) {
	for _, s := range c.options.Allow {
		c.regexp = append(c.regexp, regexp.MustCompile(s))
	}

	var optCre Option
	auth := make(StaticCredentials)
	if authName != nil {
		if cfg, ok := authName.(string); ok {
			for _, s := range config.GetConfig().Auth {
				if s.Name == cfg {
					if s.Users != nil {
						for k, v := range s.Users {
							auth[k] = v
						}
					}

				}
			}
		}
	}

	if len(auth) != 0 {
		config.GetLogger().Info("auth enable for socks5 proxy %s", c.options.Name)
		optCre = WithAuthMethods([]Authenticator{UserPassAuthenticator{auth}})
	}

	server := NewServer(config.GetLogger(), c.options, optCre)
	if err := server.ListenAndServe("tcp", fmt.Sprintf("%s:%d", c.options.Listen, c.options.Port)); err != nil {
		config.GetLogger().Error(err)
		return
	}

}
