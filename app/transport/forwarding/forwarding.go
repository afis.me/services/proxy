package forwarding

import (
	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	"fmt"
	"io"
	"net"
	"strings"
	"sync"
	"time"

	proxyproto "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/modules/go/utils/crypto/hash"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/transport/ssh"
	"github.com/go-playground/validator/v10"
)

type Transport struct {
	validate *validator.Validate
	options  *proxyproto.Listener
	ssh      *ssh.Transport
	network  string
	sync.RWMutex
}

func NewTransport(opt *proxyproto.Listener) *Transport {
	return &Transport{
		validate: validator.New(),
		options:  opt,
	}
}

func (c *Transport) Start() {
	go func() {
		_ = c.ListenAndServe()
	}()
}

func (c *Transport) ListenAndServe() error {
	if len(c.options.Listen) == 0 {
		c.options.Listen = "0.0.0.0/tcp"
	}

	lss := strings.Split(c.options.Listen, "/")
	if len(lss) >= 2 {
		c.options.Listen = lss[0]
		switch lss[1] {
		case "tcp", "udp":
			c.network = lss[1]
		}
	}

	if len(c.network) == 0 {
		c.network = "tcp"
	}

	if err := c.validate.Struct(c.options); err != nil {
		config.GetLogger().Error(err).Quit()
	}

	if err := c.validate.Var(c.options.Destination, "required,hostname_port"); err != nil {
		config.GetLogger().Error("Destination '%s' address not valid",
			c.options.Destination).Quit()
	}

	if c.network == "tcp" {
		return c.ListenTcp()
	}

	return c.ListenUdp()
}

func (c *Transport) ListenUdp() error {

	switch strings.ToLower(c.options.TransportOption.Driver) {
	case "direct":
	default:
		config.GetLogger().Error("transport %s for udp not valid, only direct supported for now", c.options.TransportOption.Name).Quit()
	}

	clientC := make(chan Conn)
	quit := make(chan bool, 1)
	stop := make(chan bool, 3)
	go ListenUDP(fmt.Sprintf("%s:%d", c.options.Listen, c.options.Port), clientC, quit)

	if c.options.MaxRequest == 0 {
		c.options.MaxRequest = 1000
	}

	globalId := hash.CreateRandomId(10)
	limit := limiter.GetDefaultLimiter()
	if limit != nil {
		limit.New(ratelimit.Config{
			ID:   globalId,
			Type: "forwarding",
			Max:  float64(c.options.MaxRequest),
		})
	}

	for {
		var serverConn Conn = nil
		select {
		case <-stop:
			quit <- true
			return nil
		case serverConn = <-clientC:
			if serverConn == nil {
				stop <- true
				continue
			}
		}

		if limit != nil {
			if ls := limit.Load(globalId); ls != nil {
				ls.Rate.Take()
			}
		}

		clientConn, err := ConnUDP(c.options.Destination)
		if err != nil {
			_ = clientConn.Close()
			config.GetLogger().Error(err)
			continue
		}

		// connect with sockets
		go c.copy(limit, hash.CreateRandomId(10), globalId, serverConn, clientConn)

	}

}

func (c *Transport) copy(limit *limiter.Limiter, id, globalId string, writer Conn, reader Conn) {
	exit := make(chan bool, 1)
	go func() {
		_, err := io.Copy(writer, reader)
		if err != nil {
			config.GetLogger().Trace("connect sock %s (A => B): %s", id, err)
		} else {
			config.GetLogger().Trace("connect sock %s (A => B) exited", id)
		}
		exit <- true
	}()

	go func() {
		_, err := io.Copy(reader, writer)
		if err != nil {
			config.GetLogger().Trace("connect sock %s (B => A): %s", id, err)
		} else {
			config.GetLogger().Trace("connect sock %s (B => A) exited", id)
		}
		exit <- true
	}()

	<-exit
	_ = writer.Close()
	_ = reader.Close()

	if limit != nil {
		if ls := limit.Load(globalId); ls != nil {
			ls.Rate.Done()
		}
	}

}

func (c *Transport) ListenTcp() error {
	l, err := net.Listen(c.network, fmt.Sprintf("%s:%d", c.options.Listen, c.options.Port))
	if err != nil {
		config.GetLogger().Error(err).Quit()
		return err
	}

	if c.options.TransportOption == nil {
		config.GetLogger().Error("transport options not valid, nil variable").Quit()
	}

	config.GetLogger().Success("Forwarding port is running on %s:%d/%s",
		c.options.Listen, c.options.Port, c.network)

	switch strings.ToLower(c.options.TransportOption.Driver) {
	case "direct":
	case "ssh":
		c.ssh = ssh.NewTransport(c.options.TransportOption)
	default:
		config.GetLogger().Error("transport %s not valid", c.options.TransportOption.Name).Quit()
	}

	if c.options.MaxRequest == 0 {
		c.options.MaxRequest = 1000
	}

	globalId := hash.CreateRandomId(10)
	limit := limiter.GetDefaultLimiter()
	if limit != nil {
		limit.New(ratelimit.Config{
			ID:   globalId,
			Type: "forwarding",
			Max:  float64(c.options.MaxRequest),
		})
	}

	defer func(l net.Listener) {
		_ = l.Close()
	}(l)
	var tempDelay time.Duration // how long to sleep on accept failure
	for {
		rw, err := l.Accept()
		if limit != nil {
			if ls := limit.Load(globalId); ls != nil {
				ls.Rate.Take()
			}
		}

		if err != nil {
			if _, ok := err.(net.Error); ok {
				if tempDelay == 0 {
					tempDelay = 5 * time.Millisecond
				} else {
					tempDelay *= 2
				}
				if max := 1 * time.Second; tempDelay > max {
					tempDelay = max
				}

				config.GetLogger().Error("Accept error: %v; retrying in %v", err, tempDelay)
				time.Sleep(tempDelay)
				continue
			}
			return err
		}

		tempDelay = 0
		go func() {
			_ = c.forwardTcp(limit, globalId, rw)
		}()

	}
}

func (c *Transport) forwardTcp(limit *limiter.Limiter, globalId string, localConn net.Conn) (err error) {
	id := hash.CreateSmallHash(8)
	tss := time.Now()
	config.GetLogger().Trace("Incoming request (%s)", id)
	var netR net.Conn
	defer func() {
		config.GetLogger().Trace("Request (%s) done in %s", id, time.Since(tss))
		if localConn != nil {
			_ = localConn.Close()
		}
	}()

	switch strings.ToLower(c.options.TransportOption.Driver) {
	case "direct":
		netR, err = net.Dial(c.network, c.options.Destination)
		if err != nil {
			config.GetLogger().Error(err)
			return err
		}

		defer func(netR net.Conn) {
			_ = netR.Close()
		}(netR)

	case "ssh":
		c.ssh.Lock()
		client := c.ssh.Client
		c.ssh.Unlock()
		if client == nil {
			return fmt.Errorf("failed to get ssh transport")
		}

		netR, err = c.ssh.ClientDial(c.network, c.options.Destination)
		if err != nil {
			config.GetLogger().Error(err)
			return err
		}

		defer func(nn net.Conn) {
			_ = nn.Close()
		}(netR)

	default:
		config.GetLogger().Error("transport %s not valid", c.options.TransportOption.Name).Quit()
	}

	if netR != nil {
		if val, ok := netR.(*net.TCPConn); ok {
			_ = val.SetKeepAlive(true)
			_ = val.SetKeepAlivePeriod(30 * time.Second)
		}

		c.copy(limit, hash.CreateRandomId(10), globalId, localConn, netR)

		//
		//var wg sync.WaitGroup
		//wg.Add(2)
		//
		//go func(wg *sync.WaitGroup) {
		//	defer wg.Done()
		//	defer func(netR net.Conn) {
		//		_ = netR.Close()
		//	}(netR)
		//
		//	buf := make([]byte, 1024)
		//	for {
		//		n, err := localConn.Read(buf)
		//		if err != nil {
		//			return
		//		}
		//		if n > 0 {
		//			_, err := io.Copy(netR, bytes.NewBuffer(buf[:n]))
		//			if err != nil {
		//				config.GetLogger().Error("io.Copy error: %s", err)
		//				return
		//			}
		//		}
		//	}
		//}(&wg)
		//
		//go func(wg *sync.WaitGroup) {
		//	defer wg.Done()
		//	defer func(localConn net.Conn) {
		//		_ = localConn.Close()
		//	}(localConn)
		//
		//	buf := make([]byte, 1024)
		//	for {
		//		n, err := netR.Read(buf)
		//		if err != nil {
		//			return
		//		}
		//		if n > 0 {
		//			_, err := io.Copy(localConn, bytes.NewBuffer(buf[:n]))
		//			if err != nil {
		//				config.GetLogger().Error("io.Copy error: %s", err)
		//				return
		//			}
		//
		//		}
		//	}
		//}(&wg)
		//
		//wg.Wait()

	}

	return err

}
