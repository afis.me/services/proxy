package forwarding

import (
	"code.afis.me/services/proxy/app/config"
	"errors"
	"net"
	"sync"
	"time"
)

type Conn interface {
	Read(b []byte) (n int, err error)
	Write(b []byte) (n int, err error)
	Close() error
	RemoteAddr() net.Addr
}

type UDPDistribute struct {
	Established bool
	Conn        *net.UDPConn
	RAddr       net.Addr
	Cache       chan []byte
	sync.RWMutex
}

func NewUDPDistribute(conn *net.UDPConn, addr net.Addr) *UDPDistribute {
	return &UDPDistribute{
		Established: true,
		Conn:        conn,
		RAddr:       addr,
		Cache:       make(chan []byte, 16),
	}
}

func (c *UDPDistribute) Close() error {
	c.Lock()
	c.Established = false
	c.Unlock()
	return nil
}

func (c *UDPDistribute) Read(b []byte) (n int, err error) {
	c.RLock()
	established := c.Established
	c.RUnlock()

	if !established {
		return 0, errors.New("udp distribute has closed")
	}

	select {
	case <-time.After(16 * time.Second):
		return 0, errors.New("udp distribute read timeout")
	case data := <-c.Cache:
		n := len(data)
		copy(b, data)
		return n, nil
	}
}

func (c *UDPDistribute) Write(b []byte) (n int, err error) {
	c.RLock()
	established := c.Established
	c.RUnlock()

	if !established {
		return 0, errors.New("udp distribute has closed")
	}
	return c.Conn.WriteTo(b, c.RAddr)
}

func (c *UDPDistribute) RemoteAddr() net.Addr {
	return c.RAddr
}

func ListenUDP(address string, clientC chan Conn, quit chan bool) {
	addr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		config.GetLogger().Error("udp listen error, %s", err)
		clientC <- nil
		return
	}

	serv, err := net.ListenUDP("udp", addr)
	if err != nil {
		config.GetLogger().Error("udp listen error, %s", err)
		clientC <- nil
		return
	}

	config.GetLogger().Success("Forwarding port is running on %s/udp", address)

	defer func(serv *net.UDPConn) {
		_ = serv.Close()
	}(serv)

	table := make(map[string]*UDPDistribute)

	for {

		select {
		case <-quit:
			return
		default:
		}

		_ = serv.SetDeadline(time.Now().Add(16 * time.Second))

		buf := make([]byte, 32*1024)
		n, addr, err := serv.ReadFrom(buf)
		if err != nil {
			if err, ok := err.(net.Error); ok && err.Timeout() {
				continue
			}
			config.GetLogger().Error("udp listen error, %s", err)
			clientC <- nil
			return
		}
		buf = buf[:n]

		if d, ok := table[addr.String()]; ok {
			if d.Established {
				d.Cache <- buf
				continue
			} else {
				delete(table, addr.String())
			}
		}

		conn := NewUDPDistribute(serv, addr)
		table[addr.String()] = conn
		conn.Cache <- buf
		clientC <- conn
	}
}

func ConnUDP(address string) (Conn, error) {
	conn, err := net.DialTimeout("udp", address, 10*time.Second)
	if err != nil {
		return nil, err
	}

	_, err = conn.Write([]byte("\x00"))
	if err != nil {
		return nil, err
	}

	_ = conn.SetDeadline(time.Now().Add(60 * time.Second))
	return conn, nil
}
