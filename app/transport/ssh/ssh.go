package ssh

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"path/filepath"
	"sync"
	"time"

	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"golang.org/x/crypto/ssh"
)

type Transport struct {
	options               *proxy.Transport
	lastConnectError      string
	lastConnectErrorCount int
	allDialLastTimes      time.Time
	dialLastTimes         time.Time
	Config                *ssh.ClientConfig
	Client                *ssh.Client
	IsConnected           bool
	lastActivity          time.Time
	sync.RWMutex
}

var smux sync.RWMutex
var sshTransportList = make(map[string]*Transport)

func NewTransport(opt *proxy.Transport) *Transport {
	smux.RLock()
	ssex := sshTransportList[opt.Name]
	smux.RUnlock()

	if ssex != nil {
		return ssex
	}

	tr := &Transport{
		options: opt,
	}

	smux.Lock()
	sshTransportList[opt.Name] = tr
	smux.Unlock()

	go tr.start().Dial()

	return tr
}

func (c *Transport) start() *Transport {
	if c.options.SSHPort == 0 {
		c.options.SSHPort = 22
	}

	c.Config = &ssh.ClientConfig{
		User: c.options.SSHUser,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
	}

	if len(c.options.SSHPrivateKey) != 0 {
		if pr, err := c.PrivateKeyFile(c.options.SSHPrivateKey); err == nil {
			c.Config.Auth = append(c.Config.Auth, pr)
		} else {
			config.GetLogger().Error("failed to load private key %s", err).Quit()
		}
	}

	if len(c.options.SSHPassword) != 0 {
		c.Config.Auth = append(c.Config.Auth, ssh.Password(c.options.SSHPassword))
	}

	return c
}

func (c *Transport) Address() string {
	return fmt.Sprintf("%s@%s:%d", c.options.SSHUser, c.options.SSHHost, c.options.SSHPort)
}

func (c *Transport) Ping() (err error) {
	if c.Client == nil {
		return fmt.Errorf("%s", "ssh not connected")
	}

	if c.options.SSHTimeout < 30000 {
		c.options.SSHTimeout = 30000
	}

	tss := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(),
		time.Duration(c.options.SSHTimeout)*time.Millisecond)
	go func() {
		c.RLock()
		client := c.Client
		c.RUnlock()

		_, _, err = client.SendRequest("keepalive@openssh.com", true, nil)
		cancel()
	}()
	<-ctx.Done()

	if ctx.Err() != nil {
		if ctx.Err().Error() != "context canceled" {
			return fmt.Errorf("failed to ping after %s : %s", time.Since(tss), ctx.Err())
		}
	}

	if err == nil || err.Error() == "request failed" {
		// Any response is a success.
		return nil
	}

	return err
}

func (c *Transport) Dial() (ss *ssh.Client, err error) {
	if c.Config.Timeout.Seconds() == 0 {
		c.Config.Timeout = 3 * time.Second
	}

	tss := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(),
		c.Config.Timeout)
	go func() {
		c.Lock()
		ss, err = ssh.Dial("tcp", fmt.Sprintf("%s:%d", c.options.SSHHost, c.options.SSHPort), c.Config)
		c.Unlock()
		cancel()
	}()
	<-ctx.Done()

	if ctx.Err() != nil {
		if ctx.Err().Error() != "context canceled" {
			c.Lock()
			err = fmt.Errorf("failed to connect ssh after %s : %s", time.Since(tss), ctx.Err())
			c.Unlock()
		}
	}

	if err != nil {

		if c.dialLastTimes.Year() == 1 {
			c.allDialLastTimes = time.Now()
			c.dialLastTimes = time.Now()
		}

		if err.Error() != c.lastConnectError {
			config.GetLogger().Error("[%s] Ssh dial %s failed %s",
				c.options.Name, c.Address(), err)
		}

		c.lastConnectError = err.Error()
		tss := time.Since(c.dialLastTimes)
		if tss.Seconds() >= 30 {
			config.GetLogger().Error("[%s] Ssh dial %s failed %d times (%s)",
				c.options.Name,
				c.Address(),
				c.lastConnectErrorCount, time.Since(c.allDialLastTimes))
			c.dialLastTimes = time.Now()
		}

		go func() {
			c.lastConnectErrorCount++
			time.Sleep(1 * time.Second)
			_, _ = c.Dial()
		}()
		return ss, err
	}

	c.Lock()
	c.Client = ss
	c.IsConnected = true
	config.GetLogger().Success("[%s] Connected to ssh server %s",
		c.options.Name, c.Address())
	c.lastConnectError = ""
	c.lastConnectErrorCount = 0
	c.dialLastTimes = time.Time{}
	c.allDialLastTimes = time.Time{}
	c.Unlock()

	go func() {
		for range time.NewTicker(1 * time.Second).C {

			err := c.Ping()

			if err != nil {
				reconnect := true

				c.RLock()
				lastActivity := c.lastActivity
				c.RUnlock()

				if lastActivity.Year() != 1 {
					if time.Since(lastActivity).Milliseconds() < 5000 {
						reconnect = false
					}
				}

				if reconnect {
					config.GetLogger().Error("[%s] lastactivity %s ago, %s",
						c.options.Name, time.Since(lastActivity), err)

					c.IsConnected = false
					if err := c.Client.Close(); err != nil {
						config.GetLogger().Error("[%s] lastactivity %s ago, %s",
							c.options.Name, time.Since(lastActivity), err)
					}
					_, _ = c.Dial()
					break
				} else {
					config.GetLogger().Warning("[%s] lastactivity %s ago, %s",
						c.options.Name, time.Since(lastActivity), err)
				}

			}
		}
	}()

	return ss, err
}

func (c *Transport) UpdateLastActivity(t time.Time) {
	c.Lock()
	c.lastActivity = t
	c.Unlock()
}

func (c *Transport) ClientDial(n string, addr string) (net.Conn, error) {
	if c.Client == nil {
		return nil, fmt.Errorf("client not connected")
	}

	css, err := c.Client.Dial(n, addr)
	if err != nil {
		return nil, err
	}

	return &Net{conn: css, updateLastActivity: c.UpdateLastActivity}, nil
}

func (c *Transport) Listen(address string, isHealty func() bool, onConnect func(conn net.Listener), forward func(localConn net.Conn) error) (err error) {

	c.Lock()
	if !isHealty() {
		err = fmt.Errorf("client local is not healty")
	} else if c.Client == nil {
		err = fmt.Errorf("ssh client nil")
	} else if !c.IsConnected {
		err = fmt.Errorf("ssh client not connected")
	}
	c.Unlock()

	if err != nil {
		config.GetLogger().Error(err)
		time.Sleep(5 * time.Second)
		return c.Listen(address, isHealty, onConnect, forward)
	}

	if conn, err := c.Client.Listen("tcp", address); err != nil {

		config.GetLogger().Error("failed to remote listen : %s", err)
		time.Sleep(5 * time.Second)
		return c.Listen(address, isHealty, onConnect, forward)

	} else {

		onConnect(conn)

		config.GetLogger().Success("listen on remote server %s", conn.Addr().String())
		defer conn.Close()

		var tempDelay time.Duration // how long to sleep on accept failure
		for {
			rw, err := conn.Accept()
			if err != nil {
				if ne, ok := err.(net.Error); ok && ne.Temporary() {
					if tempDelay == 0 {
						tempDelay = 5 * time.Millisecond
					} else {
						tempDelay *= 2
					}
					if max := 1 * time.Second; tempDelay > max {
						tempDelay = max
					}

					config.GetLogger().Error("Accept error: %v; retrying in %v", err, tempDelay)
					time.Sleep(tempDelay)
					continue
				} else {
					config.GetLogger().Error("connection to remote %s is closed : %s", conn.Addr().String(), err)
				}

				time.Sleep(5 * time.Second)

				return c.Listen(address, isHealty, onConnect, forward)
			}

			tempDelay = 0
			go forward(&Net{conn: rw, updateLastActivity: c.UpdateLastActivity})

		}

	}

}

type Net struct {
	conn               net.Conn
	updateLastActivity func(time.Time)
}

func (c *Net) Read(p []byte) (n int, err error) {
	c.updateLastActivity(time.Now())
	return c.conn.Read(p)
}

func (c *Net) Write(data []byte) (n int, err error) {
	c.updateLastActivity(time.Now())
	return c.conn.Write(data)
}

func (c *Net) Close() error {
	return c.conn.Close()
}

func (c *Net) RemoteAddr() net.Addr {
	return c.conn.LocalAddr()
}

func (c *Net) LocalAddr() net.Addr {
	return c.conn.LocalAddr()
}

func (c *Net) SetDeadline(t time.Time) error {
	return c.conn.SetDeadline(t)
}

func (c *Net) SetWriteDeadline(t time.Time) error {
	return c.conn.SetDeadline(t)
}

func (c *Net) SetReadDeadline(t time.Time) error {
	return c.conn.SetDeadline(t)
}

func (c *Transport) PrivateKeyFile(file string) (s ssh.AuthMethod, err error) {
	buffer, err := ioutil.ReadFile(filepath.Clean(file))
	if err != nil {
		return s, err
	}

	var key ssh.Signer
	if len(c.options.SSHPassphrase) == 0 {
		key, err = ssh.ParsePrivateKey(buffer)
		if err != nil {
			return s, err
		}
	} else {
		key, err = ssh.ParsePrivateKeyWithPassphrase(buffer, []byte(c.options.SSHPassphrase))
		if err != nil {
			return s, err
		}
	}

	return ssh.PublicKeys(key), nil
}
