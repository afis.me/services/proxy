package handler

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"code.afis.me/services/proxy/app/utils"

	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	"code.afis.me/modules/go/listener/lib/transport/http/middleware"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/static"
)

func ServingContents() http.Handler {
	return middleware.CompressHandler(nil, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ss := strings.Split(r.URL.Path, "/internal/assets")
		if len(ss) > 0 {
			ls := ss[len(ss)-1]

			if byPass, err := strconv.ParseBool(os.Getenv("STATIC_BYPASS")); err == nil {
				if byPass {
					devFpp := filepath.Join("static/dist/internal/assets", ls)
					if fs, err := os.Stat(devFpp); err == nil {
						if !fs.IsDir() {
							if dat, err := os.ReadFile(devFpp); err == nil {
								http.ServeContent(w, r, fs.Name(), fs.ModTime(),
									bytes.NewReader(dat))
								return
							}
						}
					}

				}
			}

			fpp := filepath.Join("dist/internal/assets", ls)
			if fss, err := static.Fs.Open(fpp); err == nil {
				if fs, err := fss.Stat(); err == nil {
					if !fs.IsDir() {
						if dat, err := static.Fs.ReadFile(fpp); err == nil {
							http.ServeContent(w, r, fs.Name(), fs.ModTime(),
								bytes.NewReader(dat))
							return
						}
					}
				}
			}

		}

		utils.ErrorResponse(w, r, http.StatusNotFound, nil)
	}))
}

func StaticRouter(h http.Handler, id string) http.HandlerFunc {
	var lm = config.GetConfig().InternalStaticMaxRequest
	if lm <= 0 {
		lm = 100
	}

	lmt := limiter.GetDefaultLimiter().New(ratelimit.Config{
		ID:    fmt.Sprintf("internal-static-resources::%s", id),
		Alias: "Internal Static Resources",
		Type:  "modules",
		Max:   float64(lm),
	})

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if val, ok := valCtx.(*interfaces.RequestContext); ok {
				if val.GetIsInternalStatic() {
					lmt.Rate.Take()
					defer lmt.Rate.Done()

					ServingContents().ServeHTTP(w, r)
					return
				}
			}
		}

		h.ServeHTTP(w, r)
	})
}
