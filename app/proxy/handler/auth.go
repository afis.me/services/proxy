package handler

import (
	tracing "code.afis.me/modules/go/tracing/interfaces"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/auth"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
	"fmt"
	"net/http"
	"path/filepath"
	"strings"
)

func AuthRouter(h http.Handler, auth *auth.Auth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if value, ok := valCtx.(*interfaces.RequestContext); ok {
				if value.GetIsProtected() {

					cfg := value.GetAuthConfig()
					if value.GetEnableTracing() {
						tr := config.GetTracing()
						if tr != nil {
							idt := tr.Operation(value.GetOutGoingContext(),
								tracing.NewInteractionName("Auth Http Handler"),
								tracing.NewInteractionTypeType(interfaces.Name))
							value.SetOutGoingContext(idt.OutgoingContext())
							defer idt.Finish()
						}
					}

					if cfg.IgnoreStatic {
						if utils.IsStatic(r.URL.Path) {
							h.ServeHTTP(w, r)
							return
						}

						for _, s := range cfg.AdditionalStaticAsset {
							if s == r.URL.Path {
								h.ServeHTTP(w, r)
								return
							}

							if filepath.Ext(r.URL.Path) == s {
								h.ServeHTTP(w, r)
								return
							}
						}

					}

					if cfg == nil {
						utils.ErrorResponse(w, r, http.StatusInternalServerError, "No auth mode available, contact your administrator")
						return
					}

					if len(cfg.Config) == 0 {
						utils.ErrorResponse(w, r, http.StatusInternalServerError, "Field 'config' must be configured in authConfig section, contact your administrator")
					}

					aut, err := auth.Load(cfg, w, r)
					if err != nil {
						utils.ErrorResponse(w, r, http.StatusInternalServerError, fmt.Sprintf("%s, contact your administrator", err.Error()))
						return
					}

					value.SetAuthProvider(aut)
					valid, isRedirect := aut.Valid()

					if isRedirect {
						return
					}

					if valid {
						h.ServeHTTP(w, r)
						return
					}

					switch strings.ToLower(cfg.Mode) {
					case "basic":
						w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
					}

					utils.ErrorResponse(w, r, http.StatusUnauthorized, "")
					return

				}

			}
		}

		authProvider := config.GetUrlAuth(r.URL.Path)
		if authProvider != nil {
			cfg := &proxy.AuthConfig{Config: authProvider.Name, IgnoreStatic: true}
			if authProvider.Oauth2Config != nil {
				cfg.Mode = "oauth2"
			}
			aut, err := auth.Load(cfg, w, r)
			if err != nil {
				utils.ErrorResponse(w, r, http.StatusInternalServerError, fmt.Sprintf("%s, contact your administrator", err.Error()))
				return
			}

			valid, isRedirect := aut.Valid()
			if isRedirect {
				return
			}

			if valid {
				h.ServeHTTP(w, r)
				return
			}
		}

		h.ServeHTTP(w, r)
	}
}
