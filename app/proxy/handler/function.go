package handler

import (
	tracing "code.afis.me/modules/go/tracing/interfaces"
	grpcUtil "code.afis.me/modules/go/utils/grpc"
	"net/http"
	"strconv"

	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
)

func FunctionRouter(h http.Handler, routingName string, sm *proxy.Routers, prefix string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var enableTracing bool
		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if value, ok := valCtx.(*interfaces.RequestContext); ok {
				if sm != nil {

					backend := sm.Backend

					isNano, _ := strconv.ParseBool(sm.Labels["isNano"])
					if isNano {
						backend = routingName
					}

					value.SetRoutingName(routingName)
					value.SetBackendName(backend)
					value.SetPathMatch(prefix)
					value.SetIsProtected(sm.Protected)
					value.SetAuthConfig(sm.AuthConfig)
					value.SetFunction(sm.Function)

					var foundBackend bool
					if st, ok := config.Backend.Load(backend); ok {
						foundBackend = true
						value.SetBackendMode(st.Mode)
						config.GetLogger().Trace("[%s] [%s] Using backend %s (%s)", r.Host, value.GetRequestID(), backend, st.Mode)
					}

					if !foundBackend {
						config.GetLogger().Trace("[%s] [%s] Using backend %s (not available)", r.Host, value.GetRequestID(), backend)
					}

					enableTracing = sm.EnableTracing
					value.SetEnableTracing(sm.EnableTracing)

				} else {
					value.SetIsInternalStatic(true)
				}

				currentProto := r.Header.Get("X-Forwarded-Proto")
				if len(currentProto) == 0 {
					if r.TLS != nil {
						r.Header.Set("X-Forwarded-Proto", "https")
					} else {
						r.Header.Set("X-Forwarded-Proto", "http")
					}
				}
				r.Header.Set("X-Forwarded-Host", r.Host)

			}
		}

		var idt tracing.Interaction
		if enableTracing {
			tr := config.GetTracing()
			if tr != nil {
				idt = tr.Operation(grpcUtil.NewContextFromRequest(r),
					tracing.NewInteractionName(r.URL.Path),
					tracing.NewInteractionTypeType(interfaces.Name),
					tracing.NewInteractionRequest(r))
				idt.SetLog("Request Headers", r.Header)
				if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
					if value, ok := valCtx.(*interfaces.RequestContext); ok {
						value.SetOutGoingContext(idt.OutgoingContext())
					}
				}

			}
		}

		defer func() {
			if enableTracing && idt != nil {
				rr := grpcUtil.ExtractIncoming(grpcUtil.NewContextFromRequest(r))
				w.Header().Set("Trace-Id", rr.Get("Trace-Id"))
				w.Header().Set("Traceparent", rr.Get("Traceparent"))
				metaResponse := grpcUtil.ExtractIncoming(grpcUtil.NewContextFromRequest(&http.Request{
					Header: w.Header(),
				})).ToJSONString()
				idt.SetLog("Response Headers", metaResponse)
				idt.Finish()
			}
		}()

		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if value, ok := valCtx.(*interfaces.RequestContext); ok {
				utils.CheckFunction(value.GetFunction(), w, r)
			}
		}

		if enableTracing && idt != nil {
			meta := grpcUtil.ExtractIncoming(idt.OutgoingContext())
			r.Header.Set("traceparent", meta.Get("traceparent"))
			itId := meta.Get("trace-id")
			if len(itId) != 0 {
				r.Header.Set("trace-id", itId)
			}
		}

		h.ServeHTTP(w, r)
	}
}
