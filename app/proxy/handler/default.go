package handler

import (
	"context"
	"net/http"
	"time"

	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/listener/lib/options"
	"code.afis.me/modules/go/utils/crypto/hash"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
)

func DefaultRouter(h http.Handler, limit *limiter.Items) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		limit.Rate.Take()

		id := hash.CreateRandomId(10)
		rCtx := &interfaces.RequestContext{}
		rCtx.SetRequestID(id)
		r.Header.Set("RequestID", id)

		options.ParsingIP(r)
		st := time.Now()
		config.GetLogger().Trace("[%s] Start %s %s request (%s) %s",
			r.Host, r.Method, r.Proto, id, r.URL.Path)

		ctx := context.WithValue(r.Context(), interfaces.RequestContextProxy, rCtx)

		defer func() {

			limit.Rate.Done()

			reqDuration := time.Since(st)
			config.GetLogger().Trace("[%s] Request %s %s done (%s) %s %s",
				r.Host, r.Method, r.Proto, id, reqDuration, r.URL.Path)

			if valCtx := ctx.Value(interfaces.RequestContextProxy); valCtx != nil {
				if value, ok := valCtx.(*interfaces.RequestContext); ok {
					if val, ok := value.GetServerData().(*limiter.Items); ok {
						val.Rate.Done()
					}
				}
			}

		}()

		if config.GetShutDown() {
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		h.ServeHTTP(w, r.WithContext(ctx))

	}
}
