package proxy

import (
	"code.afis.me/services/proxy/app/adapter/docker"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"golang.org/x/net/context"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	listener "code.afis.me/modules/go/listener/interfaces"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
	"golang.org/x/net/http2"
)

var repeatConnection = []string{
	"connection refused",
	"client connection force closed",
	"connection reset by peer",
	"unexpected EOF",
	"socket: too many open files",
	"abort Handler",
}

type ResolverItems struct {
	items map[string]*ResolverItem
	sync.RWMutex
}

func NewResolverItems() *ResolverItems {
	return &ResolverItems{
		items: map[string]*ResolverItem{},
	}
}

func (c *ResolverItems) Store(n string, o *ResolverItem) {

	c.Lock()
	defer c.Unlock()
	c.items[n] = o
}

func (c *ResolverItems) Load(n string) (val *ResolverItem, ok bool) {

	c.RLock()
	defer c.RUnlock()
	val, ok = c.items[n]
	return val, ok
}

func (c *ResolverItems) LoadAll() (val []*ResolverItem) {

	c.RLock()
	defer c.RUnlock()
	for _, s := range c.items {
		val = append(val, s)
	}
	return val
}

func (c *ResolverItems) Delete(n string) {

	c.Lock()
	defer c.Unlock()
	delete(c.items, n)
}

func (c *ResolverItems) Count() (n int) {

	c.RLock()
	defer c.RUnlock()
	n = len(c.items)
	return n
}

type ResolverItem struct {
	listener  listener.Listener
	ID        string
	backend   *proxy.Backend
	mode      string
	driver    string
	current   uint64
	serverIds []serverIds
	server    *ResolverServers
	sync.RWMutex
}

func (c *ResolverItem) nextIndex() int {
	count := len(c.serverIds)
	return int(atomic.AddUint64(&c.current, uint64(1)) % uint64(count))
}

func (c *ResolverItem) Get() *ResolverServer {

	c.RLock()
	defer c.RUnlock()

	count := len(c.serverIds)

	if count == 0 {
		return nil
	}

	index := c.nextIndex()
	if count >= index {
		idx := c.serverIds[index]
		if it, ok := c.server.Load(idx.name); ok {
			if !it.healthy {
				return nil
			}
			return it
		}
	}

	return nil
}

func (c *ResolverItem) Add(backend *proxy.Backend, a *proxy.Server) (ssv *ResolverServer, err error) {

	if val, ok := c.server.Load(a.Name); ok {
		return val, nil
	}

	if len(a.Name) == 0 {
		return ssv, fmt.Errorf("failed to register server for backend %s, name is required",
			backend.Name)
	}

	if len(a.Driver) == 0 {
		a.Driver = "http"
	}

	config.GetLogger().Debug("Register server %s, with driver (%s) for backend %s",
		a.Name, strings.ToLower(a.Driver), backend.Name)

	var reverseProxy *httputil.ReverseProxy
	var parse *url.URL

	if err = validate.Var(a.Url, "required,hostname_port"); err != nil {
		return ssv, fmt.Errorf("failed to register server %s for backend %s, url not valid",
			a.Name, backend.Name)
	}

	var uri = fmt.Sprintf("%s//%s", "http:", a.Url)
	if a.Secure {
		uri = fmt.Sprintf("%s//%s", "https:", a.Url)
	}

	parse, err = url.Parse(uri)
	if err != nil {
		return ssv, err
	}

	switch strings.ToLower(a.Driver) {
	case "tcp":
	case "http":

		if len(a.Proto) == 0 {
			a.Proto = "h1"
		}

		reverseProxy = httputil.NewSingleHostReverseProxy(parse)
		// add trusted ca if available
		var caCertPool *x509.CertPool
		if a.Secure {
			if len(a.TrustedCA) > 0 {
				caCert, err := os.ReadFile(a.TrustedCA)
				if err != nil {
					return ssv, fmt.Errorf("Error loading CA certificate: %v\n", err)
				}
				caCertPool = x509.NewCertPool()
				caCertPool.AppendCertsFromPEM(caCert)

			}
		}

		switch a.Proto {
		case "h1":
			if reverseProxy != nil {
				if a.Secure {
					reverseProxy.Transport = &http.Transport{
						DisableCompression: true,
						ForceAttemptHTTP2:  true,
						DisableKeepAlives:  false,
						TLSClientConfig: &tls.Config{
							InsecureSkipVerify: a.InsecureSkipVerify, NextProtos: []string{"h2"},
							RootCAs: caCertPool,
						},
					}
				} else {
					reverseProxy.Transport = &http.Transport{
						DisableCompression: true,
						ForceAttemptHTTP2:  true,
						DisableKeepAlives:  false,
						DialTLSContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
							return net.Dial(network, addr)
						},
					}
				}

			}
		case "h2":
			if reverseProxy != nil {
				if a.Secure {
					reverseProxy.Transport = &http2.Transport{
						AllowHTTP:                  true,
						StrictMaxConcurrentStreams: true,
						DisableCompression:         true,
						TLSClientConfig: &tls.Config{
							InsecureSkipVerify: a.InsecureSkipVerify,
							RootCAs:            caCertPool,
							NextProtos:         []string{"h2"}},
						CountError: func(errType string) {

						},
					}
				} else {
					reverseProxy.Transport = &http2.Transport{
						AllowHTTP: true,
						DialTLSContext: func(tx context.Context, network, addr string, cfg *tls.Config) (net.Conn, error) {
							return net.Dial(network, addr)
						},
						StrictMaxConcurrentStreams: true,
						DisableCompression:         true,
						CountError: func(errType string) {
						},
					}
				}

			}

		}

		if reverseProxy != nil {

			reverseProxy.ModifyResponse = func(res *http.Response) error {
				traceId := res.Request.Header.Get("Trace-Id")
				if len(traceId) != 0 {
					res.Header.Set("Trace-Id", traceId)
				}
				traceparentId := res.Request.Header.Get("traceparent")
				if len(traceparentId) != 0 {
					res.Header.Set("traceparent", traceparentId)
				}

				if res.StatusCode != 0 {
					res.Header.Set("status-code", strconv.Itoa(res.StatusCode))
				}

				// response interceptor
				if valCtx := res.Request.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
					if value, ok := valCtx.(*interfaces.RequestContext); ok {
						utils.WriteResponseFunction(value.GetFunction(), res)
					}
				}

				return nil
			}

			reverseProxy.ErrorLog = config.GetLogger().NewSystemLogger()
			reverseProxy.ErrorHandler = func(w http.ResponseWriter, r *http.Request, e error) {
				c.OnError(w, r, e, a)
			}
		}

	default:
		return ssv, fmt.Errorf("failed to register server %s for backend %s, wrong driver type %s",
			a.Name, backend.Name, a.Driver)
	}

	c.Lock()
	ssv = &ResolverServer{
		addedTime:  time.Now().Unix(),
		resolverID: c.ID,
		limiterID:  backend.Name + ":" + a.Name,
		backend:    backend,
		option:     a,
		proxy:      reverseProxy,
		url:        parse,
	}

	if ssv.option.MaxRequest <= 0 {
		ssv.option.MaxRequest = 1000
	}

	ssv.limiter = c.listener.GetLimiter().New(ratelimit.Config{
		ID:    ssv.limiterID,
		Alias: ssv.option.Name,
		Type:  "server",
		Max:   float64(ssv.option.MaxRequest),
	})

	c.Unlock()

	if reverseProxy != nil {
		reverseProxy.ErrorLog = config.GetLogger().NewSystemLogger()
	}

	c.server.Store(a.Name, ssv)
	c.UpdateServerIds()
	c.CheckHealthy(ssv)

	return ssv, err

}

func (c *ResolverItem) UpdateServerIds() {

	c.Lock()
	c.serverIds = []serverIds{}
	c.Unlock()

	for _, s := range c.server.LoadAll() {
		c.RLock()
		if s.healthy {
			c.serverIds = append(c.serverIds, serverIds{
				name:      s.option.Name,
				addedTime: s.addedTime,
			})
		}
		c.RUnlock()
	}

	c.Lock()
	sort.Slice(c.serverIds, func(i, j int) bool {
		return c.serverIds[i].addedTime < c.serverIds[j].addedTime
	})
	c.Unlock()

}

func (c *ResolverItem) CheckHealthy(ssv *ResolverServer) {
	go func() {
		isDocker, _ := strconv.ParseBool(ssv.option.Labels["isDocker"])

		var ht, dht *proxy.HealthCheck
		ht = &proxy.HealthCheck{
			Type:               "tcp",
			Secure:             ssv.option.Secure,
			InsecureSkipVerify: ssv.option.InsecureSkipVerify,
		}

		if htt, ok := config.HealthCheckValid(ssv.option.HealthCheck); ok {
			valid := true
			switch htt.Type {
			case "tcp", "http":
				if htt.Type == "http" {
					if len(htt.Path) == 0 {
						valid = false
						config.GetLogger().Error("healthy check for server %s, backend %s is not valid path (%s) for http type",
							ssv.option.Name, ssv.option.Backend, htt.Path)
					}
				}
			default:
				valid = false
				config.GetLogger().Error("healthy check for server %s, backend %s is not valid type (%s)",
					ssv.option.Name, ssv.option.Backend, htt.Type)
			}

			if valid {
				ht = htt
			}

		}

		if ht.HealthyCounter < 1 {
			ht.HealthyCounter = 3
		}

		// set default timeout 10s
		if ht.Timeout < 1000 {
			ht.Timeout = 10000
		}

		if isDocker {
			dht = &proxy.HealthCheck{
				Type: "docker",
			}
		}

		config.GetLogger().Debug("running healthy check (%s), timeout (%s) for server %s, backend %s",
			ht.Type, time.Duration(ht.Timeout)*time.Millisecond, ssv.option.Name, ssv.option.Backend)

		for range time.NewTicker(1 * time.Second).C {

			c.RLock()
			deleted := ssv.deleted
			c.RUnlock()

			if deleted {
				return
			}

			c.HealthCheck(ht, ssv)
			if dht != nil {
				c.HealthCheck(dht, ssv)
			}

		}
	}()

}

func (c *ResolverItem) HealthCheck(ht *proxy.HealthCheck, ssv *ResolverServer) {

	timeout := time.Duration(ht.Timeout) * time.Millisecond

	c.RLock()
	suspended := ssv.suspended
	c.RUnlock()

	if ht.Type == "docker" {
		res, err := docker.Get().InspectNodeContainer(os.Getenv("docker-client"), ssv.option.Labels["containerId"])
		var removingServer bool
		if err != nil {
			if !strings.Contains(err.Error(), "Error: No such container") {
				config.GetLogger().Error(err)
			}
			removingServer = true
		} else {
			if res.State.Running && !suspended {
				c.ChangeHealthy(true, ssv, ht, nil)
			} else if !res.State.Running {
				removingServer = true
			}
		}

		if removingServer {
			ssv.healthyCounter = 0
			c.ChangeHealthy(false, ssv, ht, fmt.Errorf("docker container removed"))
			c.Del(ssv.option)
			config.AddQueServerRemove(ssv.option.Name)
		}

	}

	if suspended {
		c.ChangeHealthy(false, ssv, ht, fmt.Errorf("server is suspended"))
		return
	}

	if ht.Type == "http" {
		var netClient = &http.Client{
			Timeout: timeout,
			Transport: &http.Transport{
				ForceAttemptHTTP2: true,
			},
		}

		ulf := fmt.Sprintf("http://%s%s", ssv.option.Url, ht.Path)
		if ht.Secure {
			netClient.Transport = &http.Transport{
				ForceAttemptHTTP2: true,
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: ht.InsecureSkipVerify,
				},
			}
			ulf = fmt.Sprintf("https://%s%s", ssv.option.Url, ht.Path)
		}

		req, err := http.NewRequest("GET", ulf, nil)
		if err == nil {
			req.Header = http.Header{}
			req.Header.Set("User-Agent", "nano-proxy check agent healthy/1.0.0")
			res, err := netClient.Do(req)
			if err == nil {
				if res.StatusCode == 200 {
					ssv.lastHealthy = time.Now()
					ssv.healthyCounter = ssv.healthyCounter + 1
					c.ChangeHealthy(true, ssv, ht, nil)
				} else {
					ssv.healthyCounter = 0
					c.ChangeHealthy(false, ssv, ht, fmt.Errorf("receive http status code %d", res.StatusCode))
				}

			} else {
				ssv.healthyCounter = 0
				c.ChangeHealthy(false, ssv, ht, err)
			}

		}

	}

	if ht.Type == "tcp" {

		if !ht.Secure {
			if conn, err := net.DialTimeout("tcp", ssv.option.Url, timeout); err == nil {
				if err := conn.Close(); err != nil {
					config.GetLogger().Error(err)
				}
				ssv.lastHealthy = time.Now()
				ssv.healthyCounter = ssv.healthyCounter + 1
				c.ChangeHealthy(true, ssv, ht, nil)
			} else {
				ssv.healthyCounter = 0
				c.ChangeHealthy(false, ssv, ht, err)
			}
		} else if ssv.option.Secure {
			conf := &tls.Config{InsecureSkipVerify: ht.InsecureSkipVerify}
			if conn, err := tls.DialWithDialer(&net.Dialer{Timeout: timeout}, "tcp", ssv.option.Url, conf); err == nil {
				if err := conn.Close(); err != nil {
					config.GetLogger().Error(err)
				}
				ssv.lastHealthy = time.Now()
				ssv.healthyCounter = ssv.healthyCounter + 1
				c.ChangeHealthy(true, ssv, ht, nil)
			} else {
				ssv.healthyCounter = 0
				c.ChangeHealthy(false, ssv, ht, err)
			}
		}
	}

}

func (c *ResolverItem) ChangeHealthy(healthy bool, ssv *ResolverServer, ht *proxy.HealthCheck, err error) {

	var updated bool
	c.Lock()
	if ssv.lastHealthyChange.Year() <= 1 {
		updated = true
	}
	ssv.lastHealthyChange = time.Now()
	if healthy != ssv.healthy {
		updated = true
	}
	c.Unlock()

	ssv.firstHealth.Do(func() {
		if !healthy && err != nil {
			config.GetLogger().Warning("Server %s for backend %s, is healthy : %t, (%s)",
				ssv.option.Name, c.backend.Name, ssv.healthy, err.Error())
		}
	})

	if updated {

		c.RLock()
		lastHealthyCounter := ssv.healthyCounter
		c.RUnlock()

		if int32(lastHealthyCounter) < ht.HealthyCounter {
			return
		}

		c.Lock()
		ssv.healthy = healthy
		c.Unlock()

		if err != nil {
			config.GetLogger().Warning("Server %s for backend %s, is healthy : %t, (%s)",
				ssv.option.Name, c.backend.Name, ssv.healthy, err.Error())
		} else {
			config.GetLogger().Debug("Server %s for backend %s, is healthy : %t",
				ssv.option.Name, c.backend.Name, ssv.healthy)
		}

		c.UpdateServerIds()

	}
}

func (c *ResolverItem) Del(a *proxy.Server) {
	var validItem bool
	sv, ok := c.server.Load(a.Name)
	if ok {
		validItem = true
		c.server.Delete(a.Name)
		config.GetLogger().Debug("Removing server %s for backend %s",
			a.Name, c.backend.Name)
		c.listener.GetLimiter().Remove(ratelimit.Config{
			ID: sv.limiterID,
		})
	}
	if validItem {
		c.UpdateServerIds()
	}
}

func (c *ResolverItem) Suspend(a *proxy.Server) {
	res, ok := c.server.Load(a.Name)
	if ok {
		config.GetLogger().Debug("Suspend server %s for backend %s",
			a.Name, c.backend.Name)
		c.Lock()
		res.suspended = true
		c.Unlock()
	}
}

func (c *ResolverItem) OnError(w http.ResponseWriter, r *http.Request, e error, _ *proxy.Server) {

	if c.server.Count() == 0 {
		utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
		return
	}

	var requestId string
	if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
		if value, ok := valCtx.(*interfaces.RequestContext); ok {
			requestId = value.GetRequestID()
		}
	}

	removeErrorListener := resolver.GetRemoveErrorListener(requestId)
	var repeatRequest = false
	for _, so := range repeatConnection {
		if strings.Contains(e.Error(), so) {
			repeatRequest = true
			break
		}
	}

	if !strings.Contains(e.Error(), "context canceled") {
		if !repeatRequest {
			config.GetLogger().Error(e)
		}
	}

	if removeErrorListener != nil {
		if repeatRequest {
			removeErrorListener(w, r, e)
			return
		}
	}

	utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)

}
