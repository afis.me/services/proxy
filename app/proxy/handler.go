package proxy

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"code.afis.me/modules/go/listener/lib/transport/splitter"
	tracing "code.afis.me/modules/go/tracing/interfaces"
	grpcUtil "code.afis.me/modules/go/utils/grpc"
	parsingUtils "code.afis.me/modules/go/utils/parsing"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/modules"
	"code.afis.me/services/proxy/app/utils"
	"nhooyr.io/websocket"
)

type RouterHandler struct {
	*splitter.WrappedGrpcServer
}

func (c *RouterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var pr, priId, prID, backendMode string
	ctx := r.Context()
	meta := grpcUtil.ExtractIncoming(grpcUtil.NewContextFromRequest(r))
	if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
		if value, ok := valCtx.(*interfaces.RequestContext); ok {
			prID = value.GetRequestID()
			pr = value.GetBackendName()
			priId = value.GetRoutingName()
			backendMode = value.GetBackendMode()

			if value.GetEnableTracing() {
				tr := config.GetTracing()
				if tr != nil {
					idt := tr.Operation(value.GetOutGoingContext(),
						tracing.NewInteractionName("Primary Http Handler"),
						tracing.NewInteractionTypeType(interfaces.Name))
					ctx = idt.OutgoingContext()
					value.SetOutGoingContext(ctx)
					defer idt.Finish()
				}
			}

		}
	}

	// recovery defer func
	defer func() {
		resolver.RemoveErrorListener(prID)

		req := recover()
		if req != nil {
			var err error
			switch t := req.(type) {
			case string:
				err = errors.New(t)
			case error:
				err = t
			default:
				err = fmt.Errorf("%s", "Unknown error")
			}

			erVal := tracing.ErrorStack(err.Error(),
				parsingUtils.ParseStack(debug.Stack()))

			r.Header.Set("status-code", strconv.Itoa(500))

			if !grpcUtil.IsGrpcRequest(r) {
				w.Header().Set("content-type", "application/json")
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write(erVal)
			} else {
				w.Header().Set("content-type", meta.Get("content-type"))
				w.Header().Set("grpc-status", "13")
				w.Header().Set("grpc-message", err.Error())
				w.WriteHeader(http.StatusOK)
			}

		}

	}()

	if len(pr) == 0 || len(priId) == 0 {
		utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
		return
	}

	switch backendMode {
	case "modules":
		if bc, ok := config.Backend.Load(pr); ok {
			if mod, ok := modules.Get(bc.Driver); ok {
				if mod.UseInternalService() {
					mod.ServeHTTP(w, r)
					return
				}
			}
		}
	}

	var prs *ResolverServer
	prs = resolver.GetProxy(pr, prID, func(ww http.ResponseWriter, rr *http.Request, e error) {
		prs.limiter.Rate.Done()
		rrs := r.Header.Get("repeated")
		repeated, err := strconv.Atoi(rrs)
		if err != nil {
			utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
			return
		}

		if repeated >= 2 {
			utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
			return
		}

		repeated = repeated + 1
		time.Sleep(time.Duration(1500*repeated) * time.Second)
		r.Header.Set("repeated", strconv.Itoa(repeated+1))
		c.ServeHTTP(w, r)
	})

	if prs == nil {
		config.GetLogger().Trace("[%s] no proxy server found for backend %s", prID, pr)
	}

	if prs != nil {

		prs.limiter.Rate.Take()

		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if value, ok := valCtx.(*interfaces.RequestContext); ok {
				value.SetServerData(prs.limiter)
			}
		}

		if prs.url != nil {
			r.URL.Host = prs.url.Host
			r.URL.Scheme = prs.url.Scheme
			if prs.option != nil {
				for _, s := range prs.option.Options {
					switch strings.ToUpper(s) {
					case "REWRITE_HOST":
						r.Host = prs.url.Host
					}
				}
			}
		}

		switch strings.ToLower(prs.option.Driver) {
		case "http":
			if prs.proxy != nil {
				if utils.IsWebsocket(r) {
					p := c.websocketProxy(ctx, prID, prs.option.Url, prs.option.Secure, prs.option.InsecureSkipVerify)
					p.ServeHTTP(w, r)
				} else {
					meta := grpcUtil.ExtractOutgoing(ctx)
					r.Header.Set("Traceparent", meta.Get("Traceparent"))
					prs.proxy.ServeHTTP(w, r)
				}
			} else {
				utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
				return
			}
		default:
			utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
		}

		return
	}

	utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)

}

func (c *RouterHandler) websocketProxy(ctx context.Context, prId, target string, secure, insecureSkipVerify bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if value, ok := valCtx.(*interfaces.RequestContext); ok {
				if value.GetEnableTracing() {
					tr := config.GetTracing()
					if tr != nil {
						idt := tr.Operation(ctx,
							tracing.NewInteractionName("Websocket Proxy"),
							tracing.NewInteractionTypeType(interfaces.Name))
						defer idt.Finish()
					}
				}
			}
		}

		// accept websocket connection
		userAgentLower := strings.ToLower(r.Header.Get("User-Agent"))
		isSafari := strings.Contains(userAgentLower, "safari") && !strings.Contains(userAgentLower, "chrome") && !strings.Contains(userAgentLower, "android")
		acceptOptions := &websocket.AcceptOptions{
			InsecureSkipVerify: insecureSkipVerify,
		}

		if isSafari {
			acceptOptions.CompressionMode = websocket.CompressionDisabled
		}

		worker := &config.SocketWorker{}

		w.Header().Set("X-Handler-By", "websocket-proxy")
		client, err := websocket.Accept(w, r, acceptOptions)
		if err != nil {
			config.GetLogger().Error(err)
			return
		}

		worker.Client = client

		if isError, err := strconv.ParseBool(w.Header().Get("proxy-auth-error")); err == nil {
			if isError {
				statusCode, err := strconv.ParseInt(w.Header().Get("proxy-auth-code"), 10, 64)
				if err != nil {
					return
				}
				data, _ := json.Marshal(&interfaces.WebsocketProxyError{Error: true, Code: statusCode, Message: w.Header().Get("proxy-auth-msg")})
				_ = client.Write(context.Background(), websocket.MessageBinary, data)
				_ = client.Close(websocket.StatusGoingAway, w.Header().Get("proxy-auth-msg"))
				time.Sleep(1500 * time.Millisecond)
				_ = client.Close(websocket.StatusPolicyViolation, "")
				return
			}
		}

		// connect to backend
		wsUrl := fmt.Sprintf("ws://%s%s", target, r.URL.Path)
		if secure {
			wsUrl = fmt.Sprintf("wss://%s%s", target, r.URL.Path)
		}
		wsUrl = strings.TrimRight(wsUrl, "/")

		server, _, err := websocket.Dial(r.Context(), wsUrl, &websocket.DialOptions{
			HTTPHeader: r.Header,
			HTTPClient: &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: insecureSkipVerify,
					},
				},
			},
		})
		if err != nil {
			config.GetLogger().Error(err)
			data, _ := json.Marshal(&interfaces.WebsocketProxyError{Error: true, Code: 503, Message: "No server available to handle this request"})
			_ = client.Write(context.Background(), websocket.MessageBinary, data)
			time.Sleep(1500 * time.Millisecond)
			_ = client.Close(websocket.StatusInternalError, "")
			return
		}

		worker.Server = server

		// send accepted
		data, _ := json.Marshal(&interfaces.WebsocketProxyError{Error: false, Code: 200, Message: "accepted"})
		_ = client.Write(context.Background(), websocket.MessageBinary, data)

		// copy data
		errClient := make(chan error, 2)
		config.GetSocketWorkerStorage().Add(prId, worker)
		defer config.GetSocketWorkerStorage().Remove(prId)

		go func() {
			for {
				_, raw, err := server.Read(context.Background())
				if err != nil {
					break
				}
				err = client.Write(context.Background(), websocket.MessageBinary, raw)
				if err != nil {
					break
				}
			}
			errClient <- err
		}()

		go func() {
			for {
				_, reader, err := client.Reader(context.Background())
				if err != nil {
					break
				}

				buffer := bytes.NewBuffer(nil)
				n, err := io.Copy(buffer, reader)
				if err != nil {
					break
				}

				if n != 0 {
					err := server.Write(context.Background(), websocket.MessageBinary, buffer.Bytes())
					if err != nil {
						break
					}
				}

			}
			errClient <- err
		}()

		<-errClient
		_ = client.Close(websocket.StatusNormalClosure, "")
		_ = server.Close(websocket.StatusNormalClosure, "")

	})
}

type NotFound struct {
}

func (c *NotFound) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	utils.ErrorResponse(w, r, http.StatusServiceUnavailable, nil)
}
