package proxy

import (
	listenerInterfaces "code.afis.me/modules/go/listener/interfaces"
	listenerLib "code.afis.me/modules/go/listener/lib"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	"code.afis.me/modules/go/utils/crypto/hash"
	listenerProto "code.afis.me/proto/modules/listener/interfaces"
	proxyProto "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/adapter/docker"
	"code.afis.me/services/proxy/app/auth"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/modules"
	"code.afis.me/services/proxy/app/proxy/handler"
	"code.afis.me/services/proxy/app/proxy/router"
	"code.afis.me/services/proxy/app/transport/forwarding"
	forwardingReverse "code.afis.me/services/proxy/app/transport/forwarding-reverse"
	httpTransport "code.afis.me/services/proxy/app/transport/http"
	socksTransport "code.afis.me/services/proxy/app/transport/sokcs"
	"fmt"
	"github.com/gorilla/mux"
	"net"
	"net/http"
	"nhooyr.io/websocket"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Router -
type Router struct {
	id             string
	listener       listenerInterfaces.Listener
	matchDomainReg map[string]*regexp.Regexp
	listenerRoute  map[string]*mux.Router
	auth           *auth.Auth
	router         *router.Router
	channel        chan bool
	sync.RWMutex
}

func NewRouter() *Router {
	return &Router{
		id:             hash.CreateRandomId(10),
		matchDomainReg: make(map[string]*regexp.Regexp),
		listenerRoute:  make(map[string]*mux.Router),
		listener:       listenerLib.NewLib(),
		auth:           auth.New(),
	}
}

func (c *Router) Start() {

	config.Validate()

	c.listener.InitProcess(listenerInterfaces.Options{
		ServiceName:      config.GetLogger().ServiceName(),
		Logger:           config.GetLogger(),
		EnableEncryption: true,
	})

	c.router = router.NewRouter(c.listener)

	c.listener.SetTracer(config.GetTracing())

	NewResolver(c.listener).Setup()

	c.SetListener()

	if err := c.router.Start(); err != nil {
		config.GetLogger().Error(err).Quit()
	}

	// revalidate config
	config.Validate()

	// starting modules
	modules.New(c.listener, c, c.auth)

	config.GetLogger().Info("Router is starting ....")
	// enable docker discovery
	if config.GetConfig().Discovery != nil {
		if config.GetConfig().Discovery.Enable {
			switch config.GetConfig().Discovery.Driver {
			case "docker":
				err := docker.Get().AddClient(&interfaces.Node{
					Name: os.Getenv("docker-client"),
				})
				if err != nil {
					config.GetLogger().Error(err).Quit()
				}
			default:
				config.GetLogger().Error("discovery driver %s not supported", config.GetConfig().Discovery.Driver)

			}
		}
	}

	c.channel = make(chan bool)
	go func() {
		ticker := time.NewTicker(1 * time.Second).C
		for {
			select {
			case <-config.ShutdownChanel:
				config.GetLogger().Info("Router is stopping in progress ....")
			case <-ticker:
				c.DockerWatch()
			case <-c.channel:
				config.GetLogger().Info("Router has been stopped")
				return
			}
		}
	}()

}

func (c *Router) DockerWatch() {
	for _, s := range config.Backend.LoadAll() {
		switch s.Mode {
		case "discovery":
			switch s.Driver {
			case "docker":
				if s.DockerDiscovery != nil {
					res, err := docker.Get().FindContainerByNetwork(os.Getenv("docker-client"), s.DockerDiscovery.Network)
					if err != nil {
						config.GetLogger().Error(err)
					} else {
						if len(res) != 0 {
							for _, sm := range res {
								if len(sm.NetworkSettings.Networks) != 0 {
									for _, svm := range sm.NetworkSettings.Networks {
										if err := c.AddServer(&proxyProto.Server{
											Name:               strings.Join(sm.Names, "-"),
											Backend:            s.Name,
											Service:            s.Name,
											Driver:             s.DockerDiscovery.Driver,
											Url:                fmt.Sprintf("%s:%d", svm.IPAddress, s.DockerDiscovery.Port),
											Secure:             s.DockerDiscovery.Secure,
											InsecureSkipVerify: s.DockerDiscovery.InsecureSkipVerify,
											Proto:              s.DockerDiscovery.Proto,
											MaxRequest:         s.DockerDiscovery.MaxRequest,
											HealthCheck:        s.DockerDiscovery.HealthCheck,
											Labels: map[string]string{
												"isDocker":    "true",
												"containerId": sm.ID,
											},
										}); err != nil {
											config.GetLogger().Error(err)
										}
									}

								}

							}
						}
					}
				}
			}
		}
	}
}

func (c *Router) Stop() {
	config.SetShutDown(true)
	config.ShutdownChanel <- true

	config.GetSocketWorkerStorage().Iter(func(value *config.SocketWorker) {
		if value != nil {
			if value.Client != nil {
				_ = value.Client.Close(websocket.StatusServiceRestart, "server is restarting")
			}

			if value.Server != nil {
				_ = value.Server.Close(websocket.StatusServiceRestart, "server is restarting")
			}
		}
	})

	if c.router != nil {
		c.router.Stop()
	}
	c.channel <- true
}

func (c *Router) SetListener() {
	for _, val := range config.GetListenerList() {
		config.GetLogger().Debug("found listener %s, with mode %s", val.Name, val.Mode)
		switch val.Mode {
		case "http":
			go c.SetHttpListener(val)
		case "http-proxy":
			go c.SetHttpProxyListener(val)
		case "socks":
			go socksTransport.NewTransport(val).Start(val.Name)
		case "forwarding":
			go forwarding.NewTransport(val).Start()
		case "forwarding-reverse":
			go forwardingReverse.NewTransport(val).Start()
		case "tcp":
			go c.SetTCPListener(val)
		}
	}
}

func (c *Router) SetHttpProxyListener(val *proxyProto.Listener) {
	tr := httpTransport.NewTransport(val)
	tr.Start()
	listConfig, maxRequest := c.generateListenerConfig(val)
	item := c.listener.GetLimiter().New(ratelimit.Config{
		ID:    val.Name,
		Alias: val.Name,
		Type:  "listener",
		Max:   maxRequest})
	c.router.PushRouterHProxyListener(listConfig, handler.DefaultRouter(tr.Handle(c.listener), item))
}

type FinalMatch struct {
	PathMatch   string
	RoutingName string
	Routing     *proxyProto.Routers
}

func (c *Router) generateListenerConfig(val *proxyProto.Listener) (*listenerProto.RouterListener, float64) {
	maxRequest := val.MaxRequest
	if maxRequest < 100 {
		maxRequest = 1000
	}

	pem := val.Pem
	isNano, _ := strconv.ParseBool(val.Labels["isNano"])
	if isNano && len(pem) != 0 {
		config.GetLogger().Debug("[%s] nano listener detected", val.Name)
		if va, err := filepath.Abs(filepath.Join(config.GetConfig().DataDirectory, "nodes")); err == nil {
			pem = filepath.Join(va, val.Pem)
		}
	}
	listConfig := &listenerProto.RouterListener{
		Name:             val.Name,
		Pem:              pem,
		Port:             val.Port,
		Listen:           val.Listen,
		Secure:           val.Secure,
		AutoReloadPemDir: val.AutoReloadPemDir,
	}
	if val.Vault != nil {
		listConfig.Vault = &listenerProto.Vault{
			Name:    val.Vault.Name,
			Address: val.Vault.Address,
			Token:   val.Vault.Token,
			Keys:    val.Vault.Keys,
		}
	}
	return listConfig, float64(maxRequest)
}

func (c *Router) SetHttpListener(val *proxyProto.Listener) {
	listConfig, maxRequest := c.generateListenerConfig(val)
	c.NewRouting(val)
	item := c.listener.GetLimiter().New(ratelimit.Config{
		ID:    val.Name,
		Alias: val.Name,
		Type:  "listener",
		Max:   maxRequest})
	c.router.PushRouterListener(listConfig, handler.DefaultRouter(c.Routing(val.Name), item))

}

func (c *Router) SetTCPListener(val *proxyProto.Listener) {
	listConfig, maxRequest := c.generateListenerConfig(val)
	item := c.listener.GetLimiter().New(ratelimit.Config{
		ID:    val.Name,
		Alias: val.Name,
		Type:  "listener",
		Max:   maxRequest})
	c.router.PushRouterTcpListener(listConfig, DefaultTcpRouter(item, val))
}

func (c *Router) NewRouting(val *proxyProto.Listener) {
	route := mux.NewRouter().StrictSlash(true)

	c.Lock()
	delete(c.listenerRoute, val.Name)
	c.listenerRoute[val.Name] = route
	c.Unlock()

	if val.Mode != "http" {
		return
	}

	config.GetLogger().Debug("Creating new routing for listener %s", val.Name)

	route.NotFoundHandler = &NotFound{}

	route.PathPrefix("/internal/assets").HandlerFunc(handler.FunctionRouter(
		handler.StaticRouter(handler.AuthRouter(&RouterHandler{}, c.auth), val.Name), "internalStatic", nil, "/internal/assets"))

	routingCheck := val.Routing
	isNano, _ := strconv.ParseBool(val.Labels["isNano"])
	if isNano {
		config.GetLogger().Info("nano listener detected for %s", val.Name)
		routingCheck = append(routingCheck, val.Name)
	}

	for _, fr := range config.Routing.LoadAll() {
		for _, s := range routingCheck {
			if fr.Name == s {

				if len(fr.Domain) != 0 {
					for _, st := range fr.Domain {
						for _, sm := range fr.Routers {
							config.GetLogger().Info("Found routing for listener %s, route %s, domain %s, prefix %s", val.Name, fr.Name, st, sm.Prefix)
							for _, sv := range sm.Prefix {
								rs := route.NewRoute()
								rs.Host(st).PathPrefix(sv).HandlerFunc(handler.FunctionRouter(
									handler.StaticRouter(handler.AuthRouter(&RouterHandler{}, c.auth), val.Name), s, sm, sv))
							}

						}
					}
				}

				if len(fr.Domain) == 0 {
					for _, sm := range fr.Routers {
						config.GetLogger().Info("Found routing for listener %s, route %s, prefix %s", val.Name, fr.Name, sm.Prefix)
						for _, sv := range sm.Prefix {
							rs := route.NewRoute()
							rs.PathPrefix(sv).HandlerFunc(handler.FunctionRouter(
								handler.StaticRouter(handler.AuthRouter(&RouterHandler{}, c.auth), val.Name), s, sm, sv))
						}

					}
				}
			}

		}

	}

}

func (c *Router) Routing(lisName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		c.RLock()
		route := c.listenerRoute[lisName]
		c.RUnlock()

		if route != nil {
			route.ServeHTTP(w, r)
		} else {
			http.Error(w, "router not found", 404)
		}

	}
}

func (c *Router) AddListener(val *proxyProto.Listener) error {

	for _, sv := range config.GetListenerList() {
		if sv.Name == val.Name {
			return fmt.Errorf("listener with name %s already use", val.Name)
		}
	}

	conn, err := net.Listen("tcp", fmt.Sprintf("%s:%d", val.Listen, val.Port))
	if err != nil {
		return err
	}

	_ = conn.Close()

	if val.Mode == "http" {
		config.AddListener([]*proxyProto.Listener{val})
		c.SetHttpListener(val)
	}

	if val.Mode == "tcp" {
		config.AddListener([]*proxyProto.Listener{val})
		c.SetTCPListener(val)
	}

	return nil
}

func (c *Router) RemoveListener(name string) error {
	var valid bool
	var listener *proxyProto.Listener
	for _, sv := range config.GetListenerList() {
		if sv.Name == name {
			valid = true
			listener = sv
			break
		}
	}

	if !valid {
		return fmt.Errorf("listener with name %s not found", name)
	}

	if listener.Mode == "http" {
		if err := c.router.RemoveRouterListener(name); err != nil {
			return err
		}
	}

	if listener.Mode == "tcp" {
		if err := c.router.RemoveRouterTcpListener(name); err != nil {
			return err
		}
	}

	config.RemoveListener(name)
	c.Lock()
	c.listenerRoute[name] = nil
	delete(c.listenerRoute, name)
	c.Unlock()

	c.listener.GetLimiter().Remove(ratelimit.Config{ID: name})
	c.listener.GetLimiter().Remove(ratelimit.Config{ID: fmt.Sprintf("internal-static-resources::%s", name)})

	return nil
}

func (c *Router) AddRouting(s *proxyProto.Routing) error {
	_, valid := config.Routing.Load(s.Name)
	if valid {
		return fmt.Errorf("routing with name %s already use", s.Name)
	}

	config.Routing.Store(s.Name, s)
	isNano, _ := strconv.ParseBool(s.Labels["isNano"])

	for _, sv := range config.GetListenerList() {
		if isNano {
			config.GetLogger().Info("nano routing detected for %s", s.Name)
			if sv.Name == s.Name {
				config.GetLogger().Info("listener found for %s", s.Name)
				c.NewRouting(sv)
			}
		} else {
			for _, sm := range sv.Routing {
				if sm == s.Name {
					c.NewRouting(sv)
				}
			}
		}

	}
	return nil
}

func (c *Router) RemoveRouting(name string) error {
	_, valid := config.Routing.Load(name)

	if !valid {
		return fmt.Errorf("routing with name %s not found", name)
	}

	config.Routing.Delete(name)
	for _, sv := range config.GetListenerList() {
		for _, sm := range sv.Routing {
			if sm == name {
				c.NewRouting(sv)
			}
		}
	}

	return nil
}

func (c *Router) RemoveBackend(name string) error {
	_, valid := config.Backend.Load(name)

	if !valid {
		return fmt.Errorf("backend with name %s not found", name)
	}

	if res := resolver.Get(name); res != nil {
		if res.server != nil {
			for _, s := range res.server.LoadAll() {
				res.Del(s.option)
			}
		}
	}

	resolver.Remove(name)
	config.Backend.Delete(name)

	return nil
}

func (c *Router) AddBackend(s *proxyProto.Backend) error {
	_, valid := config.Backend.Load(s.Name)
	if valid {
		return fmt.Errorf("backend with name %s already use", s.Name)
	}

	config.Backend.Store(s.Name, s)
	resolver.AddBackend(s)

	return nil
}

func (c *Router) AddServer(s *proxyProto.Server) error {
	be, valid := config.Backend.Load(s.Backend)

	if !valid {
		return fmt.Errorf("backend with name %s not found", s.Backend)
	}

	if be != nil {
		if res := resolver.Get(s.Backend); res != nil {
			_, err := res.Add(be, s)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (c *Router) RemoveServer(s *proxyProto.RemoveServerRequest) error {

	be, valid := config.Backend.Load(s.Name)

	config.GetLogger().Info("removing server %s, for backend %s", s.Name, s.BackendName)
	if !valid {
		if s.RemoveByDevelopment {
			return nil
		}
		return fmt.Errorf("backend with name %s not found", s.BackendName)
	}

	if be != nil {
		if res := resolver.Get(s.BackendName); res != nil {
			if res.server != nil {
				for _, sm := range res.server.LoadAll() {
					if sm.option.Name == s.Name {
						res.Del(sm.option)
					}
				}
			}
		}
	}

	return nil
}

func (c *Router) SuspendServer(s *proxyProto.SuspendServerRequest) error {
	if len(s.BackendName) != 0 {
		if res := resolver.Get(s.BackendName); res != nil {
			if res.server != nil {
				for _, sm := range res.server.LoadAll() {
					if sm.option.Name == s.Name {
						res.Suspend(sm.option)
					}
				}
			}
		}
	} else if len(s.ContainerId) != 0 {
		for _, sm := range config.Backend.LoadAll() {
			if res := resolver.Get(sm.Name); res != nil {
				var valid bool
				if res.server != nil {
					for _, sv := range res.server.LoadAll() {
						if containerId, ok := sv.option.Labels["containerId"]; ok {
							if containerId == s.ContainerId {
								res.Suspend(sv.option)
								valid = true
								break
							}
						}
					}
				}

				if valid {
					break
				}
			}
		}
	}

	return nil
}

func (c *Router) GetLimiter() interface{} {
	return c.listener.GetLimiter()
}

func (c *Router) IsServicesValid(services []string, ser string) bool {
	for _, s := range services {
		if s == ser {
			return true
		}
	}

	return false
}
