package proxy

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/go-playground/validator/v10"

	listener "code.afis.me/modules/go/listener/interfaces"
	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	"code.afis.me/modules/go/utils/crypto/hash"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/modules"
)

var validate *validator.Validate

func init() {
	validate = validator.New()
}

type OnProxyError func(w http.ResponseWriter, r *http.Request, e error)

type serverIds struct {
	name      string
	addedTime int64
}

type Resolver struct {
	listener     listener.Listener
	items        *ResolverItems
	onProxyError map[string]OnProxyError
	sync.RWMutex
}

var resolver *Resolver

func NewResolver(lis listener.Listener) *Resolver {
	resolver = &Resolver{
		listener:     lis,
		items:        NewResolverItems(),
		onProxyError: map[string]OnProxyError{},
	}
	return resolver
}

func (c *Resolver) Setup() {
	for _, s := range config.Backend.LoadAll() {
		c.AddBackend(s)
	}

}

func (c *Resolver) AddBackend(s *proxy.Backend) {

	config.GetLogger().Debug("Register backend private-services %s with mode %s",
		s.Name, s.Mode)

	//c.listener.GetLimiter().New(s.Name, 100, 1*time.Second)

	sv := &ResolverItem{
		listener: c.listener,
		backend:  s,
		mode:     s.Mode,
		driver:   s.Driver,
		server:   NewResolverServers(),
		ID:       hash.CreateRandomId(10)}

	switch strings.ToLower(s.Mode) {
	case "modules":
		if mod, ok := modules.Get(s.Driver); ok {
			if !mod.UseInternalService() {
				if len(s.Server) != 0 {
					for _, sm := range s.Server {
						if _, err := sv.Add(s, sm); err != nil {
							config.GetLogger().Error(err).Quit()
						}
					}
				}
			}
		}

	case "discovery":
		switch strings.ToLower(s.Driver) {
		case "docker":

		default:
			config.GetLogger().Error("Driver %s for mode %s as backend private-services %s is not valid",
				s.Driver, s.Mode, s.Name).Quit()
		}

	case "manual":
		for _, sm := range s.Server {
			if _, err := sv.Add(s, sm); err != nil {
				config.GetLogger().Error(err).Quit()
			}
		}
	default:
		config.GetLogger().Error("Mode %s for backend private-services %s is not valid",
			s.Mode, s.Name).Quit()

	}

	c.items.Store(s.Name, sv)

}

func (c *Resolver) Remove(name string) {
	c.RLock()
	val, ok := c.items.Load(name)
	c.RUnlock()
	if ok {
		c.Lock()
		c.listener.GetLimiter().Remove(ratelimit.Config{
			ID: val.ID,
		})
		c.items.Delete(val.ID)
		c.Unlock()
	}
}

func (c *Resolver) Get(name string) *ResolverItem {
	c.RLock()
	val, ok := c.items.Load(name)
	c.RUnlock()
	if ok {
		return val
	}
	return nil
}

func (c *Resolver) RemoveErrorListener(id string) {
	c.Lock()
	delete(c.onProxyError, id)
	c.Unlock()
}

func (c *Resolver) GetRemoveErrorListener(id string) (onError OnProxyError) {
	c.RLock()
	defer c.RUnlock()
	it := c.onProxyError[id]
	return it
}

func (c *Resolver) AddErrorListener(id string, onError OnProxyError) {
	c.Lock()
	defer c.Unlock()
	c.onProxyError[id] = onError
}

func (c *Resolver) GetProxy(n string, id string, onError OnProxyError) *ResolverServer {
	c.AddErrorListener(id, onError)

	it, ok := c.items.Load(n)
	if it != nil && ok {
		return it.Get()
	}
	return nil
}

func (c *Resolver) GetNextServer(n string) *ResolverServer {
	it, ok := c.items.Load(n)
	if it != nil && ok {
		return it.Get()
	}
	return nil
}

// ResolverServers sync map
type ResolverServers struct {
	items map[string]*ResolverServer
	sync.RWMutex
}

type ResolverServer struct {
	resolverID        string
	backend           *proxy.Backend
	addedTime         int64
	limiterID         string
	limiter           *limiter.Items
	healthy           bool
	lastHealthy       time.Time
	lastHealthyChange time.Time
	healthyCounter    int
	url               *url.URL
	proxy             *httputil.ReverseProxy
	option            *proxy.Server
	deleted           bool
	suspended         bool
	firstHealth       sync.Once
}

func (c *ResolverServer) copy(wg *sync.WaitGroup, wc io.WriteCloser, r io.Reader) {
	defer wc.Close()
	defer wg.Done()
	io.Copy(wc, r)
}

func (c *ResolverServer) HandleTcpConnection(clientConn net.Conn, info *listener.TcpConnectionInformation) (err error) {
	c.limiter.Rate.Take()
	defer c.limiter.Rate.Done()

	addr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%s", c.url.Hostname(), c.url.Port()))
	if err != nil {
		return err
	}

	var backendConn net.Conn
	if c.option.Secure {
		backendConn, err = tls.Dial("tcp", addr.String(), &tls.Config{InsecureSkipVerify: c.option.InsecureSkipVerify})
		if err != nil {
			return err
		}
	} else {
		backendConn, err = net.Dial("tcp", addr.String())
		if err != nil {
			return err
		}
	}

	var wg sync.WaitGroup
	wg.Add(2)
	go c.copy(&wg, backendConn, clientConn)
	go c.copy(&wg, clientConn, backendConn)
	wg.Wait()

	return nil
}

func NewResolverServers() *ResolverServers {
	return &ResolverServers{
		items: map[string]*ResolverServer{},
	}
}

func (c *ResolverServers) Store(n string, o *ResolverServer) {
	c.Lock()
	c.items[n] = o
	c.Unlock()
}

func (c *ResolverServers) Load(n string) (val *ResolverServer, ok bool) {
	c.RLock()
	val, ok = c.items[n]
	c.RUnlock()
	return val, ok
}

func (c *ResolverServers) LoadAll() (val []*ResolverServer) {
	c.RLock()
	for _, s := range c.items {
		val = append(val, s)
	}
	c.RUnlock()
	return val
}

func (c *ResolverServers) Delete(n string) {
	c.Lock()
	if val, ok := c.items[n]; ok {
		val.deleted = true
		val.proxy = nil
	}
	delete(c.items, n)
	c.Unlock()
}

func (c *ResolverServers) Count() (n int) {
	c.RLock()
	n = len(c.items)
	c.RUnlock()
	return n
}

// End ResolverServers sync map
