package router

import (
	"bufio"
	"bytes"
	listenerInterfaces "code.afis.me/modules/go/listener/interfaces"
	ioUtils "code.afis.me/modules/go/utils/io"
	"code.afis.me/proto/modules/listener/interfaces"
	"context"
	"crypto/tls"
	"fmt"
	gorillaContext "github.com/gorilla/context"
	netInfo "github.com/shirou/gopsutil/v3/net"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

type Router struct {
	listener  listenerInterfaces.Listener
	operation *Operation
}

func NewRouter(listener listenerInterfaces.Listener) *Router {
	if err := ioUtils.SetMaxRlimit(); err != nil {
		listener.GetOptions().Logger.Warning("Failed to set u-limit, %s",
			err.Error())
	}

	return &Router{
		listener: listener,
	}
}

func (c *Router) Start() error {
	c.operation = NewListener(c.listener)
	return nil
}

func (c *Router) Stop() {
	if c.operation != nil {
		c.operation.CleanAll()
	}
}

func (c *Router) PushRouterListener(l *interfaces.RouterListener, h http.HandlerFunc) {
	c.operation.AddOrSkip(l, h, false)
}

func (c *Router) PushRouterHProxyListener(l *interfaces.RouterListener, h http.HandlerFunc) {
	c.operation.AddOrSkip(l, h, true)
}

func (c *Router) PushRouterTcpListener(l *interfaces.RouterListener, h listenerInterfaces.TcpHandler) {
	c.operation.AddOrSkipTcp(l, h)
}

func (c *Router) RemoveRouterListener(name string) error {
	return c.operation.Remove(name)
}

func (c *Router) RemoveRouterTcpListener(name string) error {
	return c.operation.RemoveTcp(name)
}

type ListenerItems struct {
	options *interfaces.RouterListener
	server  *Server
	handle  http.Handler
	n       int64
}

func (c *ListenerItems) OnStateChange(conn net.Conn, state http.ConnState) {
	switch state {
	case http.StateNew:
		c.Add(1)
	case http.StateHijacked, http.StateClosed:
		c.Add(-1)
	}
}

func (c *ListenerItems) Count() int {
	return int(atomic.LoadInt64(&c.n))
}

func (c *ListenerItems) Add(cw int64) {
	atomic.AddInt64(&c.n, cw)
}

type Operation struct {
	listener listenerInterfaces.Listener
	items    map[string]*ListenerItems
	sync.RWMutex
}

func NewListener(listener listenerInterfaces.Listener) *Operation {
	return &Operation{
		listener: listener,
		items:    map[string]*ListenerItems{},
	}
}

func (c *Operation) CleanAll() {
	c.Lock()
	defer c.Unlock()

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	for _, s := range c.items {
		if s.server != nil {
			if s.server.Server != nil {
				if err := s.server.Server.Shutdown(ctx); err != nil {
					log.Printf("HTTP server Shutdown: %v\n", err)
				}
			}
			if s.server.serverTcp != nil {
				if err := s.server.serverTcp.Close(); err != nil {
					log.Printf("Tcp server Shutdown: %v\n", err)
				}
			}
		}
	}
}

func (c *Operation) AddOrSkip(l *interfaces.RouterListener, h http.HandlerFunc, disableHttp2 bool) {
	if len(l.Name) == 0 {
		c.listener.GetOptions().Logger.Error("listener name is required").Quit()
	}

	c.RLock()
	_, ok := c.items[l.Name]
	c.RUnlock()

	if !ok {
		c.newHTTPListener(l, h, disableHttp2)
	}
}

func (c *Operation) AddOrSkipTcp(l *interfaces.RouterListener, h listenerInterfaces.TcpHandler) {
	if len(l.Name) == 0 {
		c.listener.GetOptions().Logger.Error("listener name is required").Quit()
	}

	c.RLock()
	_, ok := c.items[l.Name]
	c.RUnlock()

	if !ok {
		c.newTCPListener(l, h)
	}
}

func (c *Operation) Remove(name string) error {
	c.RLock()
	lis, ok := c.items[name]
	c.RUnlock()

	if ok {
		if lis.server != nil {
			if lis.server.Server != nil {
				if err := lis.server.Server.Close(); err != nil {
					return err
				}
			}
		}
	}

	c.Lock()
	delete(c.items, name)
	c.Unlock()

	return nil
}

func (c *Operation) RemoveTcp(name string) error {
	c.RLock()
	lis, ok := c.items[name]
	c.RUnlock()

	if ok {
		if err := lis.server.StopTcp(); err != nil {
			return err
		}
	}

	c.Lock()
	delete(c.items, name)
	c.Unlock()

	return nil
}

func (c *Operation) newHTTPListener(s *interfaces.RouterListener, h http.HandlerFunc, disableHttp2 bool) {
	li := &ListenerItems{options: s}

	if !disableHttp2 {
		li.handle = h2c.NewHandler(gorillaContext.ClearHandler(h), &http2.Server{})
	}

	if disableHttp2 {
		li.handle = gorillaContext.ClearHandler(h)
	}

	li.server = &Server{
		listener: c.listener,
		secure:   s.Secure,
		Server: &http.Server{
			ConnState:         li.OnStateChange,
			Addr:              fmt.Sprintf("%s:%d", s.Listen, s.Port),
			Handler:           li.handle,
			MaxHeaderBytes:    1 << 20,
			ReadHeaderTimeout: 30 * time.Second,
		}}

	li.server.Server.ErrorLog = c.listener.GetOptions().Logger.NewSystemLogger()

	if s.Secure {
		li.server.Server.TLSConfig = c.listener.GetTLSCerts().NewConfig(
			fmt.Sprintf("router:%s", li.options.Name),
			li.options.Pem, li.options.AutoReloadPemDir, disableHttp2, li.options.Vault)
	}

	c.Lock()
	c.items[li.options.Name] = li
	c.Unlock()

	go func() {
		li.server.Listen()
	}()

}

func (c *Operation) newTCPListener(s *interfaces.RouterListener, h listenerInterfaces.TcpHandler) {
	li := &ListenerItems{options: s}
	li.server = &Server{
		listener:   c.listener,
		secure:     s.Secure,
		address:    fmt.Sprintf("%s:%d", s.Listen, s.Port),
		tcpHandler: h,
	}

	if s.Secure {
		li.server.tcpTlsConfig = c.listener.GetTLSCerts().NewConfig(
			fmt.Sprintf("router:%s", li.options.Name),
			s.Pem, s.AutoReloadPemDir, false, li.options.Vault)
	}

	c.Lock()
	c.items[li.options.Name] = li
	c.Unlock()

	go func() {
		_ = li.server.ListenTcp()
	}()

}

type Server struct {
	secure       bool
	Server       *http.Server
	address      string
	listener     listenerInterfaces.Listener
	tcpHandler   listenerInterfaces.TcpHandler
	serverTcp    *net.TCPListener
	tcpTlsConfig *tls.Config
}

func (c *Server) Listen() {

	opt := c.listener.GetOptions()
	c.Server.SetKeepAlivesEnabled(true)
	opt.Logger.Debug("starting router listener on port %s", c.Server.Addr)
	if h, p, err := net.SplitHostPort(c.Server.Addr); err == nil {
		if pp, err := strconv.Atoi(p); err == nil {

			go c.isRunning(h, pp)

			if err := http2.ConfigureServer(c.Server, nil); err != nil {
				opt.Logger.Error(err.Error()).Quit()
			}

			buf := bytes.NewBuffer(nil)
			logs := log.New(buf, "", log.LstdFlags)
			logs.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))

			if c.secure {
				go func() {
					for range time.Tick(100 * time.Millisecond) {
						scanner := bufio.NewScanner(buf)
						for scanner.Scan() {
							opt.Logger.Error(scanner.Text())
						}
						buf.Reset()
					}
				}()

				if err := c.Server.ListenAndServeTLS("", ""); err != nil {
					if err != http.ErrServerClosed {
						opt.Logger.Error(err.Error()).Quit()
					}
				}

			} else {
				if err := c.Server.ListenAndServe(); err != nil {
					if err != http.ErrServerClosed {
						opt.Logger.Error(err.Error()).Quit()
					}
				}
			}

		} else {
			opt.Logger.Error(err.Error()).Quit()
		}
	} else {
		opt.Logger.Error(err.Error()).Quit()
	}
}

func (c *Server) StopTcp() error {
	if c.serverTcp != nil {
		return c.serverTcp.Close()
	}
	return nil
}

func (c *Server) ListenTcp() error {
	opt := c.listener.GetOptions()
	if h, p, err := net.SplitHostPort(c.address); err == nil {
		opt.Logger.Debug("starting router listener on port %s", c.address)
		if pp, err := strconv.Atoi(p); err == nil {
			go c.isRunning(h, pp)

			addr, err := net.ResolveTCPAddr("tcp", c.address)
			if err != nil {
				opt.Logger.Error(err)
				return err
			}

			c.serverTcp, err = net.ListenTCP("tcp", addr)
			if err != nil {
				opt.Logger.Error(err)
				return err
			}

			defer func(serverTcp *net.TCPListener) {
				_ = serverTcp.Close()
			}(c.serverTcp)

			for {

				conn, e := c.serverTcp.AcceptTCP()
				if e != nil {
					opt.Logger.Error(e)
					return e
				}

				_ = conn.SetKeepAlive(false)

				go func() {

					info := &listenerInterfaces.TcpConnectionInformation{
						Secure: c.secure,
					}

					if c.secure {
						var clientHello *tls.ClientHelloInfo

						c.tcpTlsConfig.GetConfigForClient = func(argHello *tls.ClientHelloInfo) (*tls.Config, error) {
							clientHello = new(tls.ClientHelloInfo)
							*clientHello = *argHello
							return nil, nil
						}

						tlsConn := tls.Server(conn, c.tcpTlsConfig)
						_ = tlsConn.Handshake()
						info.ClientHello = clientHello
						c.tcpHandler(tlsConn, info, func() {})

					} else {
						c.tcpHandler(conn, info, func() {})
					}

				}()

			}

		}

	}

	return nil
}

func (c *Server) isRunning(host string, port int) {
	opt := c.listener.GetOptions()
	var isRunning bool
	if vs, err := netInfo.ConnectionsPid("tcp", int32(os.Getpid())); err == nil {
		for _, s := range vs {
			if s.Laddr.Port == uint32(port) {
				isRunning = true
				if !c.secure {
					opt.Logger.Success("router listener running on (unsecure) %s:%d",
						s.Laddr.IP, s.Laddr.Port)
				}

				if c.secure {
					opt.Logger.Success("router listener running on (secure) on %s:%d",
						s.Laddr.IP, s.Laddr.Port)
				}

				break
			}
		}
	}

	if !isRunning {
		time.Sleep(1 * time.Second)
		c.isRunning(host, port)
	}
}

type Header struct {
	LimiterID string
}
