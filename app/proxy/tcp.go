package proxy

import (
	"net"
	"strconv"
	"time"

	listenerinterfaces "code.afis.me/modules/go/listener/interfaces"
	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/utils/crypto/hash"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
)

// TODO
func DefaultTcpRouter(limit *limiter.Items, val *proxy.Listener) listenerinterfaces.TcpHandler {

	return func(conn net.Conn, info *listenerinterfaces.TcpConnectionInformation, onDone listenerinterfaces.TcpHandlerOnDone) {

		id := hash.CreateRandomId(10)
		st := time.Now()
		config.GetLogger().Trace("[%s] Start TCP request", id)

		limit.Rate.Take()

		defer func() {
			_ = conn.Close()
			limit.Rate.Done()
			config.GetLogger().Trace("[%s] Request TCP done in %s", id, time.Since(st))
			onDone()
		}()

		var isNano, foundBackend bool
		var backendName, routingName string

		if vv, ok := val.Labels["isNano"]; ok {
			if valNano, err := strconv.ParseBool(vv); err == nil {
				isNano = valNano
			}
		}

		if isNano {
			if routingVal, ok := config.Routing.Load(val.Name); ok {
				routingName = routingVal.Name
			}

		}

		if len(val.Routing) < 1 && len(routingName) == 0 {
			return
		}

		if len(routingName) == 0 && !isNano {
			routingName = val.Routing[0]
		}

		// check tls domain routing using sni
		if info != nil {
			if info.Secure {

				if s, ok := config.Routing.Load(val.Name); ok {
					for _, sv := range val.Routing {
						if s.Name == sv {

							if len(s.Domain) == 0 {
								for _, sm := range s.Routers {
									if len(sm.Backend) != 0 {
										foundBackend = true
										backendName = sm.Backend
										break
									}
								}
							} else {
								for _, sd := range s.Domain {
									if info.ClientHello != nil {
										if sd == info.ClientHello.ServerName {
											for _, sm := range s.Routers {
												if len(sm.Backend) != 0 {
													foundBackend = true
													backendName = sm.Backend
													break
												}
											}
										}
									}
								}
							}

						}
					}
				}

			}
		}

		if !foundBackend {
			if st, ok := config.Routing.Load(routingName); ok {
				if isNano {
					foundBackend = true
					backendName = st.Name
				}

				for _, s := range st.Routers {
					if len(s.Backend) != 0 {
						foundBackend = true
						backendName = s.Backend
						break
					}
				}

			}
		}

		if !foundBackend {
			return
		}

		pr := resolver.GetNextServer(backendName)

		if pr == nil {
			return
		}

		err := pr.HandleTcpConnection(conn, info)
		if err != nil {
			return
		}
	}
}
