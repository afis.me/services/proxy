package proxy

type Proxy struct {
	router *Router
}

func New() *Proxy {
	return &Proxy{
		router: NewRouter(),
	}
}

func (c *Proxy) Start() {
	c.router.Start()
}

func (c *Proxy) Stop() {
	c.router.Stop()
}
