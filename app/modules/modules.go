package modules

import (
	"bytes"
	listenerInterfaces "code.afis.me/modules/go/listener/interfaces"
	listenerLib "code.afis.me/modules/go/listener/lib"
	authInterceptor "code.afis.me/modules/go/listener/lib/transport/interceptor/auth"
	authInterfaces "code.afis.me/modules/go/listener/lib/transport/interceptor/auth"
	grpcUtil "code.afis.me/modules/go/utils/grpc"
	"code.afis.me/modules/go/utils/port"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/auth"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/modules/api"
	"code.afis.me/services/proxy/app/modules/filebrowser"
	"code.afis.me/services/proxy/app/modules/vanityurls"
	"context"
	"google.golang.org/grpc/codes"
	"net/http"
	"runtime/pprof"
	"sync"
	"time"
)

var mu sync.RWMutex
var modules map[string]interfaces.Modules

func New(routerListener listenerInterfaces.Listener, router interfaces.Router, auth *auth.Auth) {
	modules = make(map[string]interfaces.Modules)
	var modPort int
	if config.GetConfig().ModulesListener.Port == 0 {
		if vs, err := port.GetFreePort(); err == nil {
			modPort = vs
		}
	} else {
		modPort = config.GetConfig().ModulesListener.Port
	}

	lis := listenerLib.NewLib()
	options := listenerInterfaces.Options{
		Listen:         config.GetConfig().ModulesListener.Listen,
		Logger:         config.GetLogger(),
		Port:           listenerInterfaces.Port{},
		DisableSecure:  !config.GetConfig().ModulesListener.Secure,
		DisableMonitor: true,
		Pem:            config.GetConfig().ModulesListener.Pem,
	}

	if !config.GetConfig().ModulesListener.Secure {
		options.Port.Insecure = modPort
	} else {
		options.Port.Secure = modPort
	}

	lis.Init(options, authInterceptor.New(lis, AutInterceptor(auth)))
	lis.SetMonitor(routerListener.GetMonitor())
	lis.SetTracer(config.GetTracing())

	lis.RegisterHttpHandler("/profiling/heap", "GET", &DebugPprofHeap{})

	// setup docker internal client if available

	vanity := vanityurls.New()
	modules[vanity.ID()] = vanity
	config.SetRegisterModules(vanity.ID())

	fileBrowser := filebrowser.New()
	modules[fileBrowser.ID()] = fileBrowser
	config.SetRegisterModules(fileBrowser.ID())

	apiProxy := api.New(lis, router)
	modules[apiProxy.ID()] = apiProxy
	config.SetRegisterModules(apiProxy.ID())

	for _, mod := range modules {
		config.GetLogger().Debug("Registering modules id %s, %s", mod.ID(), mod.Name())
	}

	go func() {
		if err := lis.Run(); err != nil {
			config.GetLogger().Error(err)
		}
	}()

}

func Get(id string) (val interfaces.Modules, ok bool) {
	mu.RLock()
	val, ok = modules[id]
	mu.RUnlock()
	return val, ok
}

func AutInterceptor(auth *auth.Auth) func(ctx context.Context, r *http.Request) (nextCtx context.Context, res *authInterfaces.Response) {
	return func(ctx context.Context, r *http.Request) (nextCtx context.Context, res *authInterfaces.Response) {
		res = &authInterfaces.Response{Valid: false, Status: codes.Unauthenticated}

		meta := grpcUtil.ExtractIncoming(ctx)
		mu.RLock()
		mods := modules
		mu.RUnlock()
		for _, s := range mods {
			if enable, cfg := s.Protected(meta.Get("path")); enable {
				if cfg == nil {
					cfg = &proxy.AuthConfig{
						Mode:   "basicToken",
						Config: s.ID(),
					}
				}

				aut, _ := auth.Load(cfg, nil, r)
				valid, _ := aut.Valid()

				if valid {
					res.Valid = true
					return ctx, res
				}

				return ctx, res
			}
		}

		return ctx, res
	}
}

type DebugPprofHeap struct {
}

func (c *DebugPprofHeap) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	buf := bytes.NewBuffer(nil)
	time.Sleep(10 * time.Second)
	_ = pprof.WriteHeapProfile(buf)
	_, _ = w.Write(buf.Bytes())
	buf.Reset()
}
