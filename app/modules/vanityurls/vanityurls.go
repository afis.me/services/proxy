package vanityurls

import (
	"encoding/base64"
	"html/template"
	"net/http"
	"os"
	"sync"

	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
	staticTemplate "code.afis.me/services/proxy/static"
)

type Handlers struct {
	name    string
	handler *handler
	limiter *limiter.Items
}

type VanityUrls struct {
	template *template.Template
	handlers map[string]*Handlers
	sync.RWMutex
}

func New() interfaces.Modules {
	mod := &VanityUrls{
		handlers: make(map[string]*Handlers),
	}
	if err := mod.Init(); err != nil {
		config.GetLogger().Error(err)
	}
	return mod
}

func (c *VanityUrls) ID() string {
	return "modules.proxy.vanityUrls"
}

func (c *VanityUrls) Name() string {
	return "Vanity Urls"
}

func (c *VanityUrls) UseInternalService() bool {
	return true
}

func (c *VanityUrls) Protected(p string) (protected bool, config *proxy.AuthConfig) {
	return false, nil
}

func (c *VanityUrls) Init() (err error) {

	if dat, err := staticTemplate.Fs.ReadFile("dist/vanity.html"); err == nil {
		c.template = template.Must(template.New("vanity").Parse(string(dat)))
	} else {
		config.GetLogger().Error(err)
	}

	if len(config.GetConfig().VanityUrls) == 0 {
		return nil
	}

	for _, s := range config.GetConfig().VanityUrls {
		var maxRps = s.MaxRequest
		if (maxRps) <= 0 {
			maxRps = 100
		}

		c.Lock()
		c.handlers[s.Name] = &Handlers{
			name:    s.Name,
			handler: &handler{template: c.template},
			limiter: limiter.GetDefaultLimiter().New(ratelimit.Config{
				ID:    c.ID() + "-" + s.Name,
				Alias: s.Name,
				Type:  "modules",
				Max:   float64(maxRps),
			}),
		}

		if base64Data, err := base64.StdEncoding.DecodeString(s.Config); err == nil {
			c.handlers[s.Name].handler.configCache = base64Data
		} else if dat, err := os.ReadFile(s.Config); err == nil {
			c.handlers[s.Name].handler.configCache = dat
		}

		_, _ = c.handlers[s.Name].handler.UpdateHandler()

		c.Unlock()
	}

	return err
}

func (c *VanityUrls) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
		if value, ok := valCtx.(*interfaces.RequestContext); ok {

			c.RLock()
			val, ok := c.handlers[value.GetBackendName()]
			c.RUnlock()
			if ok {
				val.limiter.Rate.Take()
				defer val.limiter.Rate.Done()

				val.handler.ServeHTTP(w, r)
				return
			}
		}
	}

	utils.ErrorResponse(w, r, http.StatusNotFound, "Vanity server not found")
}
