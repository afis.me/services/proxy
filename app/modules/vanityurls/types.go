package vanityurls

type Config struct {
	Host     string `yaml:"host,omitempty" json:"host,omitempty"`
	CacheAge *int64 `yaml:"cache_max_age,omitempty" json:"cache_max_age,omitempty"`
	Paths    map[string]struct {
		Repo    string `yaml:"repo,omitempty" json:"repo,omitempty"`
		Display string `yaml:"display,omitempty" json:"display,omitempty"`
		VCS     string `yaml:"vcs,omitempty" json:"vcs,omitempty"`
	} `yaml:"paths,omitempty" json:"paths,omitempty"`
}
