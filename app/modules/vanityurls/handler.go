package vanityurls

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"sort"
	"strings"

	"code.afis.me/services/proxy/app/utils"
	"gopkg.in/yaml.v3"
)

type handler struct {
	template     *template.Template
	host         string
	cacheControl string
	configCache  []byte
	paths        pathConfigSet
	config       Config
}

type pathConfig struct {
	path    string
	repo    string
	display string
	vcs     string
}

var cacheAge = 10

type pathConfigSet []pathConfig

func (c pathConfigSet) Len() int {
	return len(c)
}

func (c pathConfigSet) Less(i, j int) bool {
	return c[i].path < c[j].path
}

func (c pathConfigSet) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c pathConfigSet) find(path string) (pc *pathConfig, subPath string) {
	// Fast path with binary search to retrieve exact matches
	// e.g. given c ["/", "/abc", "/xyz"], path "/def" won't match.
	i := sort.Search(len(c), func(i int) bool {
		return c[i].path >= path
	})
	if i < len(c) && c[i].path == path {
		return &c[i], ""
	}
	if i > 0 && strings.HasPrefix(path, c[i-1].path+"/") {
		return &c[i-1], path[len(c[i-1].path)+1:]
	}

	// Slow path, now looking for the longest prefix/shortest subPath i.e.
	// e.g. given c ["/", "/abc/", "/abc/def/", "/xyz"/]
	//  * query "/abc/foo" returns "/abc/" with a subPath of "foo"
	//  * query "/x" returns "/" with a subPath of "x"
	lenShortestSubPath := len(path)
	var bestMatchConfig *pathConfig

	// After binary search with the >= lexicographic comparison,
	// nothing greater than I will be a prefix of path.
	max := i
	for i := 0; i < max; i++ {
		ps := c[i]
		if len(ps.path) >= len(path) {
			// We previously didn't find the path by search, so any
			// route with equal or greater length is NOT a match.
			continue
		}
		sSubPath := strings.TrimPrefix(path, ps.path)
		if len(sSubPath) < lenShortestSubPath {
			subPath = sSubPath
			lenShortestSubPath = len(sSubPath)
			bestMatchConfig = &c[i]
		}
	}
	return bestMatchConfig, subPath
}

func (h *handler) UpdateHandler() (*handler, error) {
	if err := yaml.Unmarshal(h.configCache, &h.config); err != nil {
		return nil, err
	}
	h.host = h.config.Host
	cacheAge := int64(cacheAge)
	if h.config.CacheAge != nil {
		cacheAge = *h.config.CacheAge
		if cacheAge < 0 {
			return nil, errors.New("cache_max_age is negative")
		}
	}
	h.cacheControl = fmt.Sprintf("public, max-age=%d", cacheAge)
	var currentPath []pathConfig

	for path, e := range h.config.Paths {
		pc := pathConfig{
			path:    strings.TrimSuffix(path, "/"),
			repo:    e.Repo,
			display: e.Display,
			vcs:     e.VCS,
		}

		switch {
		case e.Display != "":
			// Already filled in.
		case pc.vcs == "github":
			fallthrough
		case pc.vcs == "gitlab":
			pc.vcs = "git"
			fallthrough
		case strings.HasPrefix(e.Repo, "https://github.com/"):
			pc.display = fmt.Sprintf("%v %v/tree/master{/dir} %v/blob/master{/dir}/{file}#L{line}", e.Repo, e.Repo, e.Repo)
		case pc.vcs == "bitbucket":
			pc.vcs = "git"
			fallthrough
		case strings.HasPrefix(e.Repo, "https://bitbucket.org"):
			pc.display = fmt.Sprintf("%v %v/src/default{/dir} %v/src/default{/dir}/{file}#{file}-{line}", e.Repo, e.Repo, e.Repo)
		}
		switch {
		case pc.vcs != "":
			// Already filled in.
			if pc.vcs != "bzr" && pc.vcs != "git" && pc.vcs != "hg" && pc.vcs != "svn" {
				return nil, fmt.Errorf("configuration for %v: unknown VCS %s", path, e.VCS)
			}
		case strings.HasPrefix(e.Repo, "https://github.com/"):
			pc.vcs = "git"
		default:
			return nil, fmt.Errorf("configuration for %v: cannot infer VCS from %s", path, e.Repo)
		}

		currentPath = append(currentPath, pc)

	}

	h.paths = currentPath
	sort.Sort(h.paths)

	return h, nil
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if h.template == nil {
		utils.ErrorResponse(w, r, http.StatusInternalServerError, "cannot render the page, vanity template is nil")
		return
	}

	current := r.URL.Path
	pc, subPath := h.paths.find(current)
	if pc == nil && current == "/" {
		h.serveIndex(w, r)
		return
	}
	if pc == nil {
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Cache-Control", h.cacheControl)
	if err := h.template.Execute(w, struct {
		Title   string
		Import  string
		SubPath string
		Repo    string
		Display string
		VCS     string
	}{
		Title:   "Vanity - " + h.Host(r) + pc.path,
		Import:  h.Host(r) + pc.path,
		SubPath: subPath,
		Repo:    pc.repo,
		Display: pc.display,
		VCS:     pc.vcs,
	}); err != nil {
		utils.ErrorResponse(w, r, http.StatusInternalServerError, fmt.Sprintf("cannot render the page: %s", err.Error()))
	}
}

func (h *handler) serveIndex(w http.ResponseWriter, r *http.Request) {
	if b, err := json.MarshalIndent(h.config, " ", "  "); err == nil {
		w.Header().Set("Content-Type", "application/json")
		_, _ = w.Write(b)
	}
}

func (h *handler) Host(r *http.Request) string {
	host := h.host
	if host == "" {
		host = defaultHost(r)
	}
	return host
}

func defaultHost(r *http.Request) string {
	return r.Host
}
