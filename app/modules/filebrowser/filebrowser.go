package filebrowser

import (
	"net/http"
	"sync"

	"code.afis.me/modules/go/listener/lib/limiter"
	"code.afis.me/modules/go/listener/lib/limiter/ratelimit"
	grpcUtil "code.afis.me/modules/go/utils/grpc"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
)

type Handlers struct {
	name    string
	handler http.Handler
	limiter *limiter.Items
}

type FileBrowser struct {
	handlers map[string]*Handlers
	sync.RWMutex
}

func New() interfaces.Modules {
	mod := &FileBrowser{
		handlers: make(map[string]*Handlers),
	}
	if err := mod.Init(); err != nil {
		config.GetLogger().Error(err)
	}
	return mod
}

func (c *FileBrowser) ID() string {
	return "modules.proxy.fileBrowser"
}

func (c *FileBrowser) Name() string {
	return "File Browser"
}

func (c *FileBrowser) UseInternalService() bool {
	return true
}

func (c *FileBrowser) Protected(p string) (protected bool, config *proxy.AuthConfig) {
	return false, nil
}

func (c *FileBrowser) Init() (err error) {
	if len(config.GetConfig().FileBrowser) == 0 {
		return nil
	}

	for _, s := range config.GetConfig().FileBrowser {
		var maxRps = s.MaxRequest
		if (maxRps) <= 0 {
			maxRps = 100
		}

		handler := c.NewFileBrowserHandler(s)
		c.Lock()
		c.handlers[s.Name] = &Handlers{
			name:    s.Name,
			handler: handler,
			limiter: limiter.GetDefaultLimiter().New(ratelimit.Config{
				ID:    c.ID() + "-" + s.Name,
				Alias: s.Name,
				Type:  "modules",
				Max:   float64(maxRps),
			}),
		}
		c.Unlock()
	}

	return err
}

func (c *FileBrowser) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
		if value, ok := valCtx.(*interfaces.RequestContext); ok {
			c.RLock()
			val, ok := c.handlers[value.GetBackendName()]
			c.RUnlock()
			if ok {
				val.limiter.Rate.Take()
				defer val.limiter.Rate.Done()

				if grpcUtil.IsGrpcRequest(r) {
					utils.ErrorResponse(w, r, http.StatusNotFound, "Resource not found")
					return
				}

				val.handler.ServeHTTP(w, r)
				return
			}
		}
	}

	utils.ErrorResponse(w, r, http.StatusNotFound, "Resource not found")
}
