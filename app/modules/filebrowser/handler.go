package filebrowser

import (
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"code.afis.me/modules/go/listener/lib/transport/http/middleware"
	"code.afis.me/services/proxy/app/assets"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
)

func (c *FileBrowser) NewFileBrowserHandler(cfg config.FileBrowser) http.Handler {
	return middleware.CompressHandler(nil, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if valCtx := r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
			if value, ok := valCtx.(*interfaces.RequestContext); ok {
				ph := strings.TrimPrefix(r.URL.Path, value.GetPathMatch())
				if len(ph) == 0 {
					ph = "/"
				}

				pll := filepath.Join(cfg.Directory, ph)
				fpl := filepath.Join(cfg.Directory, r.URL.Path)

				if utils.IsStatic(r.URL.Path) {
					w.Header().Set("Cache-Control", "max-age=31536000")
				}

				if ss, err := os.Stat(filepath.Clean(fpl)); err == nil {
					if !ss.IsDir() {
						http.ServeFile(w, r, filepath.Clean(fpl))
						return
					}
				}

				if ss, err := os.Stat(filepath.Clean(pll)); err == nil {
					if ss.IsDir() {
						pIndex := filepath.Join(pll, "index.html")
						if _, err := os.Stat(filepath.Clean(pIndex)); err == nil {
							http.ServeFile(w, r, filepath.Clean(pIndex))
							return
						}

						w.WriteHeader(http.StatusForbidden)
						_, _ = w.Write(assets.GenerateError(os.Getenv("ServiceID"),
							r.Header.Get("RequestID"), http.StatusForbidden, assets.ErrorMsg{}))
						return
					}

					http.ServeFile(w, r, filepath.Clean(pll))
					return

				}

				// return to index if available
				if cfg.ReturnToIndex {
					if len(mime.TypeByExtension(filepath.Ext(ph))) == 0 {
						pll := filepath.Join(cfg.Directory, "index.html")
						if _, err := os.Stat(filepath.Clean(pll)); err == nil {
							http.ServeFile(w, r, filepath.Clean(pll))
							return
						}
					}
				}

			}
		}

		utils.ErrorResponse(w, r, http.StatusNotFound, "Resource not found")

	}))
}
