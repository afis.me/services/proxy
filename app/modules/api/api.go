package api

import (
	"context"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	listenerInterfaces "code.afis.me/modules/go/listener/interfaces"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/modules/api/handler"
	"code.afis.me/services/proxy/app/modules/api/models"
	"google.golang.org/protobuf/proto"
)

type Api struct {
	routerListener listenerInterfaces.Listener
	router         interfaces.Router
	sync.RWMutex
}

func New(lis listenerInterfaces.Listener, router interfaces.Router) interfaces.Modules {
	mod := &Api{
		routerListener: lis,
		router:         router,
	}

	if err := mod.Init(); err != nil {
		config.GetLogger().Error(err)
	}

	return mod
}

func (c *Api) ID() string {
	return "modules.proxy.api"
}

func (c *Api) Name() string {
	return "Api controllers"
}

func (c *Api) UseInternalService() bool {
	return false
}

func (c *Api) Protected(p string) (protected bool, cfg *proxy.AuthConfig) {
	apiConfig := config.GetConfig().ApiController
	if apiConfig.Protected {
		sp := strings.Split(p, "/")
		if len(sp) >= 2 {
			if sp[1] == "proxy.ApiServices" {
				return true, apiConfig.AuthConfig
			}
		}
	}
	return false, nil
}

func (c *Api) Init() (err error) {
	if !config.GetConfig().ApiController.Enable {
		return nil
	}

	config.GetLogger().Debug("configure module %s", c.ID())

	// init models
	m := models.New()
	err = m.Init()
	if err != nil {
		return err
	}

	c.routerListener.RegisterMultipleHandler(handler.NewServices(c.routerListener, m, c.router))

	// insert data in db
	res, err := m.GetAllData(false, context.Background())
	if err != nil {
		config.GetLogger().Error(err)
		return err
	}

	for _, s := range res {
		if s.Type == "backend" {
			var data proxy.Backend
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			isNano, _ := strconv.ParseBool(data.Labels["isNano"])
			if !isNano {
				if err := c.router.AddBackend(&data); err != nil {
					config.GetLogger().Error(err)
					return err
				}
			}

		}
	}

	for _, s := range res {
		if s.Type == "server" {
			var data proxy.Server
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			isNano, _ := strconv.ParseBool(data.Labels["isNano"])
			if !isNano {
				if err := c.router.AddServer(&data); err != nil {
					config.GetLogger().Error(err)
					return err
				}
			}

		}
	}

	for _, s := range res {
		if s.Type == "routing" {
			var data proxy.Routing
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			isNano, _ := strconv.ParseBool(data.Labels["isNano"])
			if !isNano {
				if err := c.router.AddRouting(&data); err != nil {
					config.GetLogger().Error(err)
					return err
				}
			}

		}
	}

	for _, s := range res {
		if s.Type == "listener" {
			var data proxy.Listener
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			if err := c.router.AddListener(&data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

		}
	}

	// for nano
	for _, s := range res {
		if s.Type == "routing" {
			var data proxy.Routing
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			isNano, _ := strconv.ParseBool(data.Labels["isNano"])
			if isNano {
				if err := c.router.AddRouting(&data); err != nil {
					config.GetLogger().Error(err)
					return err
				}
			}

		}
	}

	for _, s := range res {
		if s.Type == "backend" {
			var data proxy.Backend
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			isNano, _ := strconv.ParseBool(data.Labels["isNano"])
			if isNano {
				if err := c.router.AddBackend(&data); err != nil {
					config.GetLogger().Error(err)
					return err
				}
			}

		}
	}

	for _, s := range res {
		if s.Type == "server" {
			var data proxy.Server
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return err
			}

			isNano, _ := strconv.ParseBool(data.Labels["isNano"])
			if isNano {
				if err := c.router.AddServer(&data); err != nil {
					config.GetLogger().Error(err)
					return err
				}
			}

		}
	}

	go func() {
		for range time.NewTicker(1 * time.Second).C {
			for _, s := range config.GetQueServerRemove() {
				err = m.RemoveData(false, context.Background(), s, "server")
				if err != nil {
					config.GetLogger().Error(err)
				} else {
					config.RemoveQueServerRemove(s)
				}
			}
		}
	}()

	return nil
}

func (c *Api) ServeHTTP(w http.ResponseWriter, r *http.Request) {
}
