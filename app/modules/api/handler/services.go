package handler

import (
	"context"
	"os"
	"path/filepath"
	"sync"

	listenerInterfaces "code.afis.me/modules/go/listener/interfaces"
	tracing "code.afis.me/modules/go/tracing/interfaces"
	"code.afis.me/modules/go/utils/validator"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Services struct {
	listener    listenerInterfaces.Listener
	models      interfaces.ApiModels
	router      interfaces.Router
	serviceDesc []*grpc.ServiceDesc
	dockerName  string
	proxy.UnimplementedApiServicesServer
	sync.RWMutex
}

var services *Services

func GetServices() *Services {
	return services
}

func NewServices(lis listenerInterfaces.Listener, m interfaces.ApiModels, router interfaces.Router) *Services {
	services = &Services{
		dockerName: os.Getenv("docker-client"),
		listener:   lis,
		models:     m,
		router:     router,
		serviceDesc: []*grpc.ServiceDesc{
			&proxy.ApiServices_ServiceDesc,
		},
	}

	return services
}

func (c *Services) GetServiceDesc() []*grpc.ServiceDesc {
	return c.serviceDesc
}

func (c *Services) AllowMethod(method string) (m []listenerInterfaces.Method) {
	m = append(m, listenerInterfaces.MethodGET, listenerInterfaces.MethodPOST)
	return m
}

func (c *Services) GetModel() interfaces.ApiModels {
	return c.models
}

func (c *Services) ProxyInfo(ctx context.Context, req *emptypb.Empty) (res *proxy.ProxyInfoResponse, err error) {
	res = &proxy.ProxyInfoResponse{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers ProxyInfo"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if config.GetOnCloseRequest() {
		return res, status.Error(codes.Aborted, "server shutdown in progress")
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	res.StoragePath, err = filepath.Abs(filepath.Join(config.GetConfig().DataDirectory, "nodes"))
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}
