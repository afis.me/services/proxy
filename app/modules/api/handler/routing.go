package handler

import (
	"context"
	"strings"

	tracing "code.afis.me/modules/go/tracing/interfaces"
	"code.afis.me/modules/go/utils/validator"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (c *Services) GetRouting(ctx context.Context, req *emptypb.Empty) (res *proxy.GetRoutingResponse, err error) {
	res = &proxy.GetRoutingResponse{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers GetRouting"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	res.Routing = config.Routing.LoadAll()

	return res, nil
}

func (c *Services) AddRouting(ctx context.Context, req *proxy.Routing) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers AddRouting"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	err = c.router.AddRouting(req)
	if err != nil {
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	msg, err := proto.Marshal(req)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
	}

	err = c.models.AddData(true, ctx, &proxy.Configstore{
		Name: req.Name,
		Type: "routing",
		Data: msg,
	})

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (c *Services) RemoveRouting(ctx context.Context, req *proxy.RemoveRoutingRequest) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers RemoveRouting"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	if !req.Force {
		var listener []string
		for _, s := range config.GetListenerList() {
			for _, sv := range s.Routing {
				if sv == req.Name {
					listener = append(listener, s.Name)
					break
				}
			}
		}
		if len(listener) != 0 {
			return res, status.Errorf(codes.FailedPrecondition, "routing name %s used by listener %s",
				req.Name, strings.Join(listener, ","))
		}
	}

	if err := c.router.RemoveRouting(req.Name); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	err = c.models.RemoveData(true, ctx, req.Name, "routing")

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}
