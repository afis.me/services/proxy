package handler

import (
	tracing "code.afis.me/modules/go/tracing/interfaces"
	"code.afis.me/modules/go/utils/validator"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (c *Services) AddListener(ctx context.Context, req *proxy.Listener) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers AddListener"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	switch req.Mode {
	case "http":
	default:
		return res, status.Errorf(codes.FailedPrecondition, "only Mode http is supported by api for now, use config directly")
	}

	switch req.Mode {
	case "http", "tcp":
		if req.Port == 0 {
			return res, status.Errorf(codes.FailedPrecondition, "port is required for Mode http or tcp")
		}
	}

	err = c.router.AddListener(req)
	if err != nil {
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	msg, err := proto.Marshal(req)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
	}

	err = c.models.AddData(true, ctx, &proxy.Configstore{
		Name: req.Name,
		Type: "listener",
		Data: msg,
	})

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (c *Services) GetListener(ctx context.Context, req *emptypb.Empty) (res *proxy.GetListenerResponse, err error) {
	res = &proxy.GetListenerResponse{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers GetListener"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	res.Listener = config.GetListenerList()

	return res, nil
}

func (c *Services) RemoveListener(ctx context.Context, req *proxy.RemoveListenerRequest) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers RemoveListener"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	var valid bool
	for _, s := range config.GetListenerList() {
		if s.Name == req.Name {
			switch s.Mode {
			case "http", "tcp":
				valid = true
			}
		}
	}

	if !valid {
		return res, status.Error(codes.FailedPrecondition, "listener not valid to remove")
	}

	if err := c.router.RemoveListener(req.Name); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	err = c.models.RemoveData(true, ctx, req.Name, "listener")

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}
