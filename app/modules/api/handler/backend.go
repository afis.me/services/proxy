package handler

import (
	"context"
	"errors"

	tracing "code.afis.me/modules/go/tracing/interfaces"
	"code.afis.me/modules/go/utils/validator"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/emptypb"
	"gorm.io/gorm"
)

func (c *Services) GetBackend(ctx context.Context, req *emptypb.Empty) (res *proxy.GetBackendResponse, err error) {
	res = &proxy.GetBackendResponse{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers GetBackend"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	res.Backend = config.Backend.LoadAll()

	return res, nil
}

func (c *Services) AddBackend(ctx context.Context, req *proxy.Backend) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers AddBackend"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
		ctx = idt.OutgoingContext()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	err = c.router.AddBackend(req)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	msg, err := proto.Marshal(req)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
	}

	err = c.models.AddData(true, ctx, &proxy.Configstore{
		Name: req.Name,
		Type: "backend",
		Data: msg,
	})

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (c *Services) RemoveBackend(ctx context.Context, req *proxy.RemoveBackendRequest) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers RemoveBackend"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
		ctx = idt.OutgoingContext()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	// validate if exist in database
	_, err = c.models.FindDataByTypeAndName(true, ctx, "backend", req.Name)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		if errors.Is(err, gorm.ErrRecordNotFound) {
			if req.RemoveByDevelopment {
				return res, nil
			}
			return res, status.Errorf(codes.FailedPrecondition, "backend %s not found in database", req.Name)
		}
		return res, status.Error(codes.Internal, err.Error())
	}

	if err := c.router.RemoveBackend(req.Name); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	err = c.models.RemoveData(true, ctx, req.Name, "backend")

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (c *Services) AddServer(ctx context.Context, req *proxy.Server) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers AddServer"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
		// ctx = idt.OutgoingContext()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	if err := c.router.AddServer(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	msg, err := proto.Marshal(req)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
	}

	err = c.models.AddData(true, ctx, &proxy.Configstore{
		Name: req.Name,
		Type: "server",
		Data: msg,
	})

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, err
}

func (c *Services) RemoveServer(ctx context.Context, req *proxy.RemoveServerRequest) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers RemoveServer"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
		ctx = idt.OutgoingContext()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	// validate if exist in database
	_, err = c.models.FindDataByTypeAndName(true, ctx, "server", req.Name)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		if errors.Is(err, gorm.ErrRecordNotFound) {
			if req.RemoveByDevelopment {
				return res, nil
			}
			return res, status.Errorf(codes.FailedPrecondition, "server %s not found in database", req.Name)
		}
		return res, status.Error(codes.Internal, err.Error())
	}

	if err := c.router.RemoveServer(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	err = c.models.RemoveData(true, ctx, req.Name, "server")

	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (c *Services) RemoveAllServer(ctx context.Context, req *emptypb.Empty) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers RemoveAllServer"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
		ctx = idt.OutgoingContext()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	allData, err := c.models.GetAllData(true, ctx)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return res, status.Error(codes.Internal, err.Error())
		}

	}

	for _, s := range allData {
		if s.Type == "server" {
			var data proxy.Server
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return res, err
			}

			if err := c.router.RemoveServer(&proxy.RemoveServerRequest{
				Name:        data.Name,
				BackendName: data.Backend,
			}); err != nil {
				if idt != nil {
					idt.Error(err)
				}
				config.GetLogger().Error(err)
				return res, status.Error(codes.FailedPrecondition, err.Error())
			}

			err = c.models.RemoveData(true, ctx, s.Name, "server")

		}
	}

	return res, err
}

func (c *Services) GetServer(ctx context.Context, req *emptypb.Empty) (res *proxy.GetServerResponse, err error) {
	res = &proxy.GetServerResponse{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers RemoveAllServer"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
		ctx = idt.OutgoingContext()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	allData, err := c.models.GetAllData(true, ctx)
	if err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		if err != gorm.ErrRecordNotFound {
			return res, status.Error(codes.Internal, err.Error())
		}

	}

	for _, s := range allData {
		if s.Type == "server" {
			var data proxy.Server
			if err := proto.Unmarshal(s.Data, &data); err != nil {
				config.GetLogger().Error(err)
				return res, err
			}
			res.Server = append(res.Server, &data)
		}
	}

	return res, err
}

func (c *Services) SuspendServer(ctx context.Context, req *proxy.SuspendServerRequest) (res *emptypb.Empty, err error) {
	res = &emptypb.Empty{}
	tr := config.GetTracing()
	var idt tracing.Interaction
	if tr != nil {
		idt = tr.Operation(ctx,
			tracing.NewInteractionName("Controllers SuspendServer"),
			tracing.NewInteractionTypeType("controllers"))
		defer idt.Finish()
	}

	if err := validator.GetValidator().Struct(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	if err := c.router.SuspendServer(req); err != nil {
		if idt != nil {
			idt.Error(err)
		}
		config.GetLogger().Error(err)
		return res, status.Error(codes.FailedPrecondition, err.Error())
	}

	return res, err
}
