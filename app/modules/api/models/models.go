package models

import (
	"context"
	"time"

	tracing "code.afis.me/modules/go/tracing/interfaces"

	databaseInterfaces "code.afis.me/modules/go/database/interfaces"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/adapter/database"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"gorm.io/gorm/clause"
)

type models struct {
	db databaseInterfaces.SQL
}

func New() interfaces.ApiModels {
	return &models{}
}

func (c *models) Init() error {

	c.db = database.GetORM()

	ctxDb, cancel := c.newContextTimeout()
	defer cancel()

	if err := c.db.Gorm().WithContext(ctxDb).
		Debug().AutoMigrate(&proxy.Configstore{}); err != nil {
		return err
	}

	return nil
}

func (c *models) newContextTimeout() (ctx context.Context, cancel context.CancelFunc) {
	return context.WithTimeout(context.Background(),
		time.Duration(config.GetConfig().Database.TimeoutTransaction)*time.Millisecond)
}

func (c *models) GetAllData(enableTracing bool, ctx context.Context) (res []*proxy.Configstore, err error) {
	var idt tracing.Interaction
	if enableTracing {
		tr := config.GetTracing()
		if tr != nil {
			idt = tr.Operation(ctx,
				tracing.NewInteractionName("Models GetAllData"),
				tracing.NewInteractionTypeType("models"))
			defer idt.Finish()
		}
	}

	ctxDb, cancel := c.newContextTimeout()
	defer cancel()

	tx := c.db.Gorm().WithContext(ctxDb).Debug().Model(&proxy.Configstore{}).
		Find(&res)
	if tx.Error != nil {
		if enableTracing {
			if idt != nil {
				idt.Error(tx.Error)
			}
		}
		return res, tx.Error
	}

	return res, err
}

func (c *models) FindDataByTypeAndName(enableTracing bool, ctx context.Context, typ string, name string) (res *proxy.Configstore, err error) {
	var idt tracing.Interaction
	if enableTracing {
		tr := config.GetTracing()
		if tr != nil {
			idt = tr.Operation(ctx,
				tracing.NewInteractionName("Models FindDataByTypeAndName"),
				tracing.NewInteractionTypeType("models"))
			defer idt.Finish()
		}
	}

	ctxDb, cancel := c.newContextTimeout()
	defer cancel()

	tx := c.db.Gorm().WithContext(ctxDb).Debug().Model(&proxy.Configstore{}).
		Where(map[string]interface{}{"type": typ, "name": name}).
		First(&res)
	if tx.Error != nil {
		if enableTracing {
			if idt != nil {
				idt.Error(tx.Error)
			}
		}
		return res, tx.Error
	}

	return res, err
}

func (c *models) AddData(enableTracing bool, ctx context.Context, req *proxy.Configstore) (err error) {
	var idt tracing.Interaction
	if enableTracing {
		tr := config.GetTracing()
		if tr != nil {
			idt = tr.Operation(ctx,
				tracing.NewInteractionName("Models AddData"),
				tracing.NewInteractionTypeType("models"))
			defer idt.Finish()
			idt.SetLog("Request Data", req)
		}
	}

	ctxDb, cancel := c.newContextTimeout()
	defer cancel()

	tx := c.db.Gorm().WithContext(ctxDb).Debug().Model(&proxy.Configstore{}).
		Clauses(clause.OnConflict{
			DoUpdates: clause.Assignments(map[string]interface{}{
				"data": req.Data,
			}),
		}).
		Create(req)

	if tx.Error != nil {
		if enableTracing {
			if idt != nil {
				idt.Error(tx.Error)
			}
		}
		return tx.Error
	}

	return err
}

func (c *models) RemoveData(enableTracing bool, ctx context.Context, name string, typ string) (err error) {
	var idt tracing.Interaction
	if enableTracing {
		tr := config.GetTracing()
		if tr != nil {
			idt = tr.Operation(ctx,
				tracing.NewInteractionName("Models RemoveData"),
				tracing.NewInteractionTypeType("models"))
			defer idt.Finish()
			idt.SetLog("Request Data", map[string]interface{}{
				"name": name,
				"type": typ,
			})
		}
	}

	ctxDb, cancel := c.newContextTimeout()
	defer cancel()

	tx := c.db.Gorm().WithContext(ctxDb).Debug().
		Where(map[string]interface{}{
			"name": name,
			"type": typ,
		}).Delete(&proxy.Configstore{})

	if tx.Error != nil {
		if enableTracing {
			if idt != nil {
				idt.Error(tx.Error)
			}
		}
		return tx.Error
	}

	return err
}
