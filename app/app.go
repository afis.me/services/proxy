package app

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	cliInterfaces "code.afis.me/modules/go/cli/interfaces"
	cliLib "code.afis.me/modules/go/cli/lib"
	logInterfaces "code.afis.me/modules/go/logger/interfaces"
	"code.afis.me/modules/go/utils/crypto/hash"
	"code.afis.me/services/proxy/app/adapter/database"
	"code.afis.me/services/proxy/app/assets"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/proxy"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (c *App) Start() {
	_ = os.Setenv("docker-client", "docker-internal")
	if len(os.Getenv("ServiceID")) == 0 {
		err := os.Setenv("ServiceID", hash.CreateRandomId(10))
		if err != nil {
			return
		}
	}

	cli := cliLib.NewLib()
	cli.Init(cliInterfaces.Options{
		Name:        interfaces.Name,
		Version:     interfaces.Version,
		OSBuildName: interfaces.OSBuildName,
		BuildDate:   interfaces.BuildDate,
		Logger:      config.GetLogger(),
	})

	cli.SetConfig(config.GetConfig())

	cli.SetDefaultAction(func(ctx interface{}) (err error) {
		config.Setup()
		if config.GetConfig().ShowBanner {
			cli.ShowVersion()
		} else {
			if ver := cli.GetVersion(); ver != nil {
				config.GetLogger().Info(*ver)
			}
		}

		config.GetLogger().Warning("Using data directory %s", config.GetConfig().DataDirectory)
		assets.New()
		database.New()
		pr := proxy.New()
		pr.Start()
		cli.RegisterClean(pr.Stop, config.GetTracing().Closing, config.GetLogger().Close)
		return nil
	})

	cli.SetCommand([]*cliInterfaces.Command{
		{
			Name:        "config",
			Description: "Generated config for applications",
			Action: func(ctx interface{}) (err error) {
				config.Setup()
				basedir := strings.Split(filepath.Dir(logInterfaces.GetCaller(1).File), "/")
				if len(basedir) > 0 {
					basedir = basedir[:len(basedir)-1]
				}
				pss := filepath.Join(strings.Join(basedir, "/"), "generated", "config.yaml")
				cli.GenerateConfig(config.GetConfig(), pss)
				config.GetLogger().Quit()
				return nil
			},
		},
	})

	if err := cli.Run(); err != nil {
		config.GetLogger().Error(err).Quit()
	}

}

func (c *App) RunCmd(cm string, commands []string) {
	cmd := exec.Command(cm, commands...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	// cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		config.GetLogger().Error(err).Quit()
	}
}
