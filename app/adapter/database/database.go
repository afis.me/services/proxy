package database

import (
	"os"
	"path/filepath"

	databaseInterfaces "code.afis.me/modules/go/database/interfaces"
	databaseLib "code.afis.me/modules/go/database/lib"
	"code.afis.me/services/proxy/app/config"
)

var database databaseInterfaces.Database
var ormDb databaseInterfaces.SQL

func New() {
	database = databaseLib.NewLib()
	database.Init(config.GetLogger())
	dbCfg := config.GetConfig().Database
	ffp := filepath.Join(config.GetConfig().DataDirectory, "dbStore")
	err := os.MkdirAll(filepath.Clean(ffp), 0750)
	if err != nil {
		config.GetLogger().Error(err).Quit()
	}

	dbpath := filepath.Join(ffp, dbCfg.DBPath)
	dbCfg.DBPath = dbpath
	ormDb = GetDatabase().LoadSQL("proxy", databaseInterfaces.SQLConfig{
		Driver:             dbCfg.Driver,
		Enable:             dbCfg.Enable,
		Host:               dbCfg.Host,
		Port:               dbCfg.Port,
		Username:           dbCfg.Username,
		Password:           dbCfg.Password,
		Database:           dbCfg.Database,
		Options:            dbCfg.Options,
		Connection:         dbCfg.Connection,
		AutoReconnect:      dbCfg.AutoReconnect,
		StartInterval:      dbCfg.StartInterval,
		MaxError:           dbCfg.MaxError,
		DBPath:             dbCfg.DBPath,
		SsLMode:            dbCfg.SslMode,
		TimeoutConnection:  dbCfg.TimeoutConnection,
		TimeoutTransaction: dbCfg.TimeoutTransaction,
	})

	if err := ormDb.Start(); err != nil {
		config.GetLogger().Error(err).Quit()
	}

}

func GetORM() databaseInterfaces.SQL {
	return ormDb
}

func GetDatabase() databaseInterfaces.Database {
	return database
}
