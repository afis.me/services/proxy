package docker

import (
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	dClient "github.com/docker/docker/client"
	"strings"
	"sync"
	"time"
)

type dockerClient struct {
	clients        map[string]*clientItems
	defaultTimeout int
	sync.RWMutex
}

var Client *dockerClient

type clientItems struct {
	node *interfaces.Node
	conn *dClient.Client
}

func init() {
	Client = &dockerClient{
		defaultTimeout: 10,
		clients:        map[string]*clientItems{},
	}
}

func Get() interfaces.Docker {
	return Client
}

func (c *dockerClient) AddClient(node *interfaces.Node) (err error) {
	config.GetLogger().Info("registering docker client %s", node.Name)
	cli, err := dClient.NewClientWithOpts(dClient.FromEnv)
	if err != nil {
		return err
	}
	c.Lock()
	c.clients[node.Name] = &clientItems{node: node, conn: cli}
	c.Unlock()
	return nil
}

func (c *dockerClient) NodeContainers(nodeName string) (info []types.Container, err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return info, fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()
	in, err := client.conn.ContainerList(ctx, types.ContainerListOptions{All: true})
	if err != nil {
		return info, err
	}
	return in, nil
}

func (c *dockerClient) NodeInfo(nodeName string) (info types.Info, err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return info, fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()
	in, err := client.conn.Info(ctx)
	if err != nil {
		return info, err
	}
	return in, nil
}

func (c *dockerClient) NodeNetworks(nodeName string) (res []types.NetworkResource, err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return res, fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	data, err := client.conn.NetworkList(ctx, types.NetworkListOptions{})
	if err != nil {
		return res, err
	}

	return data, err
}

func (c *dockerClient) CreateNodeNetwork(nodeName, networkName string) (err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	_, err = client.conn.NetworkCreate(ctx, networkName, types.NetworkCreate{
		Labels: map[string]string{
			"nano-network": "true",
		},
	})
	return err
}

func (c *dockerClient) RemoveNodeNetwork(nodeName, networkName string) (err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()
	err = client.conn.NetworkRemove(ctx, networkName)
	return err
}

func (c *dockerClient) RemoveNodeVolume(nodeName string, id string, force bool) (err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()
	err = client.conn.VolumeRemove(ctx, id, force)
	return err
}

func (c *dockerClient) NodeImages(nodeName string) (info []types.ImageSummary, err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return info, fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	in, err := client.conn.ImageList(ctx, types.ImageListOptions{All: true})
	if err != nil {
		return info, err
	}

	return in, nil
}

func (c *dockerClient) RemoveNodeImage(nodeName string, id string, opt types.ImageRemoveOptions) (err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	_, err = client.conn.ImageRemove(ctx, id, opt)
	return err
}

func (c *dockerClient) RemoveNodeContainer(nodeName string, containerName string, ignoreNotfound bool, opt types.ContainerRemoveOptions) (err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}

	info, err := c.NodeContainers(nodeName)
	if err != nil {
		return err
	}

	var containerID string
	for _, s := range info {
		for _, sv := range s.Names {
			if strings.TrimLeft(sv, "/") == containerName {
				containerID = s.ID
			}
		}
	}

	if len(containerID) == 0 {
		if ignoreNotfound {
			config.GetLogger().Warning("container name %s not found, ignored", containerName)
			return nil
		}

		return fmt.Errorf("container name %s not found", containerName)
	}

	config.GetLogger().Debug("Removing container id %s", containerID)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	err = client.conn.ContainerRemove(ctx, containerID, opt)
	return nil
}

func (c *dockerClient) RunNodeContainer(nodeName string, containerID string) (err error) {

	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	if err := client.conn.ContainerStart(ctx, containerID, types.ContainerStartOptions{}); err != nil {
		err = fmt.Errorf("failed to run container : %s", err.Error())
		return err
	}

	return nil
}

func (c *dockerClient) InspectNodeContainer(nodeName string, containerID string) (res types.ContainerJSON, err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return res, fmt.Errorf("client (%s) not found", nodeName)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()
	res, err = client.conn.ContainerInspect(ctx, containerID)
	if err != nil {
		err = fmt.Errorf("failed to inspect container : %s", err.Error())
		return res, err
	}

	return res, nil
}

func (c *dockerClient) StopNodeContainer(nodeName string, containerID string) (err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	var timeout int = 30
	if err := client.conn.ContainerStop(ctx, containerID, container.StopOptions{Timeout: &timeout}); err != nil {
		return err
	}

	return nil
}

func (c *dockerClient) FindContainerByNetwork(nodeName string, networkID string) (res []types.Container, err error) {
	c.RLock()
	client, ok := c.clients[nodeName]
	c.RUnlock()
	if !ok {
		return res, fmt.Errorf("client (%s) not found", nodeName)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.defaultTimeout)*time.Second)
	defer cancel()

	args := filters.NewArgs()
	args.Add("status", "running")
	args.Add("network", networkID)

	list, err := client.conn.ContainerList(ctx, types.ContainerListOptions{
		Filters: args,
	})
	if err != nil {
		return res, err
	}

	return list, err
}
