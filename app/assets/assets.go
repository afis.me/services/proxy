package assets

import (
	"bytes"
	staticTemplate "code.afis.me/services/proxy/static"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"code.afis.me/services/proxy/app/config"
)

type ErrorData struct {
	Code            int
	Description     string
	Title           string
	Message         string
	ServiceID       string
	RequestID       string
	WithLoginButton bool
	HaveDetails     bool
	Details         string
	LoginUrl        string
}

type ErrorMsg struct {
	Status     string
	GRPCStatus int `json:"-"`
	Message    string
}

var errorTpl *template.Template

func New() {
	byPass, _ := strconv.ParseBool(os.Getenv("STATIC_BYPASS"))
	if !byPass {
		log.Println("list static assets")
		dir, err := staticTemplate.Fs.ReadDir(".")
		if err == nil {
			for _, s := range dir {
				log.Println("fileName:", s.Name(), "isDirectory:", s.IsDir())
			}
		}

		if as, err := staticTemplate.Fs.ReadFile("dist/error.html"); err == nil {
			errorTpl = template.Must(template.New("error").Parse(string(as)))
		} else {
			config.GetLogger().Warning("failed to get error template: %s", err.Error())
		}
	}

}

func GenerateError(serviceId, requestId string, code int, eMSg ErrorMsg) []byte {
	return makeTemplate(serviceId, requestId, code, eMSg, false, false, "", "")
}

func GenerateLogin(serviceId, requestId string, code int, msg, details string, withLoginButton bool, loginUrl string) []byte {
	var haveDetails bool
	if len(details) != 0 {
		haveDetails = true
	}
	return makeTemplate(serviceId, requestId, code, ErrorMsg{
		Message: msg,
	}, withLoginButton, haveDetails, details, loginUrl)
}

func makeTemplate(serviceId, requestId string, code int, eMSg ErrorMsg, withLoginButton bool, haveDetails bool, details string, loginUrl string) []byte {
	byPass, _ := strconv.ParseBool(os.Getenv("STATIC_BYPASS"))
	if byPass {
		devFpp := filepath.Join("static/dist", "error.html")
		if fs, err := os.Stat(devFpp); err == nil {
			if !fs.IsDir() {
				if dat, err := os.ReadFile(devFpp); err == nil {
					errorTpl = template.Must(template.New("error").Parse(string(dat)))
				}
			}
		}
	}

	var errData ErrorData
	switch code {
	case http.StatusForbidden:
		errData = ErrorData{
			Code:        http.StatusForbidden,
			Title:       "Forbidden Access",
			Description: "Forbidden Access",
			Message:     "Sorry, You don't have permission to access this",
		}
	case http.StatusServiceUnavailable:
		errData = ErrorData{
			Code:        http.StatusServiceUnavailable,
			Title:       "Service Unavailable",
			Description: "Service Unavailable",
			Message:     "No server available to handle this request",
		}
	case http.StatusUnauthorized:
		errData = ErrorData{
			Code:        http.StatusUnauthorized,
			Title:       "Unauthorized",
			Description: "Unauthorized",
			Message:     "Sorry, You don't have permission to access this",
		}
	case http.StatusInternalServerError:
		errData = ErrorData{
			Code:        http.StatusInternalServerError,
			Title:       "Internal Server Error",
			Description: "Something When Wrong",
			Message:     "Something When Wrong, contact your administrator",
		}
	case http.StatusNotFound:
		errData = ErrorData{
			Code:        http.StatusNotFound,
			Title:       "Resource Not Found",
			Description: "Resource Not Found",
			Message:     "Your Requested Resource Not Found",
		}
	}

	if len(eMSg.Message) != 0 {
		errData.Message = eMSg.Message
	}

	errData.WithLoginButton = withLoginButton
	errData.HaveDetails = haveDetails
	errData.Details = details
	errData.ServiceID = serviceId
	errData.RequestID = requestId
	errData.LoginUrl = loginUrl

	br := bytes.NewBuffer(nil)
	if errorTpl == nil {
		return []byte("failed to get template data")
	}

	if errorTpl != nil {
		if err := errorTpl.ExecuteTemplate(br, "error", errData); err != nil {
			config.GetLogger().Error(err)
		}
	}

	defer br.Reset()

	return br.Bytes()
}
