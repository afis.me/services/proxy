package auth

import (
	"code.afis.me/services/proxy/app/utils"
	"fmt"
	"net/http"
	"sync"

	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
)

type Basic struct {
	r      *http.Request
	w      http.ResponseWriter
	config *proxy.AuthConfig
	sync.RWMutex
}

func NewBasic(config *proxy.AuthConfig, w http.ResponseWriter, r *http.Request) (interfaces.Auth, error) {

	return &Basic{
		w:      w,
		r:      r,
		config: config,
	}, nil
}

func (c *Basic) Validate(username, password string) (valid bool) {
	for _, s := range config.GetConfig().Auth {
		if s.Name == c.config.Config {
			if s.Users != nil {
				if len(s.Users[username]) != 0 {
					if s.Users[username] == password {
						return true
					}
				}
			}

		}
	}
	return valid
}

func (c *Basic) Valid() (valid bool, isRedirect bool) {
	username, password, ok := c.r.BasicAuth()
	if ok {
		valid = c.Validate(username, password)
		if !valid {
			if utils.IsWebsocket(c.r) {
				c.w.Header().Set("proxy-auth-error", "true")
				c.w.Header().Set("proxy-auth-code", fmt.Sprintf("%d", 401))
				c.w.Header().Set("proxy-auth-msg", "Unauthorized")
				return true, false
			}

		}
		return valid, false
	}
	return valid, false
}

func (c *Basic) IsExpired() bool {
	return false
}
