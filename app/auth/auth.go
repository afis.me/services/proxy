package auth

import (
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/auth/oauth2"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"fmt"
	"net/http"
)

type Auth struct {
}

func New() *Auth {
	m := &Auth{}

	if err := oauth2.NewClient().Init(); err != nil {
		config.GetLogger().Error(err).Quit()
	}

	return m
}

func (c *Auth) Load(config *proxy.AuthConfig, w http.ResponseWriter, r *http.Request) (interfaces.Auth, error) {
	switch config.Mode {
	case "basic":
		return NewBasic(config, w, r)
	case "oauth2":
		return oauth2.NewOauth2(config, w, r)
	}
	return nil, fmt.Errorf("no auth mode '%s' available", config.Mode)
}
