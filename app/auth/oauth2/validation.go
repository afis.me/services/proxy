package oauth2

import (
	"bytes"
	"code.afis.me/modules/go/utils/crypto/encrypt"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

func (c *Oauth2) validateToken(revalidate bool) (valid bool, redirect bool) {
	c.RLock()
	cookiesToken := c.cookies["token"]
	c.RUnlock()

	var token string
	if cookiesToken != nil {
		token = *cookiesToken
		decodeString, err := base64.StdEncoding.DecodeString(token)
		if err != nil {
			return c.needLogin(fmt.Errorf("failed to decode token, %s", err.Error()), false, "")
		}

		accessToken, err := encrypt.SealBoxDecrypted(c.encryptKey, decodeString)
		if err != nil {
			return c.needLogin(fmt.Errorf("failed to decrypt token, %s", err.Error()), false, "")
		}
		token = string(accessToken)

		if c.config.ExternalValidation != nil {
			if c.config.ExternalValidation.Enable {
				return c.externalValidation(token, revalidate)
			}
		}

		return c.internalValidation(token)
	}

	return c.needLogin(nil, false, "")
}

func (c *Oauth2) internalValidation(token string) (valid bool, redirect bool) {
	_, isExpired, err := c.checkToken(token)
	if err != nil {

		if isExpired {
			c.RLock()
			cookiesRefresh := c.cookies["refresh"]
			c.RUnlock()
			if cookiesRefresh != nil {
				decodeString, err := base64.StdEncoding.DecodeString(*cookiesRefresh)
				if err != nil {
					return c.needLogin(fmt.Errorf("refresh token: %s", err.Error()), true, "")
				}
				refreshToken, err := encrypt.SealBoxDecrypted(c.encryptKey, decodeString)
				if err != nil {
					return c.needLogin(fmt.Errorf("refresh token: %s", err.Error()), true, "")
				}
				err = c.RequestToken(string(refreshToken), true)
				if err != nil {
					return c.needLogin(fmt.Errorf("refresh token: %s", err.Error()), false, "")
				}
				return true, false
			}
		}

		return c.needLogin(nil, false, "")
	}

	return true, false
}

func (c *Oauth2) externalValidation(token string, revalidate bool) (valid bool, redirect bool) {
	tag := c.config.ExternalValidation.Tag
	if len(tag) == 0 {
		tag = c.r.Host
	}

	dataReq := &proxy.ValidateTokenRequest{
		AccessToken: token,
		ClientId:    c.config.ClientID,
		UUID:        c.uuid,
		Path:        c.r.URL.Path,
		Tag:         tag,
	}
	var res *proxy.ValidateTokenResponse
	var fatal bool
	var err error

	if !c.config.ExternalValidation.Validation.IsGrpc {
		res, fatal, err = c.HttpValidation(dataReq)
		if err != nil {
			return c.needLogin(err, fatal, "")
		}
	}

	if c.config.ExternalValidation.Validation.IsGrpc {
		res, fatal, err = c.GrpcValidation(dataReq)
		if err != nil {
			return c.needLogin(err, fatal, "")
		}
	}

	if res.Status != 200 {
		if int(res.Status) == http.StatusForbidden && !res.Expired {
			return c.forbidden(res.ErrorDescription)
		}

		if revalidate {
			return c.needLogin(fmt.Errorf("%s", res.ErrorDescription), false, "")
		}

		if res.Expired {

			c.RLock()
			cookiesRefresh := c.cookies["refresh"]
			c.RUnlock()

			if cookiesRefresh != nil {
				decodeString, err := base64.StdEncoding.DecodeString(*cookiesRefresh)
				if err != nil {
					return c.needLogin(fmt.Errorf("refresh token: %s", err.Error()), true, "")
				}

				refreshToken, err := encrypt.SealBoxDecrypted(c.encryptKey, decodeString)
				if err != nil {
					return c.needLogin(fmt.Errorf("refresh token: %s", err.Error()), true, "")
				}

				if err := c.RequestToken(string(refreshToken), true); err != nil {
					return c.needLogin(fmt.Errorf("refresh token: %s", err.Error()), false, "")
				}

				if c.config.ExternalValidation.RevalidationOnRefreshToken {
					return c.validateToken(true)
				}

				return true, false
			}

		}

		return c.needLogin(fmt.Errorf("failed to get data upstream: %s", res.ErrorDescription), false, "")
	}

	return true, false
}

func (c *Oauth2) HttpValidation(reqData *proxy.ValidateTokenRequest) (res *proxy.ValidateTokenResponse, fatal bool, err error) {
	res = &proxy.ValidateTokenResponse{}
	client := oauth2Client.Get(c.config.ExternalValidation.Validation)
	if client == nil {
		return res, true, fmt.Errorf("failed to get data upstream, http client is empty")
	}

	u, err := url.Parse(c.config.ExternalValidation.Validation.URL)
	if err != nil {
		return res, true, err
	}

	dataReq, _ := json.Marshal(reqData)
	req, err := http.NewRequest("POST", u.String(), bytes.NewReader(dataReq))
	if err != nil {
		return res, true, fmt.Errorf("failed to get data upstream: %s", err.Error())
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Content-Length", strconv.Itoa(len(dataReq)))
	req.Header.Set("Uuid", c.uuid)
	//req.Header.Set("Appid", c.config.ClientID)

	response, err := client.Http.Do(req.WithContext(c.context))
	if err != nil {
		return res, true, fmt.Errorf("failed to get data upstream: %s", err.Error())
	}

	if response == nil {
		return res, true, fmt.Errorf("failed to get data upstream, response is nil")
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)

	all, err := io.ReadAll(response.Body)
	if err != nil {
		return res, true, fmt.Errorf("failed to get data upstream: %s", err.Error())
	}

	if response.StatusCode != 200 && len(response.Header.Get("Grpc-Message")) > 0 {
		return res, true, fmt.Errorf("failed to get data upstream: %s", response.Header.Get("Grpc-Message"))
	}

	responseData := &proxy.ValidateTokenResponse{}
	if err := json.Unmarshal(all, responseData); err != nil {
		return res, true, fmt.Errorf("failed to get data upstream: %s", err.Error())
	}

	return responseData, false, nil
}

func (c *Oauth2) GrpcValidation(reqData *proxy.ValidateTokenRequest) (res *proxy.ValidateTokenResponse, fatal bool, err error) {
	res = &proxy.ValidateTokenResponse{}
	client := oauth2Client.Get(c.config.ExternalValidation.Validation)
	if client == nil {
		return res, true, fmt.Errorf("failed to get data upstream, grpc client is empty")
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5)*time.Second)
	defer cancel()
	//metadata.New(map[string]string{"uuid": c.uuid, "appid": c.config.ClientID})
	response, err := client.Auth.ValidateToken(ctx, reqData)
	if err != nil {
		return res, false, err
	}

	return response, false, nil
}

func (c *Oauth2) IsExpired() bool {
	c.RLock()
	cookiesToken := c.cookies["token"]
	c.RUnlock()

	var token string
	if cookiesToken != nil {
		token = *cookiesToken
		decodeString, err := base64.StdEncoding.DecodeString(token)
		if err != nil {
			return true
		}
		accessToken, err := encrypt.SealBoxDecrypted(c.encryptKey, decodeString)
		if err != nil {
			return true
		}
		token = string(accessToken)
	}

	if len(token) == 0 {
		return true
	}

	_, isExpired, _ := c.checkToken(token)
	return isExpired
}

func (c *Oauth2) checkToken(token string) (valid bool, isExpired bool, err error) {
	if len(token) == 0 {
		return false, false, nil
	}

	var claim UserClaims
	_, err = jwt.ParseWithClaims(token, &claim, func(token *jwt.Token) (interface{}, error) {
		switch c.config.SigningMethod {
		case "HS256":
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, fmt.Errorf("token not valid : signing method invalid")
			}
		case "RS256":
			_, ok := token.Method.(*jwt.SigningMethodRSA)
			if !ok {
				return nil, fmt.Errorf("token not valid : signing method invalid")
			}
		}

		if time.Now().After(claim.ExpiresAt.Time) {
			isExpired = true
		}

		_, ok := token.Method.(*jwt.SigningMethodRSA)
		if ok {
			key, err := jwt.ParseRSAPublicKeyFromPEM([]byte(c.config.ClientSecret))
			if err != nil {
				return nil, err
			}
			return key, nil
		}

		return []byte(c.config.ClientSecret), nil
	})

	if err != nil {
		return false, isExpired, err
	}

	return true, isExpired, nil
}
