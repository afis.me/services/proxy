package oauth2

import (
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"context"
	"github.com/golang-jwt/jwt/v4"
	"net/http"
	"sync"
)

type Oauth2 struct {
	w          http.ResponseWriter
	r          *http.Request
	config     *proxy.Oauth2Config
	uuid       string
	scheme     string
	encryptKey string
	cookies    map[string]*string
	context    context.Context
	sync.RWMutex
}

type UserClaims struct {
	jwt.RegisteredClaims
}

type TokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int64  `json:"expires_in"`
	ClientId     string `json:"client_id"`
	UUID         string `json:"uuid"`
}

type TokenErrorResponse struct {
	Error            string `json:"error,omitempty"`
	ErrorDescription string `json:"error_description,omitempty"`
	ErrorUri         string `json:"error_uri,omitempty"`
	Expired          bool   `json:"expired"`
}
