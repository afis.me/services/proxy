package oauth2

import (
	"code.afis.me/modules/go/utils/crypto/encrypt"
	"code.afis.me/modules/go/utils/crypto/hash"
	grpcUtil "code.afis.me/modules/go/utils/grpc"
	"code.afis.me/modules/go/utils/validator"
	proxy "code.afis.me/proto/services/proxy/interfaces"
	"code.afis.me/services/proxy/app/assets"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"code.afis.me/services/proxy/app/utils"
	"context"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"golang.org/x/net/http2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"
)

type ClientItem struct {
	Http *http.Client
	Auth proxy.AuthClient
}

type Client struct {
	enableTracing  bool
	client         map[string]*ClientItem
	onProcess      map[string]string
	vendor         map[string]*proxy.Oauth2Config
	WaitingChannel sync.Map
	sync.RWMutex
}

var oauth2Client *Client

func NewClient() *Client {
	oauth2Client = &Client{
		onProcess: map[string]string{},
		client:    map[string]*ClientItem{},
		vendor:    map[string]*proxy.Oauth2Config{},
	}
	return oauth2Client
}

func GetClient() *Client {
	return oauth2Client
}

func (c *Client) Init() error {
	for _, s := range config.GetConfig().Auth {
		if s.Oauth2Config != nil {
			oauth2Config := s.Oauth2Config
			if oauth2Config.TokenEndpoint == nil {
				return fmt.Errorf("tokenEndpoint for %s is nil", s.Name)
			}

			if len(oauth2Config.TokenEndpoint.URL) == 0 {
				return fmt.Errorf("tokenEndpoint url for %s not valid", s.Name)
			}

			switch oauth2Config.TokenEndpoint.Proto {
			case "h1", "h2":
			default:
				return fmt.Errorf("tokenEndpoint proto for %s not valid", s.Name)
			}

			if oauth2Config.ExternalValidation != nil {
				if oauth2Config.ExternalValidation.Enable {
					if oauth2Config.ExternalValidation.Validation == nil {
						return fmt.Errorf("externalValidation validatation for %s is nil", s.Name)
					}

					if len(oauth2Config.ExternalValidation.Validation.URL) == 0 {
						return fmt.Errorf("externalValidation validatation url for %s not valid", s.Name)
					}

					if !oauth2Config.ExternalValidation.Validation.IsGrpc {
						switch oauth2Config.ExternalValidation.Validation.Proto {
						case "h1", "h2":
						default:
							return fmt.Errorf("externalValidation validatation proto for %s not valid", s.Name)
						}
					}

					if oauth2Config.ExternalValidation.Validation.IsGrpc {
						err := validator.GetValidator().Var(oauth2Config.ExternalValidation.Validation.URL, "required,hostname_port")
						if err != nil {
							return fmt.Errorf("externalValidation validatation url for %s not valid, ex: 127.0.0.1:4001", s.Name)
						}
					}

					if err := oauth2Client.AddOrSkip(oauth2Config.ExternalValidation.Validation); err != nil {
						return err
					}

				}
			}

			if len(oauth2Config.AuthorizationEndpoint) == 0 {
				return fmt.Errorf("authorizationEndpoint for %s not valid", s.Name)
			}

			if len(oauth2Config.ClientID) == 0 {
				return fmt.Errorf("clientId for %s not valid", s.Name)
			}

			if len(oauth2Config.ClientSecret) == 0 {
				return fmt.Errorf("clientSecret for %s not valid", s.Name)
			}

			if len(oauth2Config.Callback) == 0 {
				return fmt.Errorf("callback for %s not valid", s.Name)
			}

			switch oauth2Config.SigningMethod {
			case "HS256", "RS256":
			default:
				return fmt.Errorf("signing method only valid `HS256 || RS256`")
			}

			if err := oauth2Client.AddOrSkip(oauth2Config.TokenEndpoint); err != nil {
				return err
			}

			c.Lock()
			c.vendor[s.Name] = oauth2Config
			c.Unlock()

		}
	}
	return nil
}

func (c *Client) GetVendor(id string) (oauth2Config *proxy.Oauth2Config) {
	c.RLock()
	defer c.RUnlock()
	return c.vendor[id]
}

func (c *Client) IsClientProcessing(uuid string) (val string, ok bool) {
	c.RLock()
	defer c.RUnlock()
	val, ok = c.onProcess[uuid]
	return val, ok
}

func (c *Client) SetClientProcessing(uuid, path string) {
	c.Lock()
	defer c.Unlock()
	c.onProcess[uuid] = path
}

func (c *Client) DeleteClientProcessing(uuid string) {
	c.Lock()
	defer c.Unlock()
	delete(c.onProcess, uuid)
}

func (c *Client) Get(cfg *proxy.Oauth2Url) *ClientItem {
	c.RLock()
	defer c.RUnlock()
	id := hash.CreateHash(cfg.URL, cfg.Proto)
	return c.client[id]
}

func (c *Client) AddOrSkip(cfg *proxy.Oauth2Url) error {
	c.Lock()
	defer c.Unlock()
	id := hash.CreateHash(cfg.URL, cfg.Proto)

	if c.client[id] == nil && cfg.IsGrpc {
		var opts []grpc.DialOption
		if !cfg.Secure {
			opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
		}
		if cfg.Secure {
			opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: cfg.InsecureSkipVerify})))
		}
		conn, err := grpc.Dial(cfg.URL, opts...)
		if err != nil {
			return fmt.Errorf("can not connect with server %s", err.Error())
		}
		if conn == nil {
			return fmt.Errorf("can not connect with server, conn is nil")
		}
		c.client[id] = &ClientItem{Auth: proxy.NewAuthClient(conn)}
		return nil
	}

	if c.client[id] == nil && !cfg.IsGrpc {
		client := &http.Client{Timeout: time.Second * 10}
		switch cfg.Proto {
		case "h1":
			if cfg.Secure {
				client.Transport = &http.Transport{
					DisableCompression: true,
					ForceAttemptHTTP2:  true,
					DisableKeepAlives:  false,
					TLSClientConfig:    &tls.Config{InsecureSkipVerify: cfg.InsecureSkipVerify},
				}

			} else {
				client.Transport = &http.Transport{
					DisableCompression: true,
					ForceAttemptHTTP2:  true,
					DisableKeepAlives:  false,
					DialTLSContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
						return net.Dial(network, addr)
					},
				}
			}
		case "h2":
			if cfg.Secure {
				client.Transport = &http2.Transport{
					AllowHTTP:                  true,
					StrictMaxConcurrentStreams: true,
					DisableCompression:         true,
					TLSClientConfig:            &tls.Config{InsecureSkipVerify: cfg.InsecureSkipVerify},
					CountError: func(errType string) {
					},
				}
			} else {
				client.Transport = &http2.Transport{
					AllowHTTP: true,
					DialTLSContext: func(ctx context.Context, network, addr string, cfg *tls.Config) (net.Conn, error) {
						return net.Dial(network, addr)
					},
					StrictMaxConcurrentStreams: true,
					DisableCompression:         true,
					CountError: func(errType string) {
					},
				}
			}
		}

		c.client[id] = &ClientItem{Http: client}
		return nil
	}

	return nil
}

func NewOauth2(cfg *proxy.AuthConfig, w http.ResponseWriter, r *http.Request) (interfaces.Auth, error) {

	scheme := "https"
	if r.TLS == nil {
		scheme = "http"
	}

	vendor := oauth2Client.GetVendor(cfg.Config)
	if vendor == nil {
		return nil, fmt.Errorf("provider %s not found", cfg.Config)
	}

	c := &Oauth2{
		w:          w,
		r:          r,
		config:     vendor,
		scheme:     scheme,
		encryptKey: hash.CreateSmallHash(32, vendor.ClientSecret),
		cookies:    map[string]*string{},
		context:    context.Background(),
	}

	if uuidVal, err := r.Cookie("uuid"); err == nil {
		c.uuid = uuidVal.Value
	}

	if len(c.uuid) == 0 {
		c.uuid = uuid.New().String()
	}

	if cookieVal, err := r.Cookie("token"); err == nil {
		val := cookieVal.Value
		if len(val) != 0 {
			c.Lock()
			c.cookies["token"] = &val
			c.Unlock()
		}

	}

	if cookieVal, err := r.Cookie("refresh"); err == nil {
		val := cookieVal.Value
		if len(val) != 0 {
			c.Lock()
			c.cookies["refresh"] = &cookieVal.Value
			c.Unlock()
		}
	}

	uuidCookie := &http.Cookie{
		Name:     "uuid",
		HttpOnly: true,
		Value:    c.uuid,
		Path:     "/",
	}

	if r.TLS != nil {
		uuidCookie.Secure = true
	}

	http.SetCookie(w, uuidCookie)

	return c, nil
}

func (c *Oauth2) Valid() (valid bool, isRedirect bool) {
	if strings.ToLower(c.r.URL.Path) == strings.ToLower(c.config.Logout) {
		c.ResetCookies(true)

		uCallback, err := url.Parse(fmt.Sprintf("%s://%s/%s", c.scheme, c.r.Host, strings.TrimPrefix(c.config.Login, "/")))
		if err != nil {
			return false, false
		}

		origin := c.r.URL.Query().Get("redirect")
		if len(origin) == 0 {
			redirectUrl, err := url.Parse(fmt.Sprintf("%s://%s/", c.scheme, c.r.Host))
			if err != nil {
				return false, false
			}
			origin = redirectUrl.String()
		}

		q := uCallback.Query()
		q.Set("redirect", origin)
		uCallback.RawQuery = q.Encode()

		http.Redirect(c.w, c.r, uCallback.String(), http.StatusTemporaryRedirect)
		return false, true

	}

	if strings.ToLower(c.r.URL.Path) == strings.ToLower(c.config.Login) {
		origin := c.r.URL.Query().Get("redirect")
		if len(origin) == 0 {
			redirectUrl, err := url.Parse(fmt.Sprintf("%s://%s/", c.scheme, c.r.Host))
			if err != nil {
				return false, false
			}
			origin = redirectUrl.String()
		}
		return c.needLogin(nil, false, origin)
	}

	if strings.ToLower(c.r.URL.Path) == strings.ToLower(c.config.LoginProcess) {

		if c.r.Method != "POST" {
			uCallback, err := url.Parse(fmt.Sprintf("%s://%s", c.scheme, c.r.Host))
			if err != nil {
				utils.ErrorResponse(c.w, c.r, http.StatusUnauthorized, err.Error())
				return false, true
			}
			http.Redirect(c.w, c.r, uCallback.String(), http.StatusTemporaryRedirect)
		}

		if _, err := c.r.Cookie("origin"); err != nil {
			uCallback, err := url.Parse(fmt.Sprintf("%s://%s", c.scheme, c.r.Host))
			if err != nil {
				utils.ErrorResponse(c.w, c.r, http.StatusUnauthorized, err.Error())
				return false, true
			}
			http.Redirect(c.w, c.r, uCallback.String(), http.StatusTemporaryRedirect)
		}

		u, err := url.Parse(c.config.AuthorizationEndpoint)
		if err != nil {
			return false, false
		}

		uCallback, err := url.Parse(fmt.Sprintf("%s://%s%s", c.scheme, c.r.Host, c.config.Callback))
		if err != nil {
			utils.ErrorResponse(c.w, c.r, http.StatusUnauthorized, err.Error())
			return false, true
		}

		state := hash.CreateRandomId(16)
		q := u.Query()
		q.Set("client_id", c.config.ClientID)
		q.Set("response_type", "code")
		q.Set("state", state)
		q.Set("redirect_uri", uCallback.String())
		u.RawQuery = q.Encode()

		http.Redirect(c.w, c.r, u.String(), http.StatusTemporaryRedirect)
		return false, true
	}

	if c.r.URL.Path == c.config.Callback {
		return c.callbackHandler()
	}

	return c.validateToken(false)
}

type ErrorJson struct {
	Message string `json:"message"`
}

func (c *Oauth2) callbackHandler() (valid bool, redirect bool) {

	state := c.r.URL.Query().Get("state")
	code := c.r.URL.Query().Get("code")
	if len(state) == 0 || len(code) == 0 {
		return false, false
	}

	if err := c.RequestToken(code, false); err != nil {
		utils.ErrorResponse(c.w, c.r, http.StatusUnauthorized, err.Error())
		return false, true
	}

	// redirect is valid
	if val, err := c.r.Cookie("origin"); err == nil {
		// deleting origin cookies

		cookie := http.Cookie{
			Name:    "origin",
			Value:   "",
			Path:    "/",
			Expires: time.Now().Add(-7 * 24 * time.Hour),
		}
		http.SetCookie(c.w, &cookie)

		decodeString, err := base64.StdEncoding.DecodeString(val.Value)
		if err != nil {
			return false, false
		}

		origin, err := encrypt.SealBoxDecrypted(c.encryptKey, decodeString)
		if err != nil {
			return false, false
		}

		http.Redirect(c.w, c.r, string(origin), http.StatusTemporaryRedirect)
		return true, true

	}
	return false, false
}

func (c *Oauth2) forbidden(msg string) (valid bool, redirect bool) {
	meta := grpcUtil.ExtractIncoming(grpcUtil.NewContextFromRequest(c.r))
	isGrpc := grpcUtil.IsGrpcRequest(c.r)
	grpcStatus := fmt.Sprintf("%d", codes.PermissionDenied)
	msgDetails := ""
	errorCode := http.StatusForbidden

	if isGrpc {
		c.w.Header().Set("content-type", meta.Get("content-type"))
		c.w.Header().Set("grpc-status", grpcStatus)
		c.w.Header().Set("grpc-message", msg)
		c.w.WriteHeader(http.StatusOK)
		return false, true
	}

	var isJsonResponse bool
	if strings.Contains(meta.Get("accept"), "application/json") {
		isJsonResponse = true
	}

	if c.r.Header.Get("X-Requested-With") == strings.ToLower("xmlHttpRequest") {
		isJsonResponse = true
	}

	// detect if path is api, generally
	sp := strings.Split(c.r.URL.Path, "/")
	if len(sp) >= 2 {
		if sp[1] == "api" {
			isJsonResponse = true
		}
	}

	if isJsonResponse {
		msw, _ := json.Marshal(ErrorJson{Message: msg})
		c.w.Header().Set("content-type", meta.Get("content-type"))
		c.w.WriteHeader(401)
		_, _ = c.w.Write(msw)
		return false, true
	}

	if dt := assets.GenerateLogin(os.Getenv("ServiceID"),
		c.r.Header.Get("RequestID"), errorCode, msg, msgDetails, false, c.config.LoginProcess); len(dt) != 0 {
		c.w.WriteHeader(errorCode)
		_, _ = c.w.Write(dt)
		return false, true
	}

	return false, false
}

func (c *Oauth2) needLogin(err error, fatalError bool, originUrl string) (valid bool, redirect bool) {

	meta := grpcUtil.ExtractIncoming(grpcUtil.NewContextFromRequest(c.r))
	isGrpc := grpcUtil.IsGrpcRequest(c.r)
	msg := "Please log in to continue using this service"
	grpcStatus := fmt.Sprintf("%d", codes.Unauthenticated)
	msgDetails := ""
	errorCode := 401

	if err != nil {
		msgDetails = err.Error()
	}

	if fatalError {
		msg = "Request can't be continue, try again later"
		errorCode = 500
		grpcStatus = fmt.Sprintf("%d", codes.Internal)
	}

	if isGrpc {
		if msgDetails != "" {
			msg = msgDetails
		}
		c.w.Header().Set("content-type", meta.Get("content-type"))
		c.w.Header().Set("grpc-status", grpcStatus)
		c.w.Header().Set("grpc-message", msg)
		c.w.WriteHeader(http.StatusOK)
		return false, true
	}

	var isJsonResponse bool
	if strings.Contains(meta.Get("accept"), "application/json") {
		isJsonResponse = true
	}

	if c.r.Header.Get("X-Requested-With") == "xmlhttprequest" {
		isJsonResponse = true
	}

	// detect if path is api, generally
	sp := strings.Split(c.r.URL.Path, "/")
	if len(sp) >= 2 {
		if sp[1] == "api" {
			isJsonResponse = true
		}
	}

	uOrigin, err := url.Parse(fmt.Sprintf("%s://%s%s", c.scheme, c.r.Host, c.r.URL.Path))
	if err != nil {
		utils.ErrorResponse(c.w, c.r, http.StatusUnauthorized, err.Error())
		return false, true
	}
	uOrigin.RawQuery = c.r.URL.RawQuery

	originData := originUrl
	if len(originData) == 0 {
		originData = uOrigin.String()
	}

	origin, err := encrypt.SealBoxEncrypted(c.encryptKey, []byte(originData))
	if err != nil {
		utils.ErrorResponse(c.w, c.r, http.StatusUnauthorized, err.Error())
		return false, true
	}

	originCookie := &http.Cookie{Name: "origin", HttpOnly: true, Path: "/", Value: base64.StdEncoding.EncodeToString(origin)}
	if c.r.TLS != nil {
		originCookie.Secure = true
	}
	http.SetCookie(c.w, originCookie)

	if !fatalError {
		if !utils.IsWebsocket(c.r) {
			c.ResetCookies(false)
		}

	}

	if isJsonResponse {
		msw, _ := json.Marshal(ErrorJson{Message: msg})
		c.w.Header().Set("content-type", meta.Get("content-type"))
		c.w.WriteHeader(errorCode)
		_, _ = c.w.Write(msw)
		return false, true
	}

	if utils.IsWebsocket(c.r) {
		c.w.Header().Set("proxy-auth-error", "true")
		c.w.Header().Set("proxy-auth-code", fmt.Sprintf("%d", errorCode))
		c.w.Header().Set("proxy-auth-msg", msgDetails)
		if len(msgDetails) == 0 {
			c.w.Header().Set("proxy-auth-msg", msg)
		}
		return true, false
	}

	if dt := assets.GenerateLogin(os.Getenv("ServiceID"),
		c.r.Header.Get("RequestID"), errorCode, msg, msgDetails, !fatalError, c.config.LoginProcess); len(dt) != 0 {
		_, _ = c.w.Write(dt)
		return false, true
	}

	return false, false
}
