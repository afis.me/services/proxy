package oauth2

import (
	tracing "code.afis.me/modules/go/tracing/interfaces"
	"code.afis.me/modules/go/utils/crypto/encrypt"
	"code.afis.me/modules/go/utils/crypto/hash"
	"code.afis.me/services/proxy/app/config"
	"code.afis.me/services/proxy/app/interfaces"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

type ChannelToken struct {
	Access    string
	Refresh   string
	StartTime time.Time
}

func (c *Oauth2) RequestToken(code string, refresh bool) (errs error) {

	var ignore = make(map[string]bool)
	ignore[c.config.Logout] = true
	ignore[c.config.Login] = true
	ignore[c.config.LoginProcess] = true
	ignore[c.config.Callback] = true

	if ignore[c.r.URL.Path] == false {
		smallUuid := hash.CreateSmallHash(8, c.uuid)

		if _, ok := oauth2Client.IsClientProcessing(smallUuid); ok {
			config.GetLogger().Trace("[%s][%s] Waiting for another process", smallUuid, c.r.URL.Path)

			messages := make(chan *ChannelToken)
			actual, _ := oauth2Client.WaitingChannel.LoadOrStore(smallUuid, &sync.Map{})
			if vs, ok := actual.(*sync.Map); ok {
				vs.Store(hash.CreateRandomId(8), messages)
			}
			msg := <-messages
			if msg == nil {
				return fmt.Errorf("failed to get token")
			}
			c.Lock()
			c.cookies["token"] = &msg.Access
			if len(msg.Refresh) != 0 {
				c.cookies["refresh"] = &msg.Refresh
			}
			c.Unlock()
			c.SetAuthCookies(true, msg.Access, msg.Refresh)
			config.GetLogger().Trace("[%s][%s] Waiting is done in %s", smallUuid, c.r.URL.Path, time.Since(msg.StartTime))
			return nil
		}

		oauth2Client.SetClientProcessing(smallUuid, c.r.URL.Path)
		tss := time.Now()
		config.GetLogger().Trace("[%s][%s] Process request access or refresh token", smallUuid, c.r.URL.Path)
		accessToken, refreshToken, err := c.requestToken(code, refresh, true)
		actual, _ := oauth2Client.WaitingChannel.LoadOrStore(smallUuid, &sync.Map{})
		if vs, ok := actual.(*sync.Map); ok {
			vs.Range(func(key, value any) bool {
				value.(chan *ChannelToken) <- &ChannelToken{
					Access:    accessToken,
					Refresh:   refreshToken,
					StartTime: tss,
				}
				vs.Delete(key)
				return true
			})
		}
		oauth2Client.DeleteClientProcessing(smallUuid)
		return err

	}

	_, _, err := c.requestToken(code, refresh, true)
	return err
}

func (c *Oauth2) requestToken(code string, refresh, writeCookies bool) (accessToken, refreshToken string, err error) {
	if valCtx := c.r.Context().Value(interfaces.RequestContextProxy); valCtx != nil {
		if value, ok := valCtx.(*interfaces.RequestContext); ok {
			if value.GetEnableTracing() {
				name := "Oauth2 Access Token"
				if refresh {
					name = "Oauth2 Refresh Token"
				}

				tr := config.GetTracing()
				if tr != nil {
					idt := tr.Operation(c.context,
						tracing.NewInteractionName(name),
						tracing.NewInteractionTypeType(interfaces.Name))
					c.context = idt.OutgoingContext()
					defer idt.Finish()
				}
			}
		}
	}

	var errs error

	client := oauth2Client.Get(c.config.TokenEndpoint)
	if client == nil {
		return "", "", fmt.Errorf("failed to get data upstream, http client is empty")
	}

	uCallback, err := url.Parse(fmt.Sprintf("%s://%s%s", c.scheme, c.r.Host, c.config.Callback))
	if err != nil {
		errs = err
		return "", "", err
	}

	data := url.Values{}
	data.Set("client_id", c.config.ClientID)
	data.Set("client_secret", c.config.ClientSecret)
	data.Set("uuid", c.uuid)

	if !refresh {
		data.Set("grant_type", "authorization_code")
		data.Set("redirect_uri", uCallback.String())
		data.Set("code", code)
	}

	if refresh {
		data.Set("grant_type", "refresh_token")
		data.Set("refresh_token", code)
	}

	encodedData := data.Encode()

	u, err := url.Parse(c.config.TokenEndpoint.URL)
	if err != nil {
		errs = err
		return "", "", err
	}

	req, err := http.NewRequest("POST", u.String(), strings.NewReader(encodedData))
	if err != nil {
		errs = fmt.Errorf("failed to get data upstream: %s", err.Error())
		return "", "", errs
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Content-Length", strconv.Itoa(len(data.Encode())))
	req.Header.Set("Uuid", c.uuid)

	if c.context == nil {
		c.context = context.Background()
	}

	response, err := client.Http.Do(req.WithContext(c.context))
	if err != nil {
		errs = fmt.Errorf("failed to get data upstream: %s", err.Error())
		return "", "", errs
	}

	if response == nil {
		errs = fmt.Errorf("failed to get data upstream, response is nil")
		return "", "", errs
	}

	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)

	if response.StatusCode != 200 {

		if strings.Contains(response.Header.Get("Content-Type"), "application/json") {

			all, err := io.ReadAll(response.Body)
			if err != nil {
				errs = fmt.Errorf("failed to get data upstream: %s", err.Error())
				return "", "", errs
			}

			var errorResponse TokenErrorResponse
			if err := json.Unmarshal(all, &errorResponse); err != nil {
				errs = fmt.Errorf("failed to get data upstream: %s", err.Error())
				return "", "", errs
			}

			errs = fmt.Errorf("failed to get data upstream: %s", errorResponse.ErrorDescription)
			return "", "", errs
		}

		errs = fmt.Errorf("failed to get data upstream, response code is %d", response.StatusCode)
		return "", "", errs
	}

	all, err := io.ReadAll(response.Body)
	if err != nil {
		errs = fmt.Errorf("failed to get data upstream: %s", err.Error())
		return "", "", errs
	}

	var token TokenResponse
	if err := json.Unmarshal(all, &token); err != nil {
		errs = fmt.Errorf("failed to get data upstream: %s", err.Error())
		return "", "", errs
	}

	encryptedToken, err := encrypt.SealBoxEncrypted(c.encryptKey, []byte(token.AccessToken))
	if err != nil {
		errs = err
		return "", "", err
	}

	accessTokenVal := base64.StdEncoding.EncodeToString(encryptedToken)
	c.Lock()
	c.cookies["token"] = &accessTokenVal
	c.Unlock()

	var refreshTokenVal string
	if len(token.RefreshToken) != 0 {
		encryptedRefreshToken, err := encrypt.SealBoxEncrypted(c.encryptKey, []byte(token.RefreshToken))
		if err != nil {
			errs = err
			return "", "", err
		}
		refreshTokenVal = base64.StdEncoding.EncodeToString(encryptedRefreshToken)
		c.Lock()
		c.cookies["refresh"] = &refreshTokenVal
		c.Unlock()
	}

	c.SetAuthCookies(writeCookies, accessTokenVal, refreshTokenVal)

	return accessTokenVal, refreshTokenVal, nil
}

func (c *Oauth2) ResetCookies(clearOrigin bool) {

	tokenCookie := &http.Cookie{Name: "token", Value: "", Path: "/", Expires: time.Now().Add(-7 * 24 * time.Hour)}
	refreshCookie := &http.Cookie{Name: "refresh", Value: "", Path: "/", Expires: time.Now().Add(-7 * 24 * time.Hour)}

	http.SetCookie(c.w, tokenCookie)
	http.SetCookie(c.w, refreshCookie)

	if clearOrigin {
		originCookie := &http.Cookie{Name: "origin", Value: "", HttpOnly: true, Path: "/", Expires: time.Now().Add(-7 * 24 * time.Hour)}
		if c.r.TLS != nil {
			originCookie.Secure = true
		}
		http.SetCookie(c.w, originCookie)
	}

}

func (c *Oauth2) SetAuthCookies(writeCookies bool, accessToken, refreshToken string) {
	if writeCookies {
		accessTokenCookie := &http.Cookie{
			Name:     "token",
			HttpOnly: true,
			Path:     "/",
			Value:    accessToken,
		}

		if c.r.TLS != nil {
			accessTokenCookie.Secure = true
		}
		http.SetCookie(c.w, accessTokenCookie)
	}

	if writeCookies {
		refreshTokenCookie := &http.Cookie{
			Name:     "refresh",
			HttpOnly: true,
			Path:     "/",
			Value:    refreshToken,
		}
		if c.r.TLS != nil {
			refreshTokenCookie.Secure = true
		}
		http.SetCookie(c.w, refreshTokenCookie)
	}
}
