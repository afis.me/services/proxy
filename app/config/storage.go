package config

import (
	"nhooyr.io/websocket"
	"sync"

	proxy "code.afis.me/proto/services/proxy/interfaces"
)

type RoutingData struct {
	items map[string]*proxy.Routing
	sync.RWMutex
}

func NewRouting() *RoutingData {
	return &RoutingData{
		items: map[string]*proxy.Routing{},
	}
}

func (c *RoutingData) Store(key string, data *proxy.Routing) {
	c.Lock()
	c.items[key] = data
	c.Unlock()
}

func (c *RoutingData) Delete(key string) {
	c.Lock()
	delete(c.items, key)
	c.Unlock()
}

func (c *RoutingData) Load(key string) (data *proxy.Routing, ok bool) {
	c.RLock()
	data, ok = c.items[key]
	c.RUnlock()
	return data, ok
}

func (c *RoutingData) LoadAll() (data []*proxy.Routing) {
	c.RLock()
	list := c.items
	c.RUnlock()
	for _, s := range list {
		data = append(data, s)
	}
	return data
}

type BackendData struct {
	items map[string]*proxy.Backend
	sync.RWMutex
}

func NewBackend() *BackendData {
	return &BackendData{
		items: map[string]*proxy.Backend{},
	}
}

func (c *BackendData) Store(key string, data *proxy.Backend) {
	c.Lock()
	c.items[key] = data
	c.Unlock()
}

func (c *BackendData) Delete(key string) {
	c.Lock()
	delete(c.items, key)
	c.Unlock()
}

func (c *BackendData) Load(key string) (data *proxy.Backend, ok bool) {
	c.RLock()
	data, ok = c.items[key]
	c.RUnlock()
	return data, ok
}

func (c *BackendData) LoadAll() (data []*proxy.Backend) {
	c.RLock()
	list := c.items
	c.RUnlock()
	for _, s := range list {
		data = append(data, s)
	}
	return data
}

func NewSocketWorkerStorage() *SocketWorkerStorage {
	return &SocketWorkerStorage{
		workers: make(map[string]*SocketWorker),
	}
}

type SocketWorker struct {
	Client *websocket.Conn
	Server *websocket.Conn
}

type SocketWorkerStorage struct {
	sync.Mutex
	workers map[string]*SocketWorker
}

func (c *SocketWorkerStorage) Add(id string, w *SocketWorker) {
	c.Lock()
	defer c.Unlock()
	c.workers[id] = w
}

func (c *SocketWorkerStorage) Remove(id string) {
	c.Lock()
	defer c.Unlock()
	delete(c.workers, id)
}

func (c *SocketWorkerStorage) Iter(routine func(worker *SocketWorker)) {
	c.Lock()
	defer c.Unlock()
	for _, worker := range c.workers {
		routine(worker)
	}
}
