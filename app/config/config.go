package config

import (
	logInterface "code.afis.me/modules/go/logger/interfaces"
	tracingInterface "code.afis.me/modules/go/tracing/interfaces"
	proxy "code.afis.me/proto/services/proxy/interfaces"
)

var config *Config

func init() {
	config = &Config{Manual: map[string]string{}}
	config.Manual = tracingInterface.SetManual(config.Manual)
	config.Manual = logInterface.SetManual(config.Manual)
}

func GetConfig() *Config {
	return config
}

type FileBrowser struct {
	Name          string `yaml:"name"`
	Directory     string `yaml:"directory"`
	ReturnToIndex bool   `yaml:"returnToIndex"`
	MaxRequest    int64  `yaml:"maxRequest"`
}

type VanityUrls struct {
	Name       string `yaml:"name"`
	Config     string `yaml:"config"`
	MaxRequest int64  `yaml:"maxRequest"`
}

type DockerController struct {
	Enable     bool              `yaml:"enable"`
	Protected  bool              `yaml:"protected"`
	AuthConfig *proxy.AuthConfig `yaml:"authConfig"`
}

type ApiController struct {
	Enable     bool              `yaml:"enable"`
	Protected  bool              `yaml:"protected"`
	AuthConfig *proxy.AuthConfig `yaml:"authConfig"`
}

type ModulesListener struct {
	Listen string `yaml:"listen" default:"127.0.0.1"`
	Port   int    `yaml:"port" default:"4444"`
	Secure bool   `yaml:"secure"`
	Pem    string `yaml:"pem"`
}

type Config struct {
	// config manual for cli
	Manual     map[string]string `cli:"manual" json:"-" yaml:"-"`
	ShowBanner bool              `cli:"showBanner" yaml:"showBanner" default:"false"`
	// default config
	Logging                  logInterface.LoggerConfig         `yaml:"log"`
	Tracing                  tracingInterface.TracingConfigCli `yaml:"tracing"`
	DataDirectory            string                            `yaml:"dataDirectory" default:"./storage"`
	InternalStaticMaxRequest int64                             `yaml:"internalStaticMaxRequest" default:"100"`
	Discovery                *proxy.Discovery                  `yaml:"discovery"`
	Listener                 []*proxy.Listener                 `yaml:"listener"`
	Routing                  []*proxy.Routing                  `yaml:"routing"`
	Backend                  []*proxy.Backend                  `yaml:"backend"`
	Healthcheck              []*proxy.HealthCheck              `yaml:"healthcheck"`
	Transport                []*proxy.Transport                `yaml:"transport"`
	Auth                     []*proxy.RouterAuth               `yaml:"auth"`
	VanityUrls               []VanityUrls                      `yaml:"vanityUrls"`
	FileBrowser              []FileBrowser                     `yaml:"fileBrowser"`
	Database                 struct {
		Driver             string `yaml:"driver" default:"sqlite" desc:"config:sql:enable"`
		Enable             bool   `yaml:"enable" default:"true" desc:"config:sql:enable"`
		Host               string `yaml:"host" default:"127.0.0.1" desc:"config:sql:host"`
		Port               int    `yaml:"port" default:"3306" desc:"config:sql:port"`
		Username           string `yaml:"username" default:"root"  desc:"config:sql:username"`
		Password           string `yaml:"password" default:"root" desc:"config:sql:password"`
		Database           string `yaml:"database" default:"proxy" desc:"config:sql:database"`
		Options            string `yaml:"options" default:"" desc:"config:sql:options"`
		Connection         string `yaml:"connection" default:"" desc:"config:sql:connection"`
		AutoReconnect      bool   `yaml:"autoReconnect" default:"false"  desc:"config:autoReconnect"`
		StartInterval      int    `yaml:"startInterval" default:"2"  desc:"config:startInterval"`
		MaxError           int    `yaml:"maxError" default:"5"  desc:"config:maxError"`
		DBPath             string `yaml:"dbpath" default:"proxy.db" desc:"config:sql:dbpath"`
		SslMode            string `yaml:"sslMode" default:"disable" desc:"config:sql:sslMode"`
		TimeoutConnection  int    `yaml:"timeoutConnection" default:"5000" desc:"config:sql:timeoutConnection"`
		TimeoutTransaction int    `yaml:"timeoutTransaction" default:"5000" desc:"config:sql:timeoutTransaction"`
	} `yaml:"database"`
	BasicTokenAuth  string          `yaml:"basicTokenAuth"`
	ModulesListener ModulesListener `yaml:"modulesListener"`
	ApiController   ApiController   `yaml:"apiController"`
}
