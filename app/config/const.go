package config

type HeadersValue string

const (
	HeadersValueUseServerHost HeadersValue = "use-server-host"
)
