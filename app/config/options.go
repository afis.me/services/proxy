package config

import (
	"code.afis.me/services/proxy/app/interfaces"
	"strings"
	"sync"

	logInterface "code.afis.me/modules/go/logger/interfaces"
	logLib "code.afis.me/modules/go/logger/lib"
	tracingInterface "code.afis.me/modules/go/tracing/interfaces"
	tracingLib "code.afis.me/modules/go/tracing/lib"
	proxy "code.afis.me/proto/services/proxy/interfaces"
)

var logger logInterface.Logger
var tracing tracingInterface.Tracing
var registerModules map[string]bool
var queServerRemove = make(map[string]bool)
var onCloseRequest bool
var mu sync.RWMutex
var authCallbackUrl = make(map[string]*proxy.RouterAuth)
var shutDown bool
var socketWorkerStorageData *SocketWorkerStorage

var Routing *RoutingData
var Backend *BackendData

var ShutdownChanel = make(chan bool)

func init() {
	socketWorkerStorageData = NewSocketWorkerStorage()
	logger = logLib.NewLib()
	logger.Init(interfaces.Name, interfaces.Version)
	tracing = tracingLib.NewLib()

}

func GetSocketWorkerStorage() *SocketWorkerStorage {
	return socketWorkerStorageData
}

func SetShutDown(val bool) {
	mu.Lock()
	defer mu.Unlock()
	shutDown = val
}

func GetShutDown() (val bool) {
	mu.RLock()
	defer mu.RUnlock()
	val = shutDown
	return val
}

func SetRegisterModules(name string) {
	if registerModules == nil {
		registerModules = make(map[string]bool)
	}
	registerModules[strings.ToLower(name)] = true
}

func SetOnCloseRequest(val bool) {
	mu.Lock()
	defer mu.Unlock()
	onCloseRequest = val
}

func GetOnCloseRequest() (val bool) {
	mu.RLock()
	defer mu.RUnlock()
	val = onCloseRequest
	return val
}

func AddQueServerRemove(s string) {
	mu.Lock()
	queServerRemove[s] = true
	mu.Unlock()
}

func GetQueServerRemove() (res []string) {
	mu.RLock()
	defer mu.RUnlock()
	for s := range queServerRemove {
		res = append(res, s)
	}
	return res
}

func RemoveQueServerRemove(s string) {
	mu.Lock()
	delete(queServerRemove, s)
	mu.Unlock()
}

func GetLogger() logInterface.Logger {
	return logger
}

func GetTracing() tracingInterface.Tracing {
	return tracing
}

func Setup() {
	logger.SetLogLevel(logInterface.GetDebugLevelFromString(config.Logging.Level))
	logger.SetOutputFormat(logInterface.GetOutputFormatFromString(config.Logging.Format))
	logger.SetLogFile(config.Logging.File)
	tracing.Init(tracingInterface.ParsingConfigCli(logger, interfaces.Name, config.Tracing))
	config.Manual = nil
	Backend = NewBackend()
	for _, s := range config.Backend {
		Backend.Store(s.Name, s)
	}

	Routing = NewRouting()
	for _, s := range config.Routing {
		Routing.Store(s.Name, s)
	}

	for _, s := range config.Auth {
		if s.Oauth2Config != nil {
			if len(s.Oauth2Config.Login) == 0 {
				logger.Error("auth %s oauth2_config login is empty", s.Name).Quit()
			}

			if len(s.Oauth2Config.Logout) == 0 {
				logger.Error("auth %s oauth2_config logout is empty", s.Name).Quit()
			}

			if len(s.Oauth2Config.Callback) == 0 {
				logger.Error("auth %s oauth2_config callback is empty", s.Name).Quit()
			}

			if authCallbackUrl[s.Oauth2Config.Login] != nil {
				logger.Error("auth %s oauth2_config login already use", s.Name).Quit()
			}

			if authCallbackUrl[s.Oauth2Config.LoginProcess] != nil {
				logger.Error("auth %s oauth2_config loginProcess already use", s.Name).Quit()
			}

			if authCallbackUrl[s.Oauth2Config.Logout] != nil {
				logger.Error("auth %s oauth2_config logout already use", s.Name).Quit()
			}

			if authCallbackUrl[s.Oauth2Config.Callback] != nil {
				logger.Error("auth %s oauth2_config callback already use", s.Name).Quit()
			}

			authCallbackUrl[s.Oauth2Config.Login] = s
			authCallbackUrl[s.Oauth2Config.Logout] = s
			authCallbackUrl[s.Oauth2Config.Callback] = s
			authCallbackUrl[s.Oauth2Config.LoginProcess] = s

		}
	}

}

func GetUrlAuth(url string) *proxy.RouterAuth {
	mu.RLock()
	defer mu.RUnlock()
	return authCallbackUrl[url]
}

func GetTransport(name string) (ss *proxy.Transport) {
	for _, s := range config.Transport {
		if s.Name == name {
			return s
		}
	}
	return ss
}

func GetListenerList() (res []*proxy.Listener) {
	for _, s := range config.Listener {
		lss := &proxy.Listener{
			Name:             s.Name,
			Mode:             s.Mode,
			Listen:           s.Listen,
			Port:             s.Port,
			Secure:           s.Secure,
			Pem:              s.Pem,
			AutoReloadPemDir: s.AutoReloadPemDir,
			Destination:      s.Destination,
			TransportOption:  GetTransport(s.Transport),
			Allow:            s.Allow,
			Routing:          s.Routing,
			Labels:           s.Labels,
			MaxRequest:       s.MaxRequest,
			Vault:            s.Vault,
		}

		res = append(res, lss)
	}

	return res
}

func AddListener(res []*proxy.Listener) {
	config.Listener = append(config.Listener, res...)
}

func RemoveListener(name string) {
	var listener []*proxy.Listener
	for _, s := range config.Listener {
		if s.Name != name {
			listener = append(listener, s)
		}
	}
	config.Listener = listener
}

func GetListenerRouting(name string) (res []string) {
	for _, s := range config.Listener {
		if s.Name == name {
			return s.Routing
		}
	}
	return res
}

func GetHealthCheck() (res []*proxy.HealthCheck) {
	return config.Healthcheck
}

func HealthCheckValid(name string) (ss *proxy.HealthCheck, valid bool) {
	for _, s := range GetHealthCheck() {
		if s.Name == name {
			return s, true
		}
	}
	return ss, valid
}

func Validate() {
	GetLogger().Debug("validating config")
	for _, s := range GetListenerList() {
		for _, sr := range s.Routing {
			if _, valid := Routing.Load(sr); !valid {
				GetLogger().Error("routing %s for service %s not valid", sr, s.Name).Quit()
			}
		}
	}

	if registerModules != nil {
		for _, s := range Backend.LoadAll() {
			switch s.Mode {
			case "modules":
				if !registerModules[s.Driver] {
					var allDriver []string
					for k := range registerModules {
						allDriver = append(allDriver, k)
					}
					GetLogger().Error("modules driver '%s' not valid. available drivers is %s", s.Driver, strings.Join(allDriver, ", ")).Quit()
				}
			}
		}
	}

}
