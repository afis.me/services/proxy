package main

import (
	"code.afis.me/services/proxy/app"
)

func main() {
	//defer profile.Start(profile.MemProfile).Stop()
	app.NewApp().Start()
}
