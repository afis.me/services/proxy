FROM --platform=$BUILDPLATFORM node:20.12-alpine3.19 as ui-builder
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "Running on $BUILDPLATFORM, building for $TARGETPLATFORM"

WORKDIR /app
COPY . .
RUN apk add --no-cache make
RUN rm -f yarn.lock
RUN yarn cache clean
RUN make ui-build

FROM --platform=$BUILDPLATFORM golang:1.22.1-alpine3.19 as builder
ENV GOPROXY https://proxy.golang.org,direct
WORKDIR /app
COPY . .
COPY --from=ui-builder /app/static/dist ./static/dist/
RUN apk add --no-cache make 
RUN make build

FROM --platform=$BUILDPLATFORM alpine:3.19
RUN apk update && apk add --no-cache tzdata
WORKDIR /app
COPY --from=builder /app/bin/app ./
ENV GODEBUG madvdontneed
RUN mv ./app /usr/local/bin/app
RUN touch /etc/app.yaml
CMD ["/usr/local/bin/app", "--config=/etc/app.yaml"]