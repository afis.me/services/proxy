export PATH := $(PWD)/bin:/usr/local/go/bin:$(PATH)
export PKGNAME := code.afis.me/services/proxy

help:
	@echo "See Makefile"
tidy:
	@go mod tidy

run:
	@go run .

build: build-main

build-main: export CGO_ENABLED=0
build-main:
	@go run ./tools/main.go build --pkgName="${PKGNAME}" --disableOSName

ui-build:
	@cd ui && yarn install --network-timeout=600000
	@cd ui && MODE=production-build yarn build

image:
	@DOCKER_BUILDKIT=1 docker buildx build --load  \
      --platform linux/amd64 \
      --tag afisme/nano-proxy:latest --force-rm \
      -f Dockerfile .