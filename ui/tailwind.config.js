import { resolve } from 'pathe';
module.exports = {
    darkMode: "class",
    content: [
        resolve(__dirname, 'src/**/*.{html,js,ejs,jsx,tsx}'),
        resolve(__dirname, 'error.html'),
        resolve(__dirname, 'vanity.html'),
    ],
    future: {
        hoverOnlyWhenSupported: true,
    },
    theme: {
        screens: {
            'sm': '640px',
            'md': '768px',
            'ipl': '844px',
            'lg': '1024px',
            'xl': '1280px',
            '2xl': '1536px',
        },
        extend:
            {
                fontFamily: {
                    'noto-sans': ['Noto Sans', 'sans-serif'],
                },
            },
    },
    variants: {
        fill: ['hover', 'focus'],
    },
    corePlugins: {
        preflight: true,
    },
    plugins: []
}