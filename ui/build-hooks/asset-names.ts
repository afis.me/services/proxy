import {PreRenderedAsset} from "rollup";

// slitting asset output directory
type AssetOutputEntry = { output: string, regex: RegExp}
const assetDir = "internal/assets";
const entryFileNames = `${assetDir}/js/[name]-[hash].js`;
const chunkFileNames = `${assetDir}/js/[name]-[hash]-chunk.js`
const assets: AssetOutputEntry[] = [
    {
        output: `${assetDir}/img/[name]-[hash][extname]`,
        regex: /\.(png|jpe?g|gif|svg|webp|avif)$/
    },
    {
        output: `${assetDir}/fonts/[name]-[hash][extname]`,
        regex: /\.(woff|woff2|ttf)$/
    },
    {
        regex: /\.css$/,
        output: `${assetDir}/css/[name]-[hash][extname]`
    },
    {
        output: `${assetDir}/js/[name]-[hash][extname]`,
        regex: /\.js$/
    },
    {
        output: `static/[name][extname]`,
        regex: /\.(html|xml|json)$/
    }
];

const processAssetFileNames = (info: PreRenderedAsset): string => {
    if (info && info.name) {
        const name = info.name as string;
        const result = assets.find(a => a.regex.test(name));
        if (result) return result.output;
    }
    return `${assetDir}/[name]-[hash][extname]`
}


export type {
    AssetOutputEntry,
};

export {
    processAssetFileNames,
    assetDir,
    entryFileNames,
    chunkFileNames,
}