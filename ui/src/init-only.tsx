import '/src/styles/scss/index.scss';
import FontFaceObserver from 'fontfaceobserver';
import uri from 'jsuri';
let alreadyCall = false;

type CheckFontCallback = () => void;
const checkFonts = (callback: CheckFontCallback) => {
  const observers: any[] = [];
  observers.push(new FontFaceObserver('Noto Sans', {weight: 400}).load());
  observers.push(new FontFaceObserver('Noto Sans', {weight: 600}).load());
  observers.push(new FontFaceObserver('Noto Sans', {weight: 700}).load());
  Promise.all(observers).then(() => callback()).catch(() => callback());
}

const eventMediaCallback = (e: MediaQueryListEvent) :void => {
    let icon = document.querySelector('link[rel="icon"]');
    if (icon) {
        const href = icon.getAttribute("href")!;
        if (href) {
            const colorScheme = e.matches ? "dark" : "light";
            let urL = new uri(href).replaceQueryParam("colorScheme", colorScheme)
            icon.setAttribute("href", urL.toString());
        }   
    }
}

const setUp = () => {
    if (alreadyCall) return;
    if (document.documentElement) {
        alreadyCall = true;
        const t = localStorage.getItem("data-theme");
        const tss = localStorage.getItem("data-theme-auto-mode");
        if (tss) {
            if (/^true$/i.test(tss)) {
                if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
                    document.documentElement.classList.add("dark");
                    document.documentElement.setAttribute("data-theme", "dark");
                } else {
                    document.documentElement.classList.add("light");
                    document.documentElement.setAttribute("data-theme", "light");
                }
                return
            }
        }

        switch (t) {
            case "light":
                document.documentElement.classList.add(t);
                document.documentElement.setAttribute("data-theme", "light");
                break;
            case "dark":
                document.documentElement.classList.add(t);
                document.documentElement.setAttribute("data-theme", "dark");
                break;
            default:
                if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
                    document.documentElement.classList.add("dark");
                    document.documentElement.setAttribute("data-theme", "dark");
                } else {
                    document.documentElement.classList.add("light");
                    document.documentElement.setAttribute("data-theme", "light");
                }
        }

        const media = window.matchMedia('(prefers-color-scheme: dark)')
        if ('addEventListener' in media) {
            media.addEventListener('change', eventMediaCallback)
        } 

        checkFonts(() => {
            document.body.classList.remove('hidden');
        });

    }
}

setUp();
document.addEventListener("readystatechange", () => {
    if (document.readyState !== "interactive") return;
    setUp();
});

