import { defineConfig, BuildOptions } from 'vite'
import react from '@vitejs/plugin-react-swc'
import { resolve } from 'pathe';
import alias from '@rollup/plugin-alias'
import { ViteMinifyPlugin } from 'vite-plugin-minify'
import { assetDir, entryFileNames, processAssetFileNames, chunkFileNames } from './build-hooks/asset-names';
import {WatcherOptions} from "rollup";

interface BuildEnv {
    MODE?: string,
}


const env = process.env as BuildEnv
const validEnv = new Map<string, boolean>([
    ["production-build", true],
    ["production-watch", true],
    ["development", true]
]);


const envValid: string[] = []
for (const key of validEnv.keys()) {
    envValid.push(key)
}

export default defineConfig(({}) => {

    if (validEnv.get(env.MODE as string) !== true) {
        console.error("env MODE only valid one of value:", envValid.join(', '))
        process.exit(2)
    }

    const buildConfig :BuildOptions = {
        outDir: resolve(__dirname, "..", "static", "dist"),
        minify: "esbuild",
        copyPublicDir: true,
        cssMinify: true,
        sourcemap: false,
        assetsDir: assetDir,
        assetsInlineLimit: 0,
        emptyOutDir: true,
        rollupOptions: {
            input: {
                error: resolve(__dirname, 'error.html'),
                vanity: resolve(__dirname, 'vanity.html'),
            },
            output: {
                entryFileNames: entryFileNames,
                assetFileNames: processAssetFileNames,
                chunkFileNames: chunkFileNames,
            },
        },
    }

    let watch :WatcherOptions = {};
    if (env.MODE=== "production-watch") buildConfig.watch = watch;

    return {
        define: {
            global: "window",
        },
        plugins: [
            react(),
            alias({
                entries: [
                    {find: 'src', replacement: resolve(__dirname, 'src')},
                    {find: 'proto', replacement: resolve(__dirname, 'proto')},
                ]
            }),
            ViteMinifyPlugin(),
        ],
        css: {
            preprocessorOptions: {},
        },
        esbuild: {
            legalComments:"external",
            minifyWhitespace: true,
            minifySyntax: true,
        },
        server: {
            port: 3001,
        },
        publicDir: resolve(__dirname, "src/public"),
        build: buildConfig,
    }
})

